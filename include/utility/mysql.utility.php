<?php
function importSQL($dumpSQL) {
	$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

	if (mysqli_connect_error()) {
		die('Connect Error (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());
	}

	//echo 'Success... ' . $mysqli->host_info . ";".DB_NAME."<br />";
	//echo 'Retrieving dumpfile' . "<br />";

	$sql = file_get_contents(SQL_PATH . $dumpSQL);
	if (!$sql) {
		die('Error opening file');
	}
	//echo
	//'processing file <br />'.$sql;
	//$mysqli->query("DROP TABLE IF EXISTS 'food_item_of_preservation_table';");
	//if(mysqli_multi_query($mysqli,"DROP TABLE IF EXISTS 'food_item_of_preservation_table';")){
	if ($mysqli -> multi_query($sql)) {
		//echo 'done.';
	} else {
		echo "Table creation failed: (" . $mysqli -> errno . ") " . $mysqli -> error;
	}

	$mysqli -> close();
}

function isDatabaseAvailable() {
	$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	if (mysqli_connect_error()) {
		return false;	
	}
	//$mysqli -> select_db(DB_NAME);

	$tableList = array();
	$res = mysqli_query($mysqli, "SHOW TABLES");
	while ($cRow = mysqli_fetch_array($res)) {
		$tableList[] = $cRow[0];
	}
	if(sizeof($tableList)==0){
		return false;
	}
	else{
		return true;
	}

}

function selectDatabaseResponse() {
	$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	if ($result = $mysqli -> query("SELECT DATABASE()")) {
		$row = $result -> fetch_row();
		printf("Default database is %s.\n", $row[0]);
		$result -> close();

	}
}
?>