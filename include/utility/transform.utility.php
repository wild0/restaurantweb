<?php
function redirectPostUrl($urltopost, $datatopost) {
	//$urltopost = "http://somewebsite.com/script.php";
	//$datatopost = array("firstname" => "Mike", "lastname" => "Lopez", "email" => "my@email.com", );

	$ch = curl_init($urltopost);
	
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $datatopost);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	$returndata = curl_exec($ch);
	return $returndata;
}
function getPreservationURL(){
	global $variableHandler;
	$host = $variableHandler->getVariable(ROOT_IP);
	$prefix = "restaurantweb";
	
	$url = "http://".$host."/".$prefix."/include/command/preservation.cmd.php";
	return $url;
}
function getADURL(){
	global $variableHandler;
	$host = $variableHandler->getVariable(ROOT_IP);
	$prefix = "restaurantweb";
	
	$url = "http://".$host."/".$prefix."/include/command/ad.cmd.php";
	return $url;
}
?>