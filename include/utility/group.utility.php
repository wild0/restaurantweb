<?php
function getGroupIndex($customerSize){
	global $variableHandler;
	
	$groupAMinValue = $variableHandler->getVariable("group_a_min_value", 0);
	$groupAMaxValue = $variableHandler->getVariable("group_a_max_value", 0);
	$groupBMinValue = $variableHandler->getVariable("group_b_min_value", 0);
	$groupBMaxValue = $variableHandler->getVariable("group_b_max_value", 0);
	$groupCMinValue = $variableHandler->getVariable("group_c_min_value", 0);
	$groupCMaxValue = $variableHandler->getVariable("group_c_max_value", 0);
	$groupDMinValue = $variableHandler->getVariable("group_d_min_value", 0);
	$groupDMaxValue = $variableHandler->getVariable("group_d_max_value", 0);
	
	
	if($groupAMinValue<=$customerSize &&  $customerSize<=$groupAMaxValue){
		return 0;
	}
	else if($groupBMinValue<=$customerSize &&  $customerSize<=$groupBMaxValue){
		return 1;
	}
	else if($groupCMinValue<=$customerSize &&  $customerSize<=$groupCMaxValue){
		return 2;
	}
	else if($groupDMinValue<=$customerSize &&  $customerSize<=$groupDMaxValue){
		return 3;
	}
	
	/*
	if($customerSize<=$groupAMaxValue){
		return 0;
	}
	else if($customerSize>2 && $customerSize<=4){
		return 1;
	}
	else if($customerSize>4){
		return 2;
	}
	 */
	/*
	else if($customerSize>4 && $customerSize<=6){
		return 2;
	}
	else if($customerSize>6){
		return 3;
	}
	 * 
	 */
	else{
		return -1;
	}
}

?>