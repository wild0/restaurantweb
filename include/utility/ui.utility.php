<?php
function showHeaderMenuItems(){
	global $userHandler;
	if($userHandler->isLogin()==1){
		echo getStoreManagement();
		echo getADManagement();
		echo getUserManagement();
		echo getPreservationManagement();
		echo getLogout();
	}
}
function getStoreManagement(){
	$buf = "<div>";
	$buf .= "<a href=\"store_management.php\">Store</a>";
	$buf .= "</div>";
	return $buf;
}
function getADManagement(){
	$buf = "<div>";
	$buf .= "<a href=\"ad_management.php\">AD</a>";
	$buf .= "</div>";
	return $buf;
}
function getUserManagement(){
	$buf = "<div>";
	$buf .= "<a href=\"user_management.php\">User</a>";
	$buf .= "</div>";
	return $buf;
}
function getPreservationManagement(){
	$buf = "<div>";
	$buf .= "<a href=\"preservation_management.php\">Preservation</a>";
	$buf .= "</div>";
	return $buf;
}
function getLogout(){
	$buf = "<div>";
	$buf .= "<a href=\"../include/command/user.cmd.php?command=logout_cmd&return_url=login\">Logout</a>";
	$buf .= "</div>";
	return $buf;
}
?>