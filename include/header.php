<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once(CONSTANT_PATH."log.constant.php");
require_once(CONSTANT_PATH."error.constant.php");
require_once(CONSTANT_PATH."advertisement.constant.php");
require_once(CONSTANT_PATH."food.menu.constant.php");
require_once(CONSTANT_PATH."preservation.constant.php");
require_once(CONSTANT_PATH."store.constant.php");
require_once(CONSTANT_PATH."user.constant.php");
require_once(CONSTANT_PATH."card.constant.php");

require_once(DIC_PATH."status_code.dic.php");

require_once(INCLUDE_PATH."Criteria.php");
require_once(INCLUDE_PATH."database.class.php");
require_once(INCLUDE_PATH."mysql.class.php");

require_once(CLASS_PATH."object.class.php");
require_once(CLASS_PATH."log.class.php");
require_once(CLASS_PATH."advertisement.class.php");
require_once(CLASS_PATH."preservation.class.php");
require_once(CLASS_PATH."store.class.php");
require_once(CLASS_PATH."food.menu.class.php");
require_once(CLASS_PATH."user.class.php");
require_once(CLASS_PATH."card.class.php");

require_once(HANDLER_PATH."ad.handler.php");
require_once(HANDLER_PATH."log.handler.php");
require_once(HANDLER_PATH."preservation.handler.php");
require_once(HANDLER_PATH."store.handler.php");
require_once(HANDLER_PATH."food.menu.handler.php");
require_once(HANDLER_PATH."variable.handler.php");
require_once(HANDLER_PATH."user.handler.php");
require_once(HANDLER_PATH."card.handler.php");

require_once(UTILITY_PATH."group.utility.php");
require_once(UTILITY_PATH."mysql.utility.php");
require_once(UTILITY_PATH."transform.utility.php");
require_once(UTILITY_PATH."waittime.utility.php");
require_once(UTILITY_PATH."sms.utility.php");
require_once(UTILITY_PATH."ui.utility.php");

$debug = false;
$cache = false;

$db = new MySQL();
$db -> setPrefix(DB_PREFIX);
$db -> connect(true);

$remoteAddr = $_SERVER["REMOTE_ADDR"];

$adHandler = new AdvertisementHandler($db);
$preservationHandler = new PreservationRecordHandler($db);
$storeHandler = new StoreHandler($db);
$foodMenuHandler = new FoodMenuHandler($db);
$userHandler =  new UserHandler($db);
$variableHandler =  new VariableHandler($db);
$logHandler =  new LogHandler($db);
$cardHandler =  new CardHandler($db);

if(getMode()=="root"){
	if(isset($_REQUEST["command"]) && $_REQUEST["command"]==LOGIN_CMD){
	}
	else if(isset($position) && $position=="store_cmd"){
	}
	else if(isset($position) && $position=="preservation_cmd"){
	}
	else if(isset($position) && $position=="food_menu_cmd"){
	}
	else if(isset($position) && $position=="ad_cmd"){
	}
	else if(isset($position) && $position=="user_cmd"){
	}
	else if(isset($position) && $position=="html"){
	}
	else if(isset($position) && $position=="dashboard"){
	}
	else if($userHandler->isLogin()==0 && $position!="login"){
		if(isset($position)){
			header("location:../administrator/login.php?return_url=".$position);
		}
		else{
			header("location:../administrator/login.php?return_url=index");
		}
	}
}
if(getMode()=="node"){
	if(isset($_REQUEST["command"]) && $_REQUEST["command"]==LOGIN_CMD){
	}
	else if(isset($position) && $position=="store_cmd"){
	}
	else if(isset($position) && $position=="preservation_cmd"){
	}
	else if(isset($position) && $position=="food_menu_cmd"){
	}
	else if(isset($position) && $position=="ad_cmd"){
	}
	else if(isset($position) && $position=="user_cmd"){
	}
	else if(isset($position) && $position=="html"){
	}
	else if($userHandler->isLogin()==0 && $position!="login"){
		if(isset($position)){
			header("location:../administrator/login.php?return_url=".$position);
		}
		else{
			header("location:../administrator/login.php?return_url=index");
		}
	}
}

function getMode(){
	global $variableHandler;
	return $variableHandler->getVariable(HOST_ROLE);
}
?>