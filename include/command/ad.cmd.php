<?php

session_start();
$position = "ad_cmd";
//header ('Content-type:text/html; charset=utf-8');
require_once ("../constant/db.constant.php");
require_once (CONSTANT_PATH . "advertisement.constant.php");
require_once (INCLUDE_PATH . "header.php");

global $adHandler, $logHandler, $storeHandler;
$variable = $_REQUEST;
$command = $variable["command"];

/*
define("ADD_ADVERTISEMENT_CMD", "add_advertisement_cmd");
define("MODIFY_ADVERTISEMENT_CMD", "modify_advertisement_cmd");
define("GET_ADVERTISEMENTS_CMD", "get_advertisements_cmd");
define("REMOVE_ADVERTISEMENT_CMD", "remove_advertisement_cmd");
define("GET_JQGRID_ADVERTISEMENTS_CMD", "get_jqgrid_advertisements_cmd");
define("GET_LATEST_ADVERTISEMENTS_CMD", "get_latest_advertisements_cmd");
*/

if ($command == ADD_ADVERTISEMENT_CMD) {
	
	
	
	$advertisementName = $variable["advertisement_name_value"];
	$advertisementContent = $variable["advertisement_content_value"];

	$advertisementActive = 0;
	if (isset($variable["advertisement_active_value"])) {
		$advertisementActive = $variable["advertisement_active_value"];
	}
	
	//add log
	$addr = $_SERVER["REMOTE_ADDR"];
	$role = getMode();
	$logHandler->addLog($addr, $role, 101,"add ad to server");
	
	
	

	$advertisement = $adHandler -> create(true);
	$advertisement -> setVar("advertisement_name", $advertisementName);
	$advertisement -> setVar("advertisement_content", $advertisementContent);
	$advertisement -> setVar("advertisement_active", $advertisementActive);

	$insertId = $adHandler -> insert($advertisement);
	
	$response = new stdClass;
	$response -> status_code = 0;
	$response -> command = ADD_ADVERTISEMENT_CMD;
	$response -> insert_id = $insertId;

	if (!isset($variable["return_url"])) {
		echo json_encode($response);
	} else {
		//header("location:".$_SERVER["HTTP_REFERER"]);
		header("location:../../administrator/ad_management.php");
	}
} else if ($command == MODIFY_ADVERTISEMENT_CMD) {

	$advertisementId = $variable["advertisement_id_value"];
	$advertisementName = $variable["advertisement_name_value"];
	$advertisementContent = $variable["advertisement_content_value"];

	$advertisementActive = 0;
	if (isset($variable["advertisement_active_value"])) {
		$advertisementActive = $variable["advertisement_active_value"];
	}
	
	
	//add log
	$addr = $_SERVER["REMOTE_ADDR"];
	$role = getMode();
	$logHandler->addLog($addr, $role, 102,"modify ad to server");
	
	

	$advertisement = $adHandler -> get($advertisementId);
	$advertisement -> setVar("advertisement_name", $advertisementName);
	$advertisement -> setVar("advertisement_content", $advertisementContent);
	$advertisement -> setVar("advertisement_active", $advertisementActive);

	$insertId = $adHandler -> replace($advertisement);
	
	$response = new stdClass;
	$response -> status_code = 0;
	$response -> command = MODIFY_ADVERTISEMENT_CMD;
	$response -> insert_id = $insertId;

	if (!isset($variable["return_url"])) {
		echo json_encode($response);
	} else {
		//header("location:".$_SERVER["HTTP_REFERER"]);
		header("location:../../administrator/ad_management.php");
	}
} else if ($command == GET_ADVERTISEMENTS_CMD) {
	
	//add log
	$addr = $_SERVER["REMOTE_ADDR"];
	$role = getMode();
	$logHandler->addLog($addr, $role, 103,"GET_ADVERTISEMENTS_CMD");
	
	
	$adsJSON = array();
	$ads = $adHandler -> getAllAds();
	for ($j = 0; $j < sizeof($ads); $j++) {
		$ad = $ads[$j];

		$adName = $ad -> getVar("advertisement_name");

		$adJSON = new stdClass;
		$adJSON -> ad_name = $adName;

		$adsJSON[] = $adJSON;
	}
	
	$response = new stdClass;
	$response -> data = $adsJSON;
	$response -> status_code = 200;

	echo json_encode($resultJSON);

} else if ($command == LIST_JQGRID_ADVERTISEMENT_CMD) {

	
	//add log
	$addr = $_SERVER["REMOTE_ADDR"];
	$role = getMode();
	$logHandler->addLog($addr, 
	$role, 
	LIST_JQGRID_ADVERTISEMENT_CMD_CODE,
	LIST_JQGRID_ADVERTISEMENT_CMD);
	
	
	//$storeId = $_REQUEST["store_id"];

	$page = $_REQUEST['page'];
	// get the requested page
	$limit = $_REQUEST['rows'];
	// get how many rows we want to have into the grid
	$sidx = $_REQUEST['sidx'];
	// get index row - i.e. user click to sort
	$sord = $_REQUEST['sord'];
	// get the direction

	//$res = array();
	//$result = array();

	//$criteria  = new CompoCriteria(new Criteria('serial', $serial));
	$count = $adHandler -> getCount();
	$ads = $adHandler -> getAds($limit, ($page-1) * $limit);

	if ($count > 0) {

		$totalPages = ceil($count / $limit);
	} else {
		$totalPages = 0;
	}

	//if($race==0){
	//	return ;
	//}
	$response = new stdClass;
	$response -> page = (int)$page;
	$response -> total = $totalPages;
	$response -> records = (int)$count;
	$i = 0;
	for ($j = 0; $j < sizeof($ads); $j++) {
		$ad = $ads[$i];

		$adId = $ad -> getVar("advertisement_id");
		$adName = $ad -> getVar("advertisement_name");
		$adActive = $ad -> getVar("advertisement_active");
		$adTimeStart = $ad -> getVar("advertisement_time_start");
		$adTimeEnd = $ad -> getVar("advertisement_time_end");

		$response -> rows[$i]['id'] = $adId;
		$response -> rows[$i]['cell'] = array($adId, $adName, $adTimeStart, $adTimeEnd, $adActive);
		$i++;
	}
	echo json_encode($response);
} else if ($command == GET_LATEST_ADVERTISEMENT_CMD) {
	//echo "aaaa";
	echo $adHandler -> getLatest() -> getVar("advertisement_content");
}
?>