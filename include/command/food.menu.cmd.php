<?php

session_start();
$position = "food_menu_cmd";
//header ('Content-type:text/html; charset=utf-8');
require_once ("../constant/db.constant.php");
require_once (CONSTANT_PATH . "food.menu.constant.php");
require_once (INCLUDE_PATH . "header.php");

global $foodMenuHandler;
$variable = $_REQUEST;
$command = $variable["command"];

/*
 define("ADD_FOOD_MENU_CMD", "add_food_menu_cmd");
 //新增
 define("MODIFY_FOOD_MENU_CMD", "modify_food_menu_cmd");
 //修改
 define("GET_FOOD_MENUS_CMD", "get_food_menus_cmd");
 //取得清單
 define("REMOVE_FOOD_MENU_CMD", "remove_food_menu_cmd");
 //移除

 //node
 define("UPDATE_ALL_FOOD_MENU_CMD", "update_all_food_menu_cmd");

 //root
 define("GET_ALL_FOOD_MENU_CMD", "get_all_food_menu_cmd");

 *
 */

if ($command == ADD_FOOD_MENU_CMD) {
	$response = new stdClass;
	$statusCode = 0;
	if (getMode() == "root") {
		//root

		$foodMenuName = $variable["food_menu_name_value"];
		$foodMenuPrice = $variable["food_menu_price_value"];
		$foodMenuDescription = $variable["food_menu_description_value"];

		$foodMenu = $foodMenuHandler -> create(true);
		$foodMenu -> setVar("food_menu_name", $foodMenuName);
		$foodMenu -> setVar("food_menu_price", $foodMenuPrice);
		$foodMenu -> setVar("food_menu_description", $foodMenuDescription);

		$insertId = $foodMenuHandler -> insertFoodMenu($foodMenu);

		$addr = $_SERVER["REMOTE_ADDR"];
		$role = ROLE_ROOT;
		if ($insertId) {
			//add log
			$statusCode = STATUS_CODE_ADD_FOOD_MENU_CMD_SUCCESS;
			$logHandler -> addLog($addr, $role, ADD_FOOD_MENU_CMD_CODE, "Add item[" . $foodMenuName . "] to food menu", $statusCode);
			$response -> insert_id = $insertId;
		} else {
			//add log
			$statusCode = STATUS_CODE_ADD_FOOD_MENU_CMD_FAIL;
			$logHandler -> addLog($addr, $role, ADD_FOOD_MENU_CMD_CODE, "Add item[" . $foodMenuName . "] to food menu", $statusCode);

		}

		$response -> status_code = $statusCode;
		$response -> command = ADD_FOOD_MENU_CMD;

	} else {
		//node

		//add log
		$addr = $_SERVER["REMOTE_ADDR"];
		$role = ROLE_NODE;
		$logHandler -> addLog($addr, $role, ADD_FOOD_MENU_CMD_CODE, "Add item[" . $foodMenuName . "] to food menu", STATUS_CODE_ADD_FOOD_MENU_CMD_FAIL_WRONG_ROLE);

		$response -> status_code = STATUS_CODE_ADD_FOOD_MENU_CMD_FAIL_WRONG_ROLE;
		$response -> command = ADD_FOOD_MENU_CMD;
	}

	if (!isset($variable["return_url"])) {
		echo json_encode($response);
	} else {
		//header("location:".$_SERVER["HTTP_REFERER"]);
		header("location:../../administrator/food_menu_management.php");
	}

} else if ($command == MODIFY_FOOD_MENU_CMD) {
	$response = new stdClass;
	$statusCode = 0;
	if (getMode() == "root") {
		$addr = $_SERVER["REMOTE_ADDR"];
		$role = ROLE_ROOT;

		$foodMenuId = $variable["food_menu_id_value"];
		$foodMenuName = $variable["food_menu_name_value"];
		$foodMenuPrice = $variable["food_menu_price_value"];
		$foodMenuDescription = $variable["food_menu_description_value"];

		$foodMenu = $foodMenuHandler -> get($foodMenuId);
		if ($foodMenu) {
			$foodMenu -> setVar("food_menu_name", $foodMenuName);
			$foodMenu -> setVar("food_menu_price", $foodMenuPrice);
			$foodMenu -> setVar("food_menu_description", $foodMenuDescription);

			$result = $foodMenuHandler -> modifyFoodMenu($foodMenu);
			if ($result) {
				$statusCode = STATUS_CODE_MODIFY_FOOD_MENU_CMD_SUCCESS;
				$logHandler -> addLog($addr, $role, MODIFY_FOOD_MENU_CMD_CODE, "Modify item[" . $foodMenuName . "] to food menu success", STATUS_CODE_MODIFY_FOOD_MENU_CMD_SUCCESS);

			} else {
				$statusCode = STATUS_CODE_MODIFY_FOOD_MENU_CMD_FAIL;
				$logHandler -> addLog($addr, $role, MODIFY_FOOD_MENU_CMD_CODE, "Modify item[" . $foodMenuName . "] to food menu success", STATUS_CODE_MODIFY_FOOD_MENU_CMD_FAIL);
			}
		} else {
			//menu does not exist
			$statusCode = STATUS_CODE_MODIFY_FOOD_MENU_CMD_FAIL_NOT_FOUND;
			$logHandler -> addLog($addr, $role, MODIFY_FOOD_MENU_CMD_CODE, "Modify item[" . $foodMenuName . "] to food menu success", STATUS_CODE_MODIFY_FOOD_MENU_CMD_FAIL_NOT_FOUND);
		}

		$response -> status_code = $statusCode;
		$response -> command = MODIFY_FOOD_MENU_CMD;

	} else {
		//node

		//add log
		$addr = $_SERVER["REMOTE_ADDR"];
		$role = ROLE_NODE;
		$logHandler -> addLog($addr, $role, MODIFY_FOOD_MENU_CMD_CODE, "Modify item[" . $foodMenuName . "] to food menu fail", STATUS_CODE_MODIFY_FOOD_MENU_CMD_FAIL_WRONG_ROLE);

		$response -> status_code = STATUS_CODE_MODIFY_FOOD_MENU_CMD_FAIL_WRONG_ROLE;
		$response -> command = MODIFY_FOOD_MENU_CMD;
	}

	if (!isset($variable["return_url"])) {
		echo json_encode($response);
	} else {
		//header("location:".$_SERVER["HTTP_REFERER"]);
		header("location:../../administrator/food_menu_management.php");
	}

} else if ($command == LIST_JQGRID_FOOD_MENU_CMD) {

	//$storeId = $_REQUEST["store_id"];

	$page = $_REQUEST['page'];
	// get the requested page
	$limit = $_REQUEST['rows'];
	// get how many rows we want to have into the grid
	$sidx = $_REQUEST['sidx'];
	// get index row - i.e. user click to sort
	$sord = $_REQUEST['sord'];
	// get the direction

	//add log
	$addr = $_SERVER["REMOTE_ADDR"];
	$role = ROLE_ROOT;
	$logHandler -> addLog($addr, $role, LIST_JQGRID_FOOD_MENU_CMD_CODE, LIST_JQGRID_FOOD_MENU_CMD);

	//$criteria  = new CompoCriteria(new Criteria('serial', $serial));
	$count = $foodMenuHandler -> getFoodMenuCount();
	//echo $count;
	$foodMenus = $foodMenuHandler -> getFoodMenus($limit, ($page - 1) * $limit);

	if ($count > 0) {

		$totalPages = ceil($count / $limit);
	} else {
		$totalPages = 0;
	}

	//if($race==0){
	//	return ;
	//}
	$response = new stdClass;
	$response -> page = (int)$page;
	$response -> total = $totalPages;
	$response -> records = (int)$count;
	$i = 0;
	for ($j = 0; $j < sizeof($foodMenus); $j++) {
		$foodMenu = $foodMenus[$i];

		$foodMenuName = $foodMenu -> getVar("food_menu_name");
		$foodMenuId = $foodMenu -> getVar("food_menu_id");
		$foodMenuPrice = $foodMenu -> getVar("food_menu_price");
		$foodMenuDescription = $foodMenu -> getVar("food_menu_description");

		$response -> rows[$i]['id'] = $foodMenuId;
		$response -> rows[$i]['cell'] = array($foodMenuId, $foodMenuName, $foodMenuPrice, $foodMenuDescription);
		$i++;
	}
	echo json_encode($response);
} else if ($command == UPDATE_ALL_FOOD_MENU_CMD && $variableHandler -> getVariable(HOST_ROLE) == "node") {

	//node process only

	//add log
	$addr = $_SERVER["REMOTE_ADDR"];
	$role = ROLE_NODE;
	$logHandler -> addLog($addr, $role, 205, "update food menu");

	$result = $foodMenuHandler -> updateDataFromRoot();
	$resultJSON = new stdClass;
	$resultJSON -> result = $result;
	$resultJSON -> status_code = 200;

	echo json_encode($resultJSON);
} else if ($command == LIST_FOOD_MENU_CMD) {
	//

	//add log
	$addr = $_SERVER["REMOTE_ADDR"];
	$role = ROLE_ROOT;
	$logHandler -> addLog($addr, $role, 206, "list all food menu");

	$foodMenusJSON = array();
	$foodMenus = $foodMenuHandler -> getFoodMenus();
	for ($j = 0; $j < sizeof($foodMenus); $j++) {
		$foodMenu = $foodMenus[$j];

		$foodMenuName = $foodMenu -> getVar("food_menu_name");
		$foodMenuId = $foodMenu -> getVar("food_menu_id");
		$foodMenuPrice = $foodMenu -> getVar("food_menu_price");
		$foodMenuDescription = $foodMenu -> getVar("food_menu_description");

		$foodMenuJSON = new stdClass;
		$foodMenuJSON -> food_menu_name = $foodMenuName;
		$foodMenuJSON -> food_menu_id = $foodMenuId;
		$foodMenuJSON -> food_menu_price = $foodMenuPrice;
		$foodMenuJSON -> food_menu_description = $foodMenuDescription;
		$foodMenusJSON[] = $foodMenuJSON;
	}
	$resultJSON = new stdClass;
	$resultJSON -> data = $foodMenusJSON;
	$resultJSON -> status_code = 200;

	echo json_encode($resultJSON);

} else if ($command == REMOVE_FOOD_MENU_CMD) {
	$response = new stdClass;
	$response -> command = REMOVE_FOOD_MENU_CMD_CODE;
	$statusCode = 0;
	if (getMode() == "root") {
		$foodMenuId = $_REQUEST["food_menu_id"];

		//add log
		$addr = $_SERVER["REMOTE_ADDR"];
		$role = ROLE_ROOT;

		$result = $foodMenuHandler -> removeFoodMenu($foodMenuId);
		if ($result) {
			$statusCode = STATUS_CODE_REMOVE_FOOD_MENU_CMD_SUCCESS;
			$logHandler -> addLog($addr, $role, REMOVE_FOOD_MENU_CMD_CODE, "remove food menu [" . $foodMenuId . "]", STATUS_CODE_REMOVE_FOOD_MENU_CMD_SUCCESS);
			
		} else {
			$statusCode = STATUS_CODE_REMOVE_FOOD_MENU_CMD_FAIL;
			$logHandler -> addLog($addr, $role, REMOVE_FOOD_MENU_CMD_CODE, "remove food menu [" . $foodMenuId . "]", STATUS_CODE_REMOVE_FOOD_MENU_CMD_FAIL);

		}

	} else {
		$statusCode = STATUS_CODE_REMOVE_FOOD_MENU_CMD_FAIL_WRONG_ROLE;
		$logHandler -> addLog($addr, $role, REMOVE_FOOD_MENU_CMD_CODE, "remove food menu [" . $foodMenuId . "]", STATUS_CODE_REMOVE_FOOD_MENU_CMD_FAIL_WRONG_ROLE);

	}
	
	$response -> status_code = $statusCode;
	
	if (!isset($variable["return_url"])) {
		echo json_encode($response);
	} else {
		//header("location:".$_SERVER["HTTP_REFERER"]);
		header("location:../../administrator/food_menu_management.php");
	}
}
?>