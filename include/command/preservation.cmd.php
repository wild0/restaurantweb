<?php

session_start();
$position = "preservation_cmd";
//header ('Content-type:text/html; charset=utf-8');
require_once ("../constant/db.constant.php");

require_once (CONSTANT_PATH . "preservation.constant.php");

require_once (INCLUDE_PATH . "header.php");
require_once (INCLUDE_PATH . "twilio-twilio-php-e931821/Services/Twilio.php");

global $preservationHandler, $storeHandler, $logHandler, $variableHandler;
$variable = $_REQUEST;
$command = $variable["command"];

if ($command == ADD_PRESERVATION_CMD) {

	$statusCode = 0;

	$preservationId = -1;
	$preservationUniqueId = "";
	$preservationStoreId = $_REQUEST["preservation_store_id"];
	$preservationTimestamp = time();
	$preservationType = 0;
	$preservationPersonCount = $_REQUEST["preservation_person_count"];
	
	/*
	if ($preservationPersonCount <= 2) {
		$preservationGroup = 0;
	} else if ($preservationPersonCount > 2 && $preservationPersonCount <= 4) {
		$preservationGroup = 1;
	} else if ($preservationPersonCount > 4 ) {
		$preservationGroup = 2;
	} 
	 */
	$preservationGroup = getGroupIndex($preservationPersonCount);
	
	//else if ($preservationPersonCount > 6) {
	//	$preservationGroup = 3;
	//}
	//$preservationGroup = $_REQUEST["preservation_group"];
	$preservationTel = $_REQUEST["preservation_tel"];
	$preservationSplitable = $_REQUEST["preservation_splitable"];

	//取得ticket
	$ticket = 1;
	$lastRecord = $preservationHandler -> getLastRecordByStoreId($preservationStoreId);
	if ($lastRecord != null) {
		// if the record exist, accumlate the ticket
		$ticket = $lastRecord -> getVar("preservation_record_ticket") + 1;
	}

	/////////////////////////////////////////////////////////////
	
	$resultDescription = "";
	
	if ($variableHandler -> getVariable(HOST_ROLE) == "node") {

		//add log
		//$addr = $_SERVER["REMOTE_ADDR"];
		//$role = ROLE_NODE;
		//$logHandler->addLog($addr, $role, ADD_PRESERVATION_CMD_CODE,"Add preservation ticket[".$ticket."] to local");

		$url = getPreservationURL();
		$postData = array("command" => ADD_PRESERVATION_CMD, "preservation_store_id" => $preservationStoreId, "preservation_person_count" => $preservationPersonCount, "preservation_group" => $preservationGroup, "preservation_tel" => $preservationTel, "preservation_splitable" => $preservationSplitable);
		$responseData = redirectPostUrl($url, $postData);
		
		//echo "responseData:".$responseData;
		if ($responseData != "") {
			$returnJson = json_decode($responseData);
			$preservationId = $returnJson -> insert_id;
			$preservationUniqueId = $returnJson -> insert_unique_id;
			$resultDescription = "COMMON_ADD_PRESERVATION_CMD_ROOT_RESPONSE:{".$responseData."}";
			$statusCode = COMMON_ADD_PRESERVATION_CMD_ROOT_RESPONSE;
		} else {
			$preservationId = $preservationHandler -> generatePreservationId();
			$preservationUniqueId = $preservationHandler -> generatePreservationUniqueId($preservationStoreId);
			$statusCode = COMMON_ADD_PRESERVATION_CMD_ROOT_NO_RESPONSE;
			$resultDescription = "COMMON_ADD_PRESERVATION_CMD_ROOT_NO_RESPONSE";
		}
		//echo "bridge:".$url.":".$responseData;
		
		$preservation = $preservationHandler -> create(true);
		$preservation -> setVar("preservation_record_id", $preservationId);
		$preservation -> setVar("preservation_record_unique_id", $preservationUniqueId);
		$preservation -> setVar("preservation_record_store_id", $preservationStoreId);
		$preservation -> setVar("preservation_record_timestamp", $preservationTimestamp);
		$preservation -> setVar("preservation_record_person_count", $preservationPersonCount);
		$preservation -> setVar("preservation_record_group", $preservationGroup);
		$preservation -> setVar("preservation_record_status", 1);
		$preservation -> setVar("preservation_record_tel", $preservationTel);
		$preservation -> setVar("preservation_record_splitable", $preservationSplitable);
		$preservation -> setVar("preservation_record_ticket", $ticket);

		$preservationHandler -> replace($preservation);

		/*
		 $response = new stdClass;
		 $response -> status_code = 0;
		 $response -> command = ADD_PRESERVATION_CMD;
		 $response -> insert_id = $preservationId;

		 $data["id"] = $preservationId;
		 $data["ticket"] = $ticket;
		 $data["date"] = $preservationTimestamp;
		 $data["person"] = $preservationPersonCount;
		 $data["splitable"] = $preservationSplitable;
		 $data["tel"] = $preservationTel;

		 $data["wait_count"] = $preservationHandler->getCountByStoreIdAndGroupIdAndStatus($preservationStoreId, $preservationGroup, 1);
		 $response -> data = $data;

		 echo json_encode($response);
		 * */
		/*
		 if(isset($_REQUEST["preservation_sms"]) && $_REQUEST["preservation_sms"]=="true"){
		 try{
		 $preservationHandler->sendSMSMessage("",$preservationTel, $data["wait_count"]);
		 }
		 catch(Exception $e){

		 }
		 }
		 *
		 */

		//$data["sms"]=$preservationHandler->sendSMSMessage("","");

	} else if ($variableHandler -> getVariable(HOST_ROLE) == "root") {

		//add log
		//$addr = $_SERVER["REMOTE_ADDR"];
		//$role = ROLE_ROOT;
		//$logHandler->addLog($addr, $role, ADD_PRESERVATION_CMD_CODE,"Add preservation ticket[".$ticket."] to local");

		$preservationId = $preservationHandler -> generatePreservationId();
		$preservationUniqueId = $preservationHandler -> generatePreservationUniqueId($preservationStoreId);
		
		//echo "preservationUniqueId:".$preservationUniqueId.":";

		$preservation = $preservationHandler -> create(true);
		$preservation -> setVar("preservation_record_id", $preservationId);
		$preservation -> setVar("preservation_record_unique_id", $preservationUniqueId);
		$preservation -> setVar("preservation_record_store_id", $preservationStoreId);
		$preservation -> setVar("preservation_record_timestamp", $preservationTimestamp);
		$preservation -> setVar("preservation_record_person_count", $preservationPersonCount);
		$preservation -> setVar("preservation_record_group", $preservationGroup);
		$preservation -> setVar("preservation_record_status", 1);
		$preservation -> setVar("preservation_record_tel", $preservationTel);
		$preservation -> setVar("preservation_record_splitable", $preservationSplitable);
		$preservation -> setVar("preservation_record_ticket", $ticket);
		$preservation -> setVar("preservation_record_sync", 2);

		$insertId = $preservationHandler -> replace($preservation);
		$statusCode = COMMON_ADD_PRESERVATION_CMD_AT_ROOT;
		$resultDescription = "COMMON_ADD_PRESERVATION_CMD_AT_ROOT";
		/*
		 $response = new stdClass;
		 $response -> status_code = 0;
		 $response -> command = ADD_PRESERVATION_CMD;
		 $response -> insert_id = $insertId;

		 $data["id"] = $insertId;
		 $data["ticket"] = $ticket;
		 //$data["dateStr"] = date("Ymd H:i:s", $preservationTimestamp);
		 $data["date"] = $preservationTimestamp;
		 $data["person"] = $preservationPersonCount;
		 $data["splitable"] = $preservationSplitable;
		 $data["tel"] = $preservationTel;
		 //$data["sms"]=$preservationHandler->sendSMSMessage("","");
		 $response -> data = $data;

		 echo json_encode($response);
		 * */
	} else {
		//wrong role response
		$statusCode = COMMON_ADD_PRESERVATION_CMD_WRONG_ROLE;
		$resultDescription = "COMMON_ADD_PRESERVATION_CMD_WRONG_ROLE";
		/*
		 $addr = $_SERVER["REMOTE_ADDR"];

		 $response = new stdClass;
		 $response -> status_code = COMMON_PRESERVATION_CMD_ERROR_ROLE_CODE;
		 $response -> status_description = COMMON_PRESERVATION_CMD_ERROR_ROLE_DESCRIPTION;
		 $response -> command = ADD_PRESERVATION_CMD;

		 $logHandler->addLog($addr, $role, COMMON_PRESERVATION_CMD_ERROR_ROLE_CODE, COMMON_PRESERVATION_CMD_ERROR_ROLE_DESCRIPTION);

		 echo json_encode($response);
		 */
	}
	$addr = $_SERVER["REMOTE_ADDR"];
	$role = getMode();
	switch($statusCode) {
		case COMMON_ADD_PRESERVATION_CMD_ROOT_NO_RESPONSE :
			//server no response
			$logHandler -> addLog($addr, $role, COMMON_ADD_PRESERVATION_CMD, " the role does not allow create store, the status code[" . $statusCode . "]", COMMON_ADD_PRESERVATION_CMD_ROOT_NO_RESPONSE, "COMMON_ADD_PRESERVATION_CMD_ROOT_NO_RESPONSE");
			break;
		case COMMON_ADD_PRESERVATION_CMD_ROOT_RESPONSE :
			//echo COMMON_ADD_USER_CMD_COMPLETE;
			$logHandler -> addLog($addr, $role, COMMON_ADD_PRESERVATION_CMD, " store[" . $preservationId . "] has been create, the status code[" . $statusCode . "]", COMMON_ADD_PRESERVATION_CMD_ROOT_RESPONSE, $resultDescription);

			break;
		case COMMON_ADD_PRESERVATION_CMD_WRONG_ROLE :
			//echo COMMON_ADD_USER_CMD_COMPLETE;
			$logHandler -> addLog($addr, $role, COMMON_ADD_PRESERVATION_CMD, " store[" . $preservationId . "] has been create, the status code[" . $statusCode . "]", COMMON_ADD_PRESERVATION_CMD_WRONG_ROLE, "COMMON_ADD_PRESERVATION_CMD_WRONG_ROLE");
			break;
		case COMMON_ADD_PRESERVATION_CMD_AT_ROOT :
			//echo COMMON_ADD_USER_CMD_COMPLETE;
			$logHandler -> addLog($addr, $role, COMMON_ADD_PRESERVATION_CMD, " store[" . $preservationId . "] has been create, the status code[" . $statusCode . "]", COMMON_ADD_PRESERVATION_CMD_AT_ROOT, "COMMON_ADD_PRESERVATION_CMD_AT_ROOT");
			break;
		default :
			break;
	}

	$response = new stdClass;
	$response -> status_code = $statusCode;
	$response -> command = ADD_PRESERVATION_CMD;
	$response -> insert_id = $preservationId;
	$response -> insert_unique_id = $preservationUniqueId;

	$data["id"] = $preservationId;
	$data["unique_id"] = $preservationUniqueId;
	$data["ticket"] = $ticket;
	$data["date"] = $preservationTimestamp;
	$data["person"] = $preservationPersonCount;
	$data["splitable"] = $preservationSplitable;
	$data["tel"] = $preservationTel;

	$data["wait_count"] = $preservationHandler -> getCountByStoreIdAndGroupIdAndStatus($preservationStoreId, $preservationGroup, 1);
	$response -> data = $data;

	echo json_encode($response);

} else if ($command == LIST_JQGRID_PRESERVATION_CMD) {
	$storeId = -1;
	if (isset($_REQUEST["store_id"])) {
		if($_REQUEST["store_id"]=="-1"){
			//$storeId = $variableHandler->getVariable(NODE_STORE_ID);
		}
		else{
			$storeId = $_REQUEST["store_id"];
		}
	}
	else{
		$storeId = $variableHandler->getVariable(NODE_STORE_ID);
	}

	//add log
	$addr = $_SERVER["REMOTE_ADDR"];
	$role = ROLE_ROOT;
	//$logHandler->addLog($addr, $role, 302," list preservation at store ".$storeId);

	$page = $_REQUEST['page'];
	
	// get the requested page
	$limit = $_REQUEST['rows'];
	// get how many rows we want to have into the grid
	$sidx = $_REQUEST['sidx'];
	// get index row - i.e. user click to sort
	$sord = $_REQUEST['sord'];
	// get the direction

	//$res = array();
	//$result = array();

	//$criteria  = new CompoCriteria(new Criteria('serial', $serial));
	$count = $preservationHandler -> getCountByStoreId($storeId);
	$preservations = $preservationHandler -> getRecordsByStoreId($storeId, $limit, ($page-1) * $limit);

	if ($count > 0) {

		$totalPages = ceil($count / $limit);
	} else {
		$totalPages = 0;
	}

	//if($race==0){
	//	return ;
	//}
	$response = new stdClass;
	$response -> page = (int)$page;
	$response -> total = $totalPages;
	$response -> records = (int)$count;
	$i = 0;
	//echo sizeof($preservations);
	for ($j = 0; $j < sizeof($preservations); $j++) {
		$preservation = $preservations[$i];

		$preservationId = $preservation -> getVar("preservation_record_id");
		$preservationStoreId = $preservation -> getVar("preservation_record_store_id");
		
		$preservationStoreName = "Local";
		$store = $storeHandler->get($preservationStoreId);
		if($store!=null){
			$preservationStoreName = $store -> getVar("store_name");
		}
		
		$preservationTimestamp = $preservation -> getVar("preservation_record_timestamp");
		$preservationType = $preservation -> getVar("preservation_record_type");
		$preservationSplitable = $preservation -> getVar("preservation_record_splitable");
		$preservationSplitableLabel = "";
		if($preservationSplitable==1){
			$preservationSplitableLabel="Splitable";
		}
		else{
			
		}
		
		$preservationPersons = $preservation -> getVar("preservation_record_person_count");
		$preservationTel = $preservation -> getVar("preservation_record_tel");
		$preservationResult = $preservation -> getVar("preservation_record_result");
		$preservationComment = $preservation -> getVar("preservation_record_comment");

		$response -> rows[$i]['id'] = $preservationId;
		$response -> rows[$i]['cell'] = array($preservationId, $preservationStoreName, date("m/d/Y H:i:s", $preservationTimestamp), $preservationSplitableLabel, $preservationPersons, $preservationTel);
		$i++;
	}
	echo json_encode($response);
} else if ($command == LIST_CURRENT_PRESERVATION_OF_GROUP_CMD) {

	//$storeId = $_REQUEST["store_id"];
	$storeId = -1;
	if (!isset($_REQUEST["store_id"])) {
		$storeId = $variableHandler -> getVariable(NODE_STORE_ID);
	} else {
		$storeId = $_REQUEST["store_id"];
	}

	//add log
	$addr = $_SERVER["REMOTE_ADDR"];
	$role = ROLE_NODE;
	//$logHandler->addLog($addr, $role, 304," list current preservation at store ".$storeId);

	$today = getdate();
	$year = $today["year"];
	$mon = $today["mon"];
	$dayofmon = $today["mday"];

	$timeStart = mktime(0, 0, 0, $mon, $dayofmon, $year);
	$timeEnd = mktime(23, 59, 59, $mon, $dayofmon, $year);

	//$groupId = $_REQUEST["group_id"];
	//echo "storeId".$storeId;
	$groupAPreservations = $preservationHandler -> getRecordsByStatusAndStoreIdAndGroupId(PRESERVATION_STATUS_SEAT, $storeId, 0, $timeStart, $timeEnd, 'DESC');
	$groupBPreservations = $preservationHandler -> getRecordsByStatusAndStoreIdAndGroupId(PRESERVATION_STATUS_SEAT, $storeId, 1, $timeStart, $timeEnd, 'DESC');
	$groupCPreservations = $preservationHandler -> getRecordsByStatusAndStoreIdAndGroupId(PRESERVATION_STATUS_SEAT, $storeId, 2, $timeStart, $timeEnd, 'DESC');
	$groupDPreservations = $preservationHandler -> getRecordsByStatusAndStoreIdAndGroupId(PRESERVATION_STATUS_SEAT, $storeId, 3, $timeStart, $timeEnd, 'DESC');

	//echo "A:".sizeof($groupAPreservations);
	//echo "B:".sizeof($groupBPreservations);
	//echo "C:".sizeof($groupCPreservations);
	//echo "D:".sizeof($groupDPreservations);

	$groupCallPreservations = $preservationHandler -> getRecordsByStatusAndStoreId(PRESERVATION_STATUS_CALL, $storeId, $timeStart, $timeEnd);

	$preservations = new stdClass;

	if ($groupAPreservations != null) {
		$groupAPreservation = $groupAPreservations[0];
		$groupACount = $preservationHandler -> getCountByStoreIdAndGroupIdAndStatus($storeId, 0, 1);
		$preservations -> a = $groupAPreservation -> toJSON($groupACount);
	} else {
		$groupAPreservation = array();
	}

	if ($groupBPreservations != null) {
		$groupBPreservation = $groupBPreservations[0];
		$groupBCount = $preservationHandler -> getCountByStoreIdAndGroupIdAndStatus($storeId, 1, 1);
		$preservations -> b = $groupBPreservation -> toJSON($groupBCount);
	} else {
		$groupBPreservation = array();
	}

	if ($groupCPreservations != null) {
		$groupCPreservation = $groupCPreservations[0];
		$groupCCount = $preservationHandler -> getCountByStoreIdAndGroupIdAndStatus($storeId, 2, 1);
		$preservations -> c = $groupCPreservation -> toJSON($groupCCount);
	} else {
		$groupCPreservation = array();
	}

	if ($groupDPreservations != null) {
		$groupDPreservation = $groupDPreservations[0];
		$groupDCount = $preservationHandler -> getCountByStoreIdAndGroupIdAndStatus($storeId, 3, 1);
		$preservations -> d = $groupDPreservation -> toJSON($groupDCount);
	} else {
		$groupDPreservation = array();
	}

	if ($groupCallPreservations != null) {
		//$groupCallPreservation = $groupCallPreservations[0];
		$groupCallPreservationJSONs = array();
		for ($j = 0; $j < sizeof($groupCallPreservations); $j++) {
			//$groupCallPreservation = $groupCallPreservations[$j];
			$groupCallPreservationJSONs[] = $groupCallPreservations[$j] -> toJSON();
		}

		$preservations -> calling = $groupCallPreservationJSONs;
	} else {
		$groupCallPreservation = array();
	}

	$preservations -> size = sizeof($preservations);

	$response = new stdClass;
	$response -> time = time();
	$response -> time_start = $timeStart;
	$response -> time_end = $timeEnd;

	$response -> status_code = 200;
	$response -> command = LIST_CURRENT_PRESERVATION_OF_GROUP_CMD;
	$response -> data = $preservations;

	//echo json_encode($preservations);
	echo json_encode($response);

} else if ($command == WAIT_PRESERVATION_CMD) {
	$preservationUniqueId = $_REQUEST["preservation_unique_id"];
	if ($variableHandler -> getVariable(HOST_ROLE) == "node") {

		//add log
		//$addr = $_SERVER["REMOTE_ADDR"];
		//$role = ROLE_NODE;
		//$logHandler -> addLog($addr, $role, 310, " change the preservation[" . $preservationId . "] to wait");

		$url = getPreservationURL();
		//echo $url;
		$postData = array("command" => WAIT_PRESERVATION_CMD, "preservation_unique_id" => $preservationUniqueId);
		$responseData = redirectPostUrl($url, $postData);
		//echo $responseData;
		$statusCode = 0;
		if ($responseData != "") {
			$returnJson = json_decode($responseData);
			$preservationUniqueId = $returnJson -> preservation_unique_id;
			//echo $preservationId;
			$statusCode = $returnJson -> status_code;
			$statusCode = $preservationHandler -> changeStatusByPreservationUniqueId($preservationUniqueId, PRESERVATION_STATUS_WAIT);
		} else {
			$statusCode = $preservationHandler -> changeStatusByPreservationUniqueId($preservationUniqueId, PRESERVATION_STATUS_WAIT);
		}

		$response = new stdClass;
		$response -> status_code = $statusCode;
		$response -> command = WAIT_PRESERVATION_CMD;
		//$response -> preservation_id = $preservationId;
		$response -> preservation_unique_id = $preservationUniqueId;
		echo json_encode($response);

		//header("location:../../dashboard/dashboard.php?status_code=".$statusCode);
	} else if ($variableHandler -> getVariable(HOST_ROLE) == "root") {
		$statusCode = $preservationHandler -> changeStatusByPreservationUniqueId($preservationUniqueId, PRESERVATION_STATUS_WAIT);
		if ($statusCode == 500) {
			$statusCode = 501;
			//server update fail;
		}

		//add log
		$addr = $_SERVER["REMOTE_ADDR"];
		$role = ROLE_ROOT;
		$logHandler -> addLog($addr, $role, 310, " change the preservation[" . $preservationUniqueId . "] to wait, the status code[" . $statusCode . "]");

		$response = new stdClass;
		$response -> status_code = $statusCode;
		$response -> command = WAIT_PRESERVATION_CMD;
		$response -> preservation_unique_id = $preservationUniqueId;

		echo json_encode($response);
	}

} else if ($command == SEAT_PRESERVATION_CMD) {
	$preservationUniqueId = $_REQUEST["preservation_unique_id"];
	$p = $preservationHandler -> get($preservationUniqueId);
	//$preservationHandler -> changeStatusByPreservationId($preservationId, 2);

	//header("location:../../dashboard/dashboard.php");

	if ($variableHandler -> getVariable(HOST_ROLE) == "node") {

		$url = getPreservationURL();
		//echo $url;
		$postData = array("command" => SEAT_PRESERVATION_CMD, "preservation_unique_id" => $preservationUniqueId);
		$responseData = redirectPostUrl($url, $postData);
		//echo "responseData:".$responseData;
		$statusCode = 0;
		$sms = "";
		if ($responseData != "") {
			$returnJson = json_decode($responseData);
			$preservationUniqueId = $returnJson -> preservation_unique_id;
			$storeName = $returnJson -> preservation_store_name;
			//echo $preservationId;
			$statusCode = $returnJson -> status_code;
			$statusCode = $preservationHandler -> changeStatusByPreservationUniqueId($preservationUniqueId, PRESERVATION_STATUS_SEAT);
			$groupId = $p -> getVar("preservation_record_group");
			$storeId = $p -> getVar("preservation_record_store_id");
			
			
			//取得下兩個號碼
			$record = $preservationHandler -> getRecordsByStatusAndStoreIdAndGroupIdAndIndex(1, $storeId, $groupId, 2);
			//echo "aaa:".$record;
			if ($record != null) {
				$sms = "has_record";
				if ($record -> getVar("preservation_record_tel") != "") {
					//$sms = $preservationHandler -> sendSMSMessage("", $record->getVar("preservation_record_tel"), 2,$p->getTicketLabel(), $storeName);
					//echo $record->getVar("preservation_record_id")."send";
					try {
						//在sms.utility.php
						$sms =  sendSMSMessage("", $record -> getVar("preservation_record_tel"), 2, $p -> getTicketLabel(), $storeName);
					} catch(Exception $e) {

					}
				}
			}
		} else {
			//root沒有回應
			$statusCode = $preservationHandler -> changeStatusByPreservationUniqueId($preservationUniqueId, PRESERVATION_STATUS_SEAT);
		}

		//add log
		$addr = $_SERVER["REMOTE_ADDR"];
		$role = ROLE_NODE;
		$logHandler -> addLog($addr, $role, 311, " change the preservation[" . $preservationUniqueId . "] to seat, the status code[" . $statusCode . "]");

		$response = new stdClass;
		$response -> time = time();
		$response -> status_code = $statusCode;
		$response -> command = SEAT_PRESERVATION_CMD;
		$response -> preservation_unique_id = $preservationUniqueId;
		echo json_encode($response);

		//header("location:../../dashboard/dashboard.php?status_code=".$statusCode."&sms=".$sms);
	} else if ($variableHandler -> getVariable(HOST_ROLE) == "root") {
		$statusCode = $preservationHandler -> changeStatusByPreservationUniqueId($preservationUniqueId, PRESERVATION_STATUS_SEAT);
		$store = $storeHandler -> get($p -> getVar("preservation_record_store_id"));
		if ($statusCode == 500) {
			$statusCode = 501;
			//server update fail;
		}

		//add log
		$addr = $_SERVER["REMOTE_ADDR"];
		$role = ROLE_ROOT;
		$logHandler -> addLog($addr, $role, 311, " change the preservation[" . $preservationUniqueId . "] to seat, the status code[" . $statusCode . "]");

		$response = new stdClass;
		$response -> status_code = $statusCode;
		$response -> time = time();
		$response -> command = SEAT_PRESERVATION_CMD;
		$response -> preservation_unique_id = $preservationUniqueId;
		$response -> preservation_store_name = $store -> getVar("store_name");
		$response -> preservation_store_address = $store -> getVar("store_address");

		echo json_encode($response);
	}

} else if ($command == CANCEL_PRESERVATION_CMD) {
	$preservationUniqueId = $_REQUEST["preservation_unique_id"];
	//$preservationHandler -> changeStatusByPreservationId($preservationId, 3);

	//header("location:../../dashboard/dashboard.php");
	if ($variableHandler -> getVariable(HOST_ROLE) == "node") {

		$url = getPreservationURL();
		//echo $url;
		$postData = array("command" => CANCEL_PRESERVATION_CMD, "preservation_unique_id" => $preservationUniqueId);
		$responseData = redirectPostUrl($url, $postData);
		//echo $responseData;
		$statusCode = 0;
		if ($responseData != "") {
			$returnJson = json_decode($responseData);
			$preservationUniqueId = $returnJson -> preservation_unique_id;
			//echo $preservationId;
			$statusCode = $returnJson -> status_code;
			$statusCode = $preservationHandler -> changeStatusByPreservationUniqueId($preservationUniqueId, PRESERVATION_STATUS_REMOVE);
		} else {
			$statusCode = $preservationHandler -> changeStatusByPreservationUniqueId($preservationUniqueId, PRESERVATION_STATUS_REMOVE);
		}

		//add log
		$addr = $_SERVER["REMOTE_ADDR"];
		$role = ROLE_NODE;
		$logHandler -> addLog($addr, $role, 312, " change the preservation[" . $preservationUniqueId . "] to cancel, the status code[" . $statusCode . "]");

		$response = new stdClass;
		$response -> status_code = $statusCode;
		$response -> time = time();
		$response -> command = CANCEL_PRESERVATION_CMD;
		$response -> preservation_unique_id = $preservationUniqueId;
		echo json_encode($response);

		//header("location:../../dashboard/dashboard.php?status_code=".$statusCode);
	} else if ($variableHandler -> getVariable(HOST_ROLE) == "root") {
		$statusCode = $preservationHandler -> changeStatusByPreservationUniqueId($preservationUniqueId, PRESERVATION_STATUS_REMOVE);
		if ($statusCode == 500) {
			$statusCode = 501;
			//server update fail;
		}

		//add log
		$addr = $_SERVER["REMOTE_ADDR"];
		$role = ROLE_ROOT;
		$logHandler -> addLog($addr, $role, 312, " change the preservation[" . $preservationUniqueId . "] to cancel, the status code[" . $statusCode . "]");

		$response = new stdClass;
		$response -> status_code = $statusCode;
		$response -> time = time();
		$response -> command = CANCEL_PRESERVATION_CMD;
		$response -> preservation_unique_id = $preservationUniqueId;

		echo json_encode($response);
	}
} else if ($command == SKIP_PRESERVATION_CMD) {
	$preservationUniqueId = $_REQUEST["preservation_unique_id"];
	//$statusCode = $preservationHandler -> changeStatusByPreservationId($preservationId, 4);

	//header("location:../../dashboard/dashboard.php?status_code=".$statusCode);
	if ($variableHandler -> getVariable(HOST_ROLE) == "node") {

		$url = getPreservationURL();
		//echo $url;
		$postData = array("command" => SKIP_PRESERVATION_CMD, "preservation_unique_id" => $preservationUniqueId);
		$responseData = redirectPostUrl($url, $postData);
		//echo $responseData;
		$statusCode = 0;
		if ($responseData != "") {
			$returnJson = json_decode($responseData);
			$preservationUniqueId = $returnJson -> preservation_unique_id;
			//echo $preservationId;
			$statusCode = $returnJson -> status_code;
			$statusCode = $preservationHandler -> changeStatusByPreservationUniqueId($preservationUniqueId, PRESERVATION_STATUS_SKIP);
		} else {
			$statusCode = $preservationHandler -> changeStatusByPreservationUniqueId($preservationUniqueId, PRESERVATION_STATUS_SKIP);
		}

		//add log
		$addr = $_SERVER["REMOTE_ADDR"];
		$role = ROLE_NODE;
		$logHandler -> addLog($addr, $role, SKIP_PRESERVATION_CMD, " change the preservation[" . $preservationUniqueId . "] to skip, the status code[" . $statusCode . "]");

		$response = new stdClass;
		$response -> status_code = $statusCode;
		$response -> command = SKIP_PRESERVATION_CMD;
		$response -> preservation_unique_id = $preservationUniqueId;
		echo json_encode($response);

		//header("location:../../dashboard/dashboard.php?status_code=".$statusCode);
	} else if ($variableHandler -> getVariable(HOST_ROLE) == "root") {
		$statusCode = $preservationHandler -> changeStatusByPreservationUniqueId($preservationUniqueId, PRESERVATION_STATUS_SKIP);
		if ($statusCode == 500) {
			//the target preservation dose not exist
			$statusCode = 501;
			//server update fail;
		}

		//add log
		$addr = $_SERVER["REMOTE_ADDR"];
		$role = ROLE_ROOT;
		$logHandler -> addLog($addr, $role, SKIP_PRESERVATION_CMD, " change the preservation[" . $preservationUniqueId . "] to skip, the status code[" . $statusCode . "]");

		$response = new stdClass;
		$response -> status_code = $statusCode;
		$response -> command = SKIP_PRESERVATION_CMD;
		$response -> preservation_unique_id = $preservationUniqueId;

		echo json_encode($response);
	}
} else if ($command == CALL_PRESERVATION_CMD) {
	$preservationUniqueId = $_REQUEST["preservation_unique_id"];
	//$accessLocation = $_REQUEST["location"];
	//$statusCode = $preservationHandler -> changeStatusByPreservationId($preservationId, 5);
	$response = new stdClass;
	//header("location:../../dashboard/dashboard.php?status_code=".$statusCode);
	if ($variableHandler -> getVariable(HOST_ROLE) == "node") {

		$url = getPreservationURL();

		$postData = array("command" => CALL_PRESERVATION_CMD, "preservation_unique_id" => $preservationUniqueId);
		$responseData = redirectPostUrl($url, $postData);
		//echo "response data :".$responseData;
		//echo "response data :".$responseData;
		/*
		 * if $responseData equals "" then means no network available
		 *
		 *
		 */

		$rootStatusCode = 0;
		$nodeStatusCode = 0;
		if ($responseData != "") {
			$returnJson = json_decode($responseData);
			$rootStatusCode = $returnJson -> status_code;
			if ($rootStatusCode == 200) {
				$preservationId = $returnJson -> preservation_unique_id;
				$nodeStatusCode = $preservationHandler -> changeStatusByPreservationUniqueId($preservationUniqueId, PRESERVATION_STATUS_CALL);

			} else if ($rootStatusCode == 501) {
				//the preservation not exist in root
				$nodeStatusCode = $preservationHandler -> changeStatusByPreservationUniqueId($preservationUniqueId, PRESERVATION_STATUS_CALL);
			}

		} else {
			
			$nodeStatusCode = $preservationHandler -> changeStatusByPreservationUniqueId($preservationUniqueId, PRESERVATION_STATUS_CALL);
		}
		//echo "nodeStatusCode :".$nodeStatusCode;
		//add log
		$addr = $_SERVER["REMOTE_ADDR"];
		$role = ROLE_NODE;
		switch($nodeStatusCode) {
			case 0 :
				//server no response
				$logHandler -> addLog($addr, $role, COMMON_CALL_PRESERVATION_CMD, " change the preservation[" . $preservationUniqueId . "] to call, the status code[" . $nodeStatusCode . "]");
				break;
			case 500 :
				// the target preservation does not exist in node
				$logHandler -> addLog($addr, $role, COMMON_CALL_PRESERVATION_CMD, " change the preservation[" . $preservationUniqueId . "] to call, the status code[" . $nodeStatusCode . "]", COMMON_CALL_PRESERVATION_CMD_ERROR_ROOT_ROLE_PRESERVATION_NOT_EXIST, "COMMON_CALL_PRESERVATION_CMD_ERROR_ROOT_ROLE_PRESERVATION_NOT_EXIST");
				break;
			case 501 :
				// the target preservation does not exist in root
				$logHandler -> addLog($addr, $role, COMMON_CALL_PRESERVATION_CMD, " change the preservation[" . $preservationUniqueId . "] to call, the status code[" . $nodeStatusCode . "]", COMMON_CALL_PRESERVATION_CMD_ERROR_NODE_ROLE_PRESERVATION_NOT_EXIST, "COMMON_CALL_PRESERVATION_CMD_ERROR_NODE_ROLE_PRESERVATION_NOT_EXIST");

				break;
			default :
				break;
		}
		//$logHandler->addLog($addr, $role, CALL_PRESERVATION_CMD," change the preservation[".$preservationId."] to call, the status code[".$statusCode."]");

		//$response = new stdClass;
		$response -> status_code = $nodeStatusCode;
		$response -> command = CALL_PRESERVATION_CMD;
		$response -> preservation_unique_id = $preservationUniqueId;
		$response -> role = "root";
		//echo json_encode($response);

		//header("location:../../dashboard/dashboard.php?status_code=".$statusCode);
	} else if ($variableHandler -> getVariable(HOST_ROLE) == "root") {
		$statusCode = $preservationHandler -> changeStatusByPreservationUniqueId($preservationUniqueId, PRESERVATION_STATUS_CALL);
		if ($statusCode == 500) {
			$statusCode = 501;
			//server update fail;
		}

		//add log
		$addr = $_SERVER["REMOTE_ADDR"];
		$role = ROLE_ROOT;
		switch($statusCode) {
			case 501 :
				//server no response
				$logHandler -> addLog($addr, $role, COMMON_CALL_PRESERVATION_CMD, " change the preservation[" . $preservationUniqueId . "] to call, the status code[" . $statusCode . "]");
				break;
			default :
				break;
		}

		
		$response -> status_code = $statusCode;
		$response -> command = CALL_PRESERVATION_CMD;
		$response -> preservation_unique_id = $preservationUniqueId;
		$response -> role = "node";
		
	}
	echo json_encode($response);
} else if ($command == SEND_SMS_MESSAGE_CMD) {
	echo "sms";
	//$preservationHandler->sendSMSMessage("","");
} else if ($command == LIST_PRESERVATION_BY_TIME_RANGE_CMD) {

	$storeId = $_REQUEST["store_id"];
	$timeStart = $_REQUEST["time_start"];
	$timeEnd = $_REQUEST["time_end"];

	//add log
	$addr = $_SERVER["REMOTE_ADDR"];
	$role = ROLE_NODE;
	$logHandler -> addLog($addr, $role, 305, "get preservations between " . $timeStart . " and " . $timeEnd . " at store " . $storeId);

	$count = $preservationHandler -> getCountByStoreIdAndTimeRangeAndStatus($storeId, $timeStart, $timeEnd);

	$preservations = $preservationHandler -> getRecordsByStoreIdAndTimeRangeAndStatus($storeId, $timeStart, $timeEnd);

	//echo $count;
	$response = new stdClass;
	$response -> status_code = 200;
	$response -> time = time();
	
	$response -> start_time = $timeStart;
	$response -> end_time = $endStart;
	$response -> command = LIST_PRESERVATION_BY_TIME_RANGE_CMD;

	$data = array();
	for ($i = 0; $i < sizeof($preservations); $i++) {
		$preservation = $preservations[$i];
		$data[] = $preservation -> toJSON();
		/*
		 $preservationId = $preservation -> getVar("preservation_record_id");
		 $preservationStoreId = $preservation -> getVar("preservation_record_store_id");
		 $preservationTimestamp = $preservation -> getVar("preservation_record_timestamp");
		 $preservationType = $preservation -> getVar("preservation_record_type");
		 $preservationSplitable = $preservation -> getVar("preservation_record_splitable");

		 $preservationPersons = $preservation -> getVar("preservation_record_person_count");
		 $preservationTel = $preservation -> getVar("preservation_record_tel");
		 $preservationResult = $preservation -> getVar("preservation_record_result");
		 $preservationComment = $preservation -> getVar("preservation_record_comment");
		 */

	}
	$response -> data = $data;

	echo json_encode($response);
} else if ($command == LIST_RESERVATION_BY_TODAY_CMD) {

	//$storeId = $variableHandler->getVariable(NODE_STORE_ID);
	$storeId = -1;
	if (!isset($_REQUEST["store_id"])) {
		$storeId = $variableHandler -> getVariable(NODE_STORE_ID);
	} else {
		$storeId = $_REQUEST["store_id"];
	}
	$today = getdate();
	$year = $today["year"];
	$mon = $today["mon"];
	$dayofmon = $today["mday"];

	$timeStart = mktime(0, 0, 0, $mon, $dayofmon, $year);
	$timeEnd = mktime(23, 59, 59, $mon, $dayofmon, $year);

	//add log
	$addr = $_SERVER["REMOTE_ADDR"];
	$role = ROLE_NODE;
	$logHandler -> addLog($addr, $role, 306, "get preservations between " . $timeStart . " and " . $timeEnd . " at store " . $storeId);

	//$timeStart = $_REQUEST["time_start"];
	//$timeEnd = $_REQUEST["time_end"];
	//echo $timeStart.",".$timeEnd;

	$count = $preservationHandler -> getCountByStoreIdAndTimeRangeAndStatus($storeId, $timeStart, $timeEnd);
	$preservations = $preservationHandler -> getRecordsByStoreIdAndTimeRangeAndStatus($storeId, $timeStart, $timeEnd);

	//echo $count;
	$response = new stdClass;
	$response -> status_code = 200;
	$response -> time_start = $timeStart;
	$response -> time_end = $timeEnd;
	$response -> time = time();
	$response -> command = LIST_RESERVATION_BY_TODAY_CMD;

	$data = array();
	for ($i = 0; $i < sizeof($preservations); $i++) {
		$preservation = $preservations[$i];
		$data[] = $preservation -> toJSON();
		/*
		 $preservationId = $preservation -> getVar("preservation_record_id");
		 $preservationStoreId = $preservation -> getVar("preservation_record_store_id");
		 $preservationTimestamp = $preservation -> getVar("preservation_record_timestamp");
		 $preservationType = $preservation -> getVar("preservation_record_type");
		 $preservationSplitable = $preservation -> getVar("preservation_record_splitable");

		 $preservationPersons = $preservation -> getVar("preservation_record_person_count");
		 $preservationTel = $preservation -> getVar("preservation_record_tel");
		 $preservationResult = $preservation -> getVar("preservation_record_result");
		 $preservationComment = $preservation -> getVar("preservation_record_comment");
		 */

	}
	$response -> data = $data;

	echo json_encode($response);
}
?>