<?php

session_start();
$position = "user_cmd";
//header ('Content-type:text/html; charset=utf-8');
require_once ("../constant/db.constant.php");
require_once (CONSTANT_PATH . "user.constant.php");
require_once (INCLUDE_PATH . "header.php");

global $userHandler, $variableHandler;
$variable = $_REQUEST;
$command = $variable["command"];

//define("LOGIN_CMD","login_cmd");
//define("LOGOUT_CMD","logout_cmd");
/*
 define("ADD_USER_CMD","add_user_cmd");
 define("REMOVE_USER_CMD","remove_user_cmd");
 define("GET_JQGRID_USERS_CMD","get_jqgrid_users_cmd");
 */

if ($command == LOGIN_CMD) {
	$statusCode = 0;

	$uname = $_REQUEST["uname"];
	$password = $_REQUEST["password"];
	$device = $_REQUEST["device"];

	$platform = "web";
	if (isset($_REQUEST["platform"])) {
		$platform = $_REQUEST["platform"];
	}

	$returnUrl = "";
	if (isset($_REQUEST["return_url"])) {
		$returnUrl = $_REQUEST["return_url"];
	}

	$result = $userHandler -> login($uname, $password, $device);
	if ($result == 0) {
		$statusCode = STATUS_CODE_LOGIN_CMD_SUCCESS;

	} else if ($result == 1){
		$statusCode = STATUS_CODE_LOGIN_CMD_FAIL_WRONG_PASSWORD;

	}
	else if ($result == 2){
		$statusCode = STATUS_CODE_LOGIN_CMD_FAIL_DEVICE_ERROR;

	}

	//echo $statusCode;
	//add log
	$addr = $_SERVER["REMOTE_ADDR"];
	$role = getMode();
	switch($statusCode) {
		case STATUS_CODE_LOGIN_CMD_FAIL_WRONG_PASSWORD :
			//server no response
			$logHandler -> addLog($addr, $role, LOGIN_CMD_CODE, " user[" . $uname . "] does not exist, the status code[" . $statusCode . "]", STATUS_CODE_LOGIN_CMD_FAIL_WRONG_PASSWORD, "COMMON_LOGIN_CMD_ERROR_USER_DOES_NOT_EXIST");
			break;
		case STATUS_CODE_LOGIN_CMD_SUCCESS :
			//echo COMMON_ADD_USER_CMD_COMPLETE;
			$logHandler -> addLog($addr, $role, LOGIN_CMD_CODE, " user[" . $uname . "] login complete, the status code[" . $statusCode . "]", STATUS_CODE_LOGIN_CMD_SUCCESS, "COMMON_LOGIN_CMD_COMPLETE");
			break;
		case STATUS_CODE_LOGIN_CMD_FAIL_DEVICE_ERROR :
			//echo COMMON_ADD_USER_CMD_COMPLETE;
			$logHandler -> addLog($addr, $role, LOGIN_CMD_CODE, " user[" . $uname . "] login complete, the status code[" . $statusCode . "]", STATUS_CODE_LOGIN_CMD_FAIL_DEVICE_ERROR, "COMMON_LOGIN_CMD_COMPLETE");
			break;
		default :
			break;
	}

	$ret = new stdClass;
	//$ret -> result = $result;
	$ret -> status_code = $statusCode;
	$ret -> store_id = $variableHandler -> getVariable(NODE_STORE_ID);

	if ($platform == "web" && $returnUrl == "store") {
		header("location:../../administrator/store_management.php");
	}
	if ($platform == "web" && $returnUrl == "ad") {
		header("location:../../administrator/ad_management.php");
	}
	if ($platform == "web") {
		header("location:../../administrator/preservation_status_management.php");
	} else {
		echo json_encode($ret);
	}
}
if ($command == LOGOUT_CMD) {
	$platform = "web";
	if (isset($_REQUEST["platform"])) {
		$platform = $_REQUEST["platform"];
	}

	$result = $userHandler -> logout();

	$ret = new stdClass;
	$ret -> result = $result;
	if ($result == 0) {
		$ret -> status_code = STATUS_CODE_LOGOUT_CMD_SUCCESS;
	} else {
		$ret -> status_code = STATUS_CODE_LOGOUT_CMD_FAIL;
	}
	//echo getMode().$_REQUEST["return_url"];
	if ($platform == "web") {
		header("location:../../administrator/login.php");
	} else {
		echo json_encode($ret);
	}
}
if ($command == ADD_USER_CMD) {
	$uname = $_REQUEST["uname_value"];
	$password = $_REQUEST["password_value"];
	$devices = $_REQUEST["device_list_value"];
	$storeId = $variableHandler -> getVariable(NODE_STORE_ID);

	//print_r($_REQUEST);
	$statusCode = 0;
	$userId = -1;
	if ($userHandler -> getByUserName($uname)) {
		//the user has exist
		$statusCode = STATUS_CODE_ADD_USER_CMD_FAIL_EXIST;
		//COMMON_ADD_USER_CMD_ERROR_USER_EXIST
	} else {
		$user = $userHandler -> create(true);
		$user -> setVar("user_name", $uname);
		$user -> setVar("user_password", $password);
		$user -> setVar("user_device_list", $devices);
		$user -> setVar("user_store_id", $storeId);

		$userId = $userHandler -> insertUser($user);
		$statusCode = STATUS_CODE_ADD_USER_CMD_SUCCESS;
		// add account success

	}
	//echo $statusCode;
	//add log
	$addr = $_SERVER["REMOTE_ADDR"];
	$role = getMode();
	switch($statusCode) {
		case STATUS_CODE_ADD_USER_CMD_FAIL_EXIST :
			//server no response
			$logHandler -> addLog($addr, $role, ADD_USER_CMD_CODE, " add user[" . $uname . "], the status code[" . $statusCode . "]", STATUS_CODE_ADD_USER_CMD_FAIL_EXIST, "COMMON_ADD_USER_CMD_ERROR_USER_EXIST");
			break;
		case STATUS_CODE_ADD_USER_CMD_SUCCESS :
			//echo COMMON_ADD_USER_CMD_COMPLETE;
			$logHandler -> addLog($addr, $role, ADD_USER_CMD_CODE, " add user[" . $uname . "], the status code[" . $statusCode . "]", STATUS_CODE_ADD_USER_CMD_SUCCESS, "COMMON_ADD_USER_CMD_COMPLETE");
			break;
		default :
			break;
	}

	$response = new stdClass;
	$response -> status_code = $statusCode;
	$response -> command = ADD_USER_CMD;
	$response -> insert_id = $userId;
	//echo json_encode($response);
	if (!isset($variable["return_url"])) {
		echo json_encode($response);
	} else {
		//header("location:".$_SERVER["HTTP_REFERER"]);
		header("location:../../administrator/user_management.php?status_code=" . $statusCode);
	}

}

if ($command == MODIFY_USER_CMD) {
	$uid = $_REQUEST["uid_value"];
	$uname = $_REQUEST["uname_value"];
	$password = $_REQUEST["password_value"];
	$devices = $_REQUEST["device_list_value"];
	$storeId = $variableHandler -> getVariable(NODE_STORE_ID);

	//print_r($_REQUEST);
	$statusCode = 0;
	$user = $userHandler -> getByUserName($uname);
	if ($user) {
		$user = $userHandler -> get($uid);
		//$user -> setVar("user_id", $uid);
		$user -> setVar("user_name", $uname);
		$user -> setVar("user_password", $password);
		$user -> setVar("user_device_list", $devices);
		$user -> setVar("user_store_id", $storeId);

		$result = $userHandler -> modifyUser($user);
		$statusCode = STATUS_CODE_MODIFY_USER_CMD_SUCCESS;
	} else {
		$statusCode = STATUS_CODE_MODIFY_USER_CMD_FAIL_NOT_FOUND;
	}

	$addr = $_SERVER["REMOTE_ADDR"];
	$role = getMode();
	switch($statusCode) {
		case STATUS_CODE_MODIFY_USER_CMD_FAIL_NOT_FOUND :
			//server no response
			$logHandler -> addLog($addr, $role, MODIFY_USER_CMD_CODE, " add user[" . $uname . "], the status code[" . $statusCode . "]", STATUS_CODE_MODIFY_USER_CMD_FAIL_NOT_FOUND, "COMMON_MODIFY_USER_CMD_ERROR_USER_DOSE_NOT_EXIST");
			break;
		case STATUS_CODE_MODIFY_USER_CMD_SUCCESS :
			//echo COMMON_ADD_USER_CMD_COMPLETE;
			$logHandler -> addLog($addr, $role, MODIFY_USER_CMD_CODE, " add user[" . $uname . "], the status code[" . $statusCode . "]", STATUS_CODE_MODIFY_USER_CMD_SUCCESS, "COMMON_MODIFY_USER_CMD_COMPLETE");
			break;
		default :
			break;
	}

	$response = new stdClass;
	$response -> status_code = $statusCode;
	$response -> command = MODIFY_USER_CMD;


	if (!isset($variable["return_url"])) {
		echo json_encode($response);
	} else {
		//header("location:".$_SERVER["HTTP_REFERER"]);
		header("location:../../administrator/user_management.php");
	}

}

if ($command == REMOVE_USER_CMD) {
	$uid = $_REQUEST["user_id"];

	$statusCode = 0;
	$user = $userHandler -> get($uid);
	if ($user) {
		$userHandler -> remove($uid);
		$statusCode = STATUS_CODE_REMOVE_USER_CMD_SUCCESS;
	} else {
		$statusCode = STATUS_CODE_REMOVE_USER_CMD_FAIL_NOT_FOUND;
	}

	$addr = $_SERVER["REMOTE_ADDR"];
	$role = getMode();
	switch($statusCode) {
		case STATUS_CODE_REMOVE_USER_CMD_FAIL_NOT_FOUND :
			//server no response
			$logHandler -> addLog($addr, $role, REMOVE_USER_CMD_CODE, " add user[" . $uname . "], the status code[" . $statusCode . "]", STATUS_CODE_REMOVE_USER_CMD_FAIL_NOT_FOUND, "COMMON_REMOVE_USER_CMD_ERROR_USER_DOSE_NOT_EXIST");
			break;
		case STATUS_CODE_REMOVE_USER_CMD_SUCCESS :
			//echo COMMON_ADD_USER_CMD_COMPLETE;
			$logHandler -> addLog($addr, $role, REMOVE_USER_CMD_CODE, " add user[" . $uname . "], the status code[" . $statusCode . "]", STATUS_CODE_REMOVE_USER_CMD_SUCCESS, "COMMON_REMOVE_USER_CMD_COMPLETE");
			break;
		default :
			break;
	}

	$response = new stdClass;
	$response -> status_code = $statusCode;
	$response -> command = REMOVE_USER_CMD;
	//$response -> insert_id = $uid;

	if (!isset($variable["return_url"])) {
		echo json_encode($response);
	} else {
		//header("location:".$_SERVER["HTTP_REFERER"]);
		header("location:../../administrator/user_management.php?status_code=" . $statusCode);
	}
} else if ($command == LIST_JQGRID_USERS_CMD) {
	global $userHandler;
	//$storeId = $_REQUEST["store_id"];

	$page = $_REQUEST['page'];
	// get the requested page
	$limit = $_REQUEST['rows'];
	// get how many rows we want to have into the grid
	$sidx = $_REQUEST['sidx'];
	// get index row - i.e. user click to sort
	$sord = $_REQUEST['sord'];
	// get the direction

	//$res = array();
	//$result = array();

	//$criteria  = new CompoCriteria(new Criteria('serial', $serial));
	$count = $userHandler -> getUserCount();
	$users = $userHandler -> getUsers($limit, ($page - 1) * $limit);

	if ($count > 0) {

		$totalPages = ceil($count / $limit);
	} else {
		$totalPages = 0;
	}

	//if($race==0){
	//	return ;
	//}
	$response = new stdClass;
	$response -> page = (int)$page;
	$response -> total = $totalPages;
	$response -> records = (int)$count;
	$i = 0;
	for ($j = 0; $j < sizeof($users); $j++) {
		$user = $users[$i];

		$userId = $user -> getVar("user_id");
		$userName = $user -> getVar("user_name");
		$userDeviceList = $user -> getVar("user_device_list");

		$response -> rows[$i]['id'] = $userId;
		$response -> rows[$i]['cell'] = array($userId, $userName, $userDeviceList);
		$i++;
	}
	echo json_encode($response);
}
?>
