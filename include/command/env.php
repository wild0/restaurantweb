<?php
session_start ();
$position = "user_cmd";
// header ('Content-type:text/html; charset=utf-8');
require_once ("../constant/db.constant.php");
require_once (CONSTANT_PATH . "user.constant.php");
require_once (CONSTANT_PATH . "error.constant.php");
require_once (INCLUDE_PATH . "header.php");

global $userHandler, $variableHandler;
$variable = $_REQUEST;
$command = $variable ["command"];

if ($command == "get_info_cmd") {
	$response = new stdClass ();
	$response->status_code = 200;
	
	$response->mode = getMode ();
	echo json_encode ( $response );
} else if ($command == "login_root_cmd") {
	
	$rootIp = $variable ["root_ip"];
	$storeId = $variable ["node_store_id"];
	$storePassword = $variable ["node_store_password"];
	
	$arr = array ();
	$arr ["store_id"] = $storeId;
	$arr ["store_password"] = $storePassword;
	$arr ["command"] = LOGIN_STORE_CMD;
	// print_r($arr);
	
	$ret = redirectPostUrl ( "http://" . $rootIp . "/" . WEB_PREFIX . "/include/command/store.cmd.php", $arr );
	// echo "ret:".$ret;
	$responseJSONFromRoot = json_decode ( $ret );
	$responseStoreInfo = array ();
	
	$statusCode = 0;
	
	if ($responseJSONFromRoot == null) {
		$statusCode = COMMON_LOGIN_STORE_CMD_ROOT_NO_RESPONSE;
		
		$varStoreId = $variableHandler->getVariable ( NODE_STORE_ID );
		$varStoreName = $variableHandler->getVariable ( NODE_STORE_NAME );
		$varStorePassword = $variableHandler->getVariable ( NODE_STORE_PASSWORD );
		
		$groupAMinValue = $variableHandler->getVariable ( "group_a_min_value", 0 );
		$groupAMaxValue = $variableHandler->getVariable ( "group_a_max_value", 0 );
		$groupBMinValue = $variableHandler->getVariable ( "group_b_min_value", 0 );
		$groupBMaxValue = $variableHandler->getVariable ( "group_b_max_value", 0 );
		$groupCMinValue = $variableHandler->getVariable ( "group_c_min_value", 0 );
		$groupCMaxValue = $variableHandler->getVariable ( "group_c_max_value", 0 );
		$groupDMinValue = $variableHandler->getVariable ( "group_d_min_value", 0 );
		$groupDMaxValue = $variableHandler->getVariable ( "group_d_max_value", 0 );
		
		if ($varStoreId == $storeId && $varStorePassword == $storePassword) {
			$responseStoreInfo ["id"] = $varStoreId;
			$responseStoreInfo ["name"] = $varStoreName;
		}
	} else if ($responseJSONFromRoot->status_code == COMMON_LOGIN_STORE_CMD_COMPLETE) {
		// $logHandler -> addLog($addr, $role, COMMON_LOGIN_STORE_CMD, " store[" . $storeId . "] login success, the status code[" . $statusCode . "]", COMMON_LOGIN_STORE_CMD_COMPLETE, "COMMON_LOGIN_STORE_CMD_COMPLETE");
		$responseStoreInfo ["id"] = $responseJSONFromRoot->data->id;
		$responseStoreInfo ["name"] = $responseJSONFromRoot->data->name;
		
		$group_a_min = $responseJSONFromRoot->data->group_a->_min;
		$group_a_max = $responseJSONFromRoot->data->group_a->_max;
		$group_b_min = $responseJSONFromRoot->data->group_b->_min;
		$group_b_max = $responseJSONFromRoot->data->group_b->_max;
		$group_c_min = $responseJSONFromRoot->data->group_c->_min;
		$group_c_max = $responseJSONFromRoot->data->group_c->_max;
		$group_d_min = $responseJSONFromRoot->data->group_d->_min;
		$group_d_max = $responseJSONFromRoot->data->group_d->_max;
		
		$variableHandler->setVariable ( "group_a_min_value", $group_a_min );
		$variableHandler->setVariable ( "group_a_max_value", $group_a_max );
		$variableHandler->setVariable ( "group_b_min_value", $group_b_min );
		$variableHandler->setVariable ( "group_b_max_value", $group_b_max );
		$variableHandler->setVariable ( "group_c_min_value", $group_c_min );
		$variableHandler->setVariable ( "group_c_max_value", $group_c_max );
		$variableHandler->setVariable ( "group_d_min_value", $group_d_min );
		$variableHandler->setVariable ( "group_d_max_value", $group_d_max );
		
		$variableHandler->setVariable ( ROOT_IP, $rootIp );
		$variableHandler->setVariable ( NODE_STORE_ID, $storeId );
		$variableHandler->setVariable ( NODE_STORE_NAME, $responseJSONFromRoot->data->name );
		$variableHandler->setVariable ( NODE_STORE_PASSWORD, $storePassword );
		
		$statusCode = COMMON_LOGIN_STORE_CMD_COMPLETE;
	} else if ($responseFromRoot->status_code == COMMON_LOGIN_STORE_CMD_STORE_DOES_NOT_EXIST) {
		$statusCode = COMMON_LOGIN_STORE_CMD_STORE_DOES_NOT_EXIST;
	}
	
	$response = new stdClass ();
	$response->data = $responseStoreInfo;
	$response->status_code = $statusCode;
	$response->command = LOGIN_STORE_CMD;
	
	if (! isset ( $variable ["return_url"] )) {
		echo json_encode ( $response );
	} else {
		// header("location:".$_SERVER["HTTP_REFERER"]);
		header ( "location:../../administrator/" . $variable ["return_url"] );
	}
	
	// echo json_encode($response);
} else if ($command == "build_env_cmd") {
	if ($variable ["host_role"] == "root") {
		importSQL ( "restaurant_root.sql" );
		$variableHandler->setVariable ( HOST_ROLE, "root" );
	} else if ($variable ["host_role"] == "node") {
		importSQL ( "restaurant_node.sql" );
		$variableHandler->setVariable ( HOST_ROLE, "node" );
	}
	header ( "location:../../administrator/configure.php" );
} else if ($command == "change_group_define_cmd") {
	
	$groupAMinValue = $variable ["group_a_min_value"];
	$groupAMaxValue = $variable ["group_a_max_value"];
	
	$groupBMinValue = $variable ["group_b_min_value"];
	$groupBMaxValue = $variable ["group_b_max_value"];
	
	$groupCMinValue = $variable ["group_c_min_value"];
	$groupCMaxValue = $variable ["group_c_max_value"];
	
	$groupDMinValue = $variable ["group_d_min_value"];
	$groupDMaxValue = $variable ["group_d_max_value"];
	
	$variableHandler->setVariable ( "group_a_min_value", $groupAMinValue );
	$variableHandler->setVariable ( "group_a_max_value", $groupAMaxValue );
	$variableHandler->setVariable ( "group_b_min_value", $groupBMinValue );
	$variableHandler->setVariable ( "group_b_max_value", $groupBMaxValue );
	$variableHandler->setVariable ( "group_c_min_value", $groupCMinValue );
	$variableHandler->setVariable ( "group_c_max_value", $groupCMaxValue );
	$variableHandler->setVariable ( "group_d_min_value", $groupDMinValue );
	$variableHandler->setVariable ( "group_d_max_value", $groupDMaxValue );
	
	header ( "location:../../administrator/group_define.php" );
} else if ($command == "get_group_define_cmd") {
	$groupAMinValue = $variableHandler->getVariable ( "group_a_min_value", 0 );
	$groupAMaxValue = $variableHandler->getVariable ( "group_a_max_value", 0 );
	$groupBMinValue = $variableHandler->getVariable ( "group_b_min_value", 0 );
	$groupBMaxValue = $variableHandler->getVariable ( "group_b_max_value", 0 );
	$groupCMinValue = $variableHandler->getVariable ( "group_c_min_value", 0 );
	$groupCMaxValue = $variableHandler->getVariable ( "group_c_max_value", 0 );
	$groupDMinValue = $variableHandler->getVariable ( "group_d_min_value", 0 );
	$groupDMaxValue = $variableHandler->getVariable ( "group_d_max_value", 0 );
	
	$data = new stdClass ();
	$data->group_a_min_value = $groupAMinValue;
	$data->group_a_max_value = $groupAMaxValue;
	$data->group_b_min_value = $groupBMinValue;
	$data->group_b_max_value = $groupBMaxValue;
	$data->group_c_min_value = $groupCMinValue;
	$data->group_c_max_value = $groupCMaxValue;
	$data->group_d_min_value = $groupDMinValue;
	$data->group_d_max_value = $groupDMaxValue;
	
	$response = new stdClass ();
	$response->command = "get_group_define_cmd";
	$response->status_code = 200;
	$response->data = $data;
	$response->mode = getMode ();
	echo json_encode ( $response );
}

?>