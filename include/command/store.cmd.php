<?php

session_start();
$position = "store_cmd";
//header ('Content-type:text/html; charset=utf-8');
require_once ("../constant/db.constant.php");
require_once (CONSTANT_PATH . "store.constant.php");
require_once (CONSTANT_PATH . "error.constant.php");
require_once (CONSTANT_PATH . "preservation.constant.php");
require_once (INCLUDE_PATH . "header.php");

global $storeHandler, $variableHandler;
$variable = $_REQUEST;
$command = $variable["command"];

/*
 define("ADD_STORE_CMD", "add_store_cmd");
 define("REMOVE_STORE_CMD", "remove_store_cmd");
 define("MODIFY_STORE_CMD", "modify_store_cmd");
 define("GET_JQGRID_STORES_CMD", "get_jqgrid_stores_cmd");
 define("GET_STORES_CMD", "get_stores_cmd");
 define("LOGIN_STORE_CMD", "login_store_cmd");
 define("GET_STORES_WITH_GROUP_WAITING_TIME_CMD", "get_stores_with_group_waiting_time_cmd");
 define("GET_JQGRID_STORE_STATUS_CMD","get_jqgrid_store_status_cmd");
 */

if ($command == ADD_STORE_CMD) {
	$insertId = -1;
	$statusCode = 0;

	if (getMode() == "root") {

		$storeName = $variable["store_name_value"];
		$storeAddress = $variable["store_address_value"];
		$storeTel = $variable["store_tel_value"];
		$storePassword = $variable["store_password_value"];
		$storeBusinessTime = $variable["store_business_time_value"];
		$storeLati = $variable["store_lati_value"];
		$storeLongi = $variable["store_longi_value"];
		$storeDescription = $variable["store_description_value"];

		$store = $storeHandler -> create(true);
		$store -> setVar("store_name", $storeName);
		$store -> setVar("store_address", $storeAddress);
		$store -> setVar("store_tel", $storeTel);
		$store -> setVar("store_password", $storePassword);
		$store -> setVar("store_business_time", $storeBusinessTime);

		$store -> setVar("store_lati", $storeLati);
		$store -> setVar("store_longi", $storeLongi);
		$store -> setVar("store_description", $storeDescription);

		$insertId = $storeHandler -> insert($store);
		if ($insertId > 0) {
			$statusCode = STATUS_CODE_ADD_STORE_CMD_SUCCESS;
		} else {
			$statusCode = STATUS_CODE_ADD_STORE_CMD_FAIL;
		}

	} else {
		$statusCode = STATUS_CODE_ADD_STORE_CMD_FAIL_WRONG_ROLE;

	}

	$addr = $_SERVER["REMOTE_ADDR"];
	$role = getMode();
	switch($statusCode) {
		case STATUS_CODE_ADD_STORE_CMD_FAIL_WRONG_ROLE :
			//server no response
			$logHandler -> addLog($addr, $role, ADD_STORE_CMD_CODE, " the role does not allow create store, the status code[" . $statusCode . "]", STATUS_CODE_ADD_STORE_CMD_FAIL_WRONG_ROLE, "COMMON_ADD_STORE_CMD_WRONG_ROLE");
			break;
		case STATUS_CODE_ADD_STORE_CMD_SUCCESS :
			//echo COMMON_ADD_USER_CMD_COMPLETE;
			$logHandler -> addLog($addr, $role, ADD_STORE_CMD_CODE, " store[" . $insertId . "] has been create, the status code[" . $statusCode . "]", STATUS_CODE_ADD_STORE_CMD_SUCCESS, "COMMON_ADD_STORE_CMD_COMPLETE");
			break;
		case STATUS_CODE_ADD_STORE_CMD_FAIL :
			//echo COMMON_ADD_USER_CMD_COMPLETE;
			$logHandler -> addLog($addr, $role, ADD_STORE_CMD_CODE, " store[" . $insertId . "] has been create, the status code[" . $statusCode . "]", STATUS_CODE_ADD_STORE_CMD_FAIL, "COMMON_ADD_STORE_CMD_COMPLETE");
			break;
		default :
			break;
	}
	$response = new stdClass;
	$response -> status_code = $statusCode;
	$response -> command = ADD_STORE_CMD;
	$response -> insert_id = $insertId;

	if (!isset($variable["return_url"])) {
		echo json_encode($response);
	} else {
		//header("location:".$_SERVER["HTTP_REFERER"]);
		header("location:../../administrator/store_management.php");
	}

} else if ($command == REMOVE_STORE_CMD) {

	$statusCode = 0;
	$storeId = -1;
	$role = getMode();

	if ($role == "root") {
		$storeId = $variable["store_id"];
		$result = $storeHandler -> get($storeId);
		if ($result) {
			$storeHandler -> removeStore($storeId);
			$statusCode = STATUS_CODE_REMOVE_STORE_CMD_SUCCESS;
			//remove complete
		} else {

			$statusCode = STATUS_CODE_REMOVE_STORE_CMD_FAIL_NOT_FOUND;
			//store does not exist
		}

	} else {
		$statusCode = STATUS_CODE_REMOVE_STORE_CMD_FAIL_WRONG_ROLE;
		//wrong role
	}

	$addr = $_SERVER["REMOTE_ADDR"];

	switch($statusCode) {
		case STATUS_CODE_REMOVE_STORE_CMD_FAIL_NOT_FOUND :
			//server no response
			$logHandler -> addLog($addr, $role, REMOVE_STORE_CMD_CODE, " the store[" . $storeId . "] does not exist, the status code[" . $statusCode . "]", STATUS_CODE_REMOVE_STORE_CMD_FAIL_NOT_FOUND, "COMMON_REMOVE_STORE_CMD_STORE_DOES_NOT_EXIST");
			break;
		case STATUS_CODE_REMOVE_STORE_CMD_FAIL_WRONG_ROLE :
			//server no response
			$logHandler -> addLog($addr, $role, REMOVE_STORE_CMD_CODE, " the role does not allow remove store, the status code[" . $statusCode . "]", STATUS_CODE_REMOVE_STORE_CMD_FAIL_WRONG_ROLE, "COMMON_REMOVE_STORE_CMD_WRONG_ROLE");
			break;
		case STATUS_CODE_REMOVE_STORE_CMD_SUCCESS :
			//echo COMMON_ADD_USER_CMD_COMPLETE;
			$logHandler -> addLog($addr, $role, REMOVE_STORE_CMD_CODE, " store[" . $insertId . "] has been removed, the status code[" . $statusCode . "]", STATUS_CODE_REMOVE_STORE_CMD_SUCCESS, "COMMON_REMOVE_STORE_CMD_COMPLETE");
			break;
		default :
			break;
	}

	$response = new stdClass;
	$response -> status_code = $statusCode;
	$response -> command = REMOVE_STORE_CMD;
	if (!isset($variable["return_url"])) {
		echo json_encode($response);
	} else {
		//header("location:".$_SERVER["HTTP_REFERER"]);
		header("location:../../administrator/store_management.php?status_code=" . $statusCode);
	}

} else if ($command == MODIFY_STORE_CMD) {

	$statusCode = 0;
	$storeId = -1;
	$role = getMode();

	if ($role == "root") {
		$storeId = $variable["store_id_value"];
		$storeName = $variable["store_name_value"];
		$storeAddress = $variable["store_address_value"];
		$storeTel = $variable["store_tel_value"];
		$storePassword = $variable["store_password_value"];
		$storeBusinessTime = $variable["store_business_time_value"];
		$storeLati = $variable["store_lati_value"];
		$storeLongi = $variable["store_longi_value"];
		$storeDescription = $variable["store_description_value"];

		$store = $storeHandler -> get($storeId);
		if ($store) {
			/*
			 $store -> setVar("store_name", $storeName);
			 $store -> setVar("store_address", $storeAddress);
			 $store -> setVar("store_tel", $storeTel);
			 $store -> setVar("store_password", $storePassword);
			 $store -> setVar("store_business_time", $storeBusinessTime);

			 $store -> setVar("store_lati", $storeLati);
			 $store -> setVar("store_longi", $storeLongi);
			 $store -> setVar("store_description", $storeDescription);
			 */
			$result = $storeHandler -> modifyStore($storeId, $storeName, $storeAddress, $storeTel, $storeBusinessTime, $storeLati, $storeLongi, $storeDescription);
			if ($result) {
				$statusCode = STATUS_CODE_MODIFY_STORE_CMD_SUCCESS;
			} else {
				$statusCode = STATUS_CODE_MODIFY_STORE_CMD_FAIL;
			}
		} else {
			$statusCode = STATUS_CODE_MODIFY_STORE_CMD_FAIL_NOT_FOUND;
		}
		//$response -> status_code = 0;
		//$response -> command = MODIFY_STORE_CMD;
		//$response -> insert_id = $storeId;
	} else {
		$statusCode = STATUS_CODE_MODIFY_STORE_CMD_FAIL_WRONG_ROLE;
		//$response -> status_code = 99;
		//$response -> command = MODIFY_STORE_CMD;
	}

	$addr = $_SERVER["REMOTE_ADDR"];
	$role = getMode();
	switch($statusCode) {
		case STATUS_CODE_MODIFY_STORE_CMD_FAIL_WRONG_ROLE :
			//server no response
			$logHandler -> addLog($addr, $role, MODIFY_STORE_CMD_CODE, " the store[" . $storeId . "] does not exist, the status code[" . $statusCode . "]", STATUS_CODE_MODIFY_STORE_CMD_FAIL_WRONG_ROLE, "COMMON_MODIFY_STORE_CMD_STORE_EXIST");
			break;
		case STATUS_CODE_MODIFY_STORE_CMD_FAIL_NOT_FOUND :
			$logHandler -> addLog($addr, $role, MODIFY_STORE_CMD_CODE, " the role does not allow remove store, the status code[" . $statusCode . "]", STATUS_CODE_MODIFY_STORE_CMD_FAIL_NOT_FOUND, "COMMON_MODIFY_STORE_CMD_WRONG_ROLE");
			break;
		case STATUS_CODE_MODIFY_STORE_CMD_FAIL :
			//echo COMMON_ADD_USER_CMD_COMPLETE;
			$logHandler -> addLog($addr, $role, MODIFY_STORE_CMD_CODE, " store[" . $insertId . "] has been removed, the status code[" . $statusCode . "]", STATUS_CODE_MODIFY_STORE_CMD_FAIL, "COMMON_MODIFY_STORE_CMD_COMPLETE");
			break;
		case STATUS_CODE_MODIFY_STORE_CMD_SUCCESS :
			//echo COMMON_ADD_USER_CMD_COMPLETE;
			$logHandler -> addLog($addr, $role, MODIFY_STORE_CMD_CODE, " store[" . $insertId . "] has been removed, the status code[" . $statusCode . "]", STATUS_CODE_MODIFY_STORE_CMD_SUCCESS, "COMMON_MODIFY_STORE_CMD_COMPLETE");
			break;
		default :
			break;
	}

	$response = new stdClass;
	$response -> status_code = $statusCode;
	$response -> command = MODIFY_STORE_CMD;

	if (!isset($variable["return_url"])) {
		echo json_encode($response);
	} else {
		//header("location:".$_SERVER["HTTP_REFERER"]);
		header("location:../../administrator/store_management.php?status_code=" . $statusCode);
	}
} else if ($command == LIST_JQGRID_STORE_CMD) {
	global $storeHandler;
	//$storeId = $_REQUEST["store_id"];

	$page = $_REQUEST['page'];
	// get the requested page
	$limit = $_REQUEST['rows'];
	// get how many rows we want to have into the grid
	$sidx = $_REQUEST['sidx'];
	// get index row - i.e. user click to sort
	$sord = $_REQUEST['sord'];
	// get the direction

	//$res = array();
	//$result = array();

	//$criteria  = new CompoCriteria(new Criteria('serial', $serial));
	$count = $storeHandler -> getStoreCount();
	$stores = $storeHandler -> getStores($limit, ($page - 1) * $limit);

	if ($count > 0) {

		$totalPages = ceil($count / $limit);
	} else {
		$totalPages = 0;
	}

	//if($race==0){
	//	return ;
	//}
	$response = new stdClass;
	$response -> page = (int)$page;
	$response -> total = $totalPages;
	$response -> records = (int)$count;
	$i = 0;
	for ($j = 0; $j < sizeof($stores); $j++) {
		$store = $stores[$i];

		$storeId = $store -> getVar("store_id");
		$storeName = $store -> getVar("store_name");
		$storeAddress = $store -> getVar("store_address");
		$storeTel = $store -> getVar("store_tel");
		$storeBusinessTime = $store -> getVar("store_business_time");
		$storeType = $store -> getVar("store_type");
		$storeCapability = $store -> getVar("store_capability");
		$storeDescription = $store -> getVar("store_description");
		$storeLati = $store -> getVar("store_lati");
		$storeLongi = $store -> getVar("store_longi");

		$response -> rows[$i]['id'] = $storeId;
		$response -> rows[$i]['cell'] = array($storeId, $storeName, $storeAddress, $storeTel, $storeDescription, $storeLati, $storeLongi);
		$i++;
	}
	echo json_encode($response);
} else if ($command == LIST_STORE_CMD) {
	$stores = $storeHandler -> getStores();
	$response = new stdClass;
	$response -> status_code = 200;

	$storesJson = array();
	for ($j = 0; $j < sizeof($stores); $j++) {
		$store = $stores[$j];

		$storeId = $store -> getVar("store_id");
		$storeName = $store -> getVar("store_name");
		$storeAddress = $store -> getVar("store_address");
		$storeTel = $store -> getVar("store_tel");
		$storeBusinessTime = $store -> getVar("store_business_time");
		$storeType = $store -> getVar("store_type");
		$storeCapability = $store -> getVar("store_capability");
		$storeDescription = $store -> getVar("store_description");
		$storeLati = $store -> getVar("store_lati");
		$storeLongi = $store -> getVar("store_longi");

		$storeJson["id"] = $storeId;
		$storeJson["name"] = $storeName;
		$storeJson["address"] = $storeAddress;
		$storeJson["tel"] = $storeTel;
		$storeJson["business_time"] = $storeBusinessTime;
		$storeJson["type"] = $storeType;
		$storeJson["cap"] = $storeCapability;
		$storeJson["description"] = $storeDescription;
		$storeJson["lati"] = $storeLati;
		$storeJson["longi"] = $storeLongi;

		$storesJson[] = $storeJson;
	}
	$response -> data = $storesJson;

	echo json_encode($response);
} else if ($command == LIST_STORE_WITH_GROUP_WAITING_TIME_CMD) {
	$groupIndex = $_REQUEST["group_index"];

	$stores = $storeHandler -> getStores();
	$response = new stdClass;
	$response -> status_code = 200;

	$storesJson = array();
	for ($j = 0; $j < sizeof($stores); $j++) {
		$store = $stores[$j];

		$storeId = $store -> getVar("store_id");
		$storeName = $store -> getVar("store_name");
		$storeAddress = $store -> getVar("store_address");
		$storeTel = $store -> getVar("store_tel");
		$storeBusinessTime = $store -> getVar("store_business_time");
		$storeType = $store -> getVar("store_type");
		$storeCapability = $store -> getVar("store_capability");
		$storeDescription = $store -> getVar("store_description");
		$storeLati = $store -> getVar("store_lati");
		$storeLongi = $store -> getVar("store_longi");

		$groupCount = $preservationHandler -> getCountByStoreIdAndGroupIdAndStatus($storeId, $groupIndex, PRESERVATION_STATUS_WAIT);
		$groupCurrent = $preservationHandler -> getLastRecordByStoreIdAndGroupIdAndStatus($storeId, $groupIndex, PRESERVATION_STATUS_SEAT);

		//$count = $preservationHandler -> getCountByStoreIdAndGroupIdAndStatus($storeId, $groupIndex, $status);
		$waitingTime = 0;
		if ($groupIndex == 0) {

			//$groupCount = $preservationHandler->getCountByStoreIdAndGroupIdAndStatus($storeId, 0, PRESERVATION_STATUS_WAIT);
			//$groupCurrent = $preservationHandler->getLastRecordByStoreIdAndGroupIdAndStatus($storeId, 0, PRESERVATION_STATUS_SEAT);
			//$waitingTime = getWaittimeOfCountByGroupA($groupACount);

			$waitingTime = getWaittimeOfCountByGroupA($groupCount);
		} else if ($groupIndex == 1) {
			$waitingTime = getWaittimeOfCountByGroupB($groupCount);
		} else if ($groupIndex == 2) {
			$waitingTime = getWaittimeOfCountByGroupC($groupCount);
		} else if ($groupIndex == 3) {
			$waitingTime = getWaittimeOfCountByGroupD($groupCount);
		}

		$ticketLabel = "";
		if ($groupCurrent != null) {
			$ticketLabel = $groupCurrent -> getTicketLabel();
		}

		$storeJson["id"] = $storeId;
		$storeJson["name"] = $storeName;
		$storeJson["address"] = $storeAddress;
		$storeJson["tel"] = $storeTel;
		$storeJson["business_time"] = $storeBusinessTime;
		$storeJson["type"] = $storeType;
		$storeJson["cap"] = $storeCapability;
		$storeJson["description"] = $storeDescription;
		$storeJson["lati"] = $storeLati;
		$storeJson["longi"] = $storeLongi;

		$storeJson["group_index"] = $groupIndex;
		$storeJson["current_ticket"] = $ticketLabel;
		$storeJson["group_count"] = $groupCount;
		$storeJson["group_waiting_time"] = $waitingTime;

		$storesJson[] = $storeJson;
	}
	$response -> data = $storesJson;

	echo json_encode($response);
} else if ($command == LOGIN_STORE_CMD) {
	//$response = new stdClass;
	if (getMode() == "root") {

		$storeId = $variable["store_id"];
		$storePassword = $variable["store_password"];

		$tempStore = $storeHandler -> get($storeId);
		$storeJson = array();
		if ($tempStore) {
			$store = $storeHandler -> login($storeId, $storePassword);

			if ($store) {
				$storeId = $store -> getVar("store_id");
				$storeName = $store -> getVar("store_name");
				$storeAddress = $store -> getVar("store_address");
				$storeTel = $store -> getVar("store_tel");
				$storeBusinessTime = $store -> getVar("store_business_time");
				$storeType = $store -> getVar("store_type");
				$storeCapability = $store -> getVar("store_capability");
				$storeDescription = $store -> getVar("store_description");
				$storeLati = $store -> getVar("store_lati");
				$storeLongi = $store -> getVar("store_longi");

				$storeJson = array();
				$storeJson["id"] = $storeId;
				$storeJson["name"] = $storeName;
				$storeJson["address"] = $storeAddress;
				$storeJson["tel"] = $storeTel;
				$storeJson["business_time"] = $storeBusinessTime;
				$storeJson["type"] = $storeType;
				$storeJson["cap"] = $storeCapability;
				$storeJson["description"] = $storeDescription;
				$storeJson["lati"] = $storeLati;
				$storeJson["longi"] = $storeLongi;

				$storeJson["group_a"]["_min"] = $variableHandler -> getVariable("group_a_min_value");
				$storeJson["group_a"]["_max"] = $variableHandler -> getVariable("group_a_max_value");
				$storeJson["group_b"]["_min"] = $variableHandler -> getVariable("group_b_min_value");
				$storeJson["group_b"]["_max"] = $variableHandler -> getVariable("group_b_max_value");
				$storeJson["group_c"]["_min"] = $variableHandler -> getVariable("group_c_min_value");
				$storeJson["group_c"]["_max"] = $variableHandler -> getVariable("group_c_max_value");
				$storeJson["group_d"]["_min"] = $variableHandler -> getVariable("group_d_min_value");
				$storeJson["group_d"]["_max"] = $variableHandler -> getVariable("group_d_max_value");

				$statusCode = STATUS_CODE_LOGIN_STORE_CMD_SUCCESS;
			} else {
				$statusCode = STATUS_CODE_LOGIN_STORE_CMD_FAIL_WRONG_PASSWORD;
				//login fail, wrong password.
			}
		} else {
			$statusCode = STATUS_CODE_LOGIN_STORE_CMD_FAIL_NOT_FOUND;
			//login fail, store id does not exist.
		}
	} else {
		$statusCode = STATUS_CODE_LOGIN_STORE_CMD_FAIL_WRONG_ROLE;
	}
	$addr = $_SERVER["REMOTE_ADDR"];
	$role = getMode();
	switch($statusCode) {
		case STATUS_CODE_LOGIN_STORE_CMD_FAIL_WRONG_PASSWORD :
			//store password error
			$logHandler -> addLog($addr, $role, LOGIN_STORE_CMD_CODE, " the store[" . $storeId . "] login fail, the status code[" . $statusCode . "]", STATUS_CODE_LOGIN_STORE_CMD_FAIL_WRONG_PASSWORD, "COMMON_LOGIN_STORE_CMD_WRONG_STORE_PASSWORD");
			break;
		case STATUS_CODE_LOGIN_STORE_CMD_FAIL_NOT_FOUND :
			//server no response
			$logHandler -> addLog($addr, $role, LOGIN_STORE_CMD_CODE, " the store[" . $storeId . "] login fail, the status code[" . $statusCode . "]", STATUS_CODE_LOGIN_STORE_CMD_FAIL_NOT_FOUND, "COMMON_LOGIN_STORE_CMD_STORE_DOES_NOT_EXIST");
			break;
		case STATUS_CODE_LOGIN_STORE_CMD_SUCCESS :
			//echo COMMON_ADD_USER_CMD_COMPLETE;
			$logHandler -> addLog($addr, $role, LOGIN_STORE_CMD_CODE, " store[" . $storeId . "] login success, the status code[" . $statusCode . "]", STATUS_CODE_LOGIN_STORE_CMD_SUCCESS, "COMMON_LOGIN_STORE_CMD_COMPLETE");
			break;
		case STATUS_CODE_LOGIN_STORE_CMD_FAIL_WRONG_ROLE :
			//echo COMMON_ADD_USER_CMD_COMPLETE;
			$logHandler -> addLog($addr, $role, LOGIN_STORE_CMD_CODE, " store[" . $storeId . "] login success, the status code[" . $statusCode . "]", STATUS_CODE_LOGIN_STORE_CMD_FAIL_WRONG_ROLE, "COMMON_LOGIN_STORE_CMD_COMPLETE");
			break;
		default :
			break;
	}

	$response = new stdClass;
	$response -> data = $storeJson;
	$response -> status_code = $statusCode;
	$response -> command = LOGIN_STORE_CMD;

	echo json_encode($response);
	
} else if ($command == LIST_JQGRID_STORE_STATUS_CMD) {
	global $storeHandler;
	global $preservationHandler;
	//$storeId = $_REQUEST["store_id"];

	$page = $_REQUEST['page'];
	// get the requested page
	$limit = $_REQUEST['rows'];
	// get how many rows we want to have into the grid
	$sidx = $_REQUEST['sidx'];
	// get index row - i.e. user click to sort
	$sord = $_REQUEST['sord'];
	// get the direction

	//$res = array();
	//$result = array();

	$today = getdate();
	$year = $today["year"];
	$mon = $today["mon"];
	$dayofmon = $today["mday"];

	$timeStart = mktime(0, 0, 0, $mon, $dayofmon, $year);
	$timeEnd = mktime(23, 59, 59, $mon, $dayofmon, $year);

	//$criteria  = new CompoCriteria(new Criteria('serial', $serial));
	$count = $storeHandler -> getStoreCount();
	$stores = $storeHandler -> getStores($limit, ($page - 1) * $limit);

	if ($count > 0) {

		$totalPages = ceil($count / $limit);
	} else {
		$totalPages = 0;
	}

	//if($race==0){
	//	return ;
	//}
	$response = new stdClass;
	$response -> page = (int)$page;
	$response -> total = $totalPages;
	$response -> records = (int)$count;
	$i = 0;
	for ($j = 0; $j < sizeof($stores); $j++) {
		$store = $stores[$i];

		$storeId = $store -> getVar("store_id");
		$storeName = $store -> getVar("store_name");

		$groupACount = $preservationHandler -> getCountByStoreIdAndGroupIdAndStatus($storeId, 0, PRESERVATION_STATUS_WAIT, $timeStart, $timeEnd);
		$groupACurrent = $preservationHandler -> getLastRecordByStoreIdAndGroupIdAndStatus($storeId, 0, PRESERVATION_STATUS_SEAT, $timeStart, $timeEnd);
		$groupAWaitingTime = getWaittimeOfCountByGroupA($groupACount);

		$ticketA = "";
		if ($groupACurrent != null) {
			$ticketA = $groupACurrent -> getTicketLabel();
		}

		$bufferA = $groupACount . " group<br/>" . $ticketA . "<br/>" . $groupAWaitingTime . " minutes";

		$groupBCount = $preservationHandler -> getCountByStoreIdAndGroupIdAndStatus($storeId, 1, PRESERVATION_STATUS_WAIT, $timeStart, $timeEnd);
		$groupBCurrent = $preservationHandler -> getLastRecordByStoreIdAndGroupIdAndStatus($storeId, 1, PRESERVATION_STATUS_SEAT, $timeStart, $timeEnd);
		$groupBWaitingTime = getWaittimeOfCountByGroupB($groupBCount);

		$ticketB = "";
		if ($groupBCurrent != null) {
			$ticketB = $groupBCurrent -> getTicketLabel();
		}

		$bufferB = $groupBCount . " group<br/>" . $ticketB . "<br/>" . $groupBWaitingTime . " minutes";
		//echo "buffer C";
		$groupCCount = $preservationHandler -> getCountByStoreIdAndGroupIdAndStatus($storeId, 2, PRESERVATION_STATUS_WAIT, $timeStart, $timeEnd);
		$groupCCurrent = $preservationHandler -> getLastRecordByStoreIdAndGroupIdAndStatus($storeId, 2, PRESERVATION_STATUS_SEAT, $timeStart, $timeEnd);
		$groupCWaitingTime = getWaittimeOfCountByGroupC($groupCCount);

		$ticketC = "";
		if ($groupCCurrent != null) {
			$ticketC = $groupCCurrent -> getTicketLabel();
		}

		$bufferC = $groupCCount . " group<br/>" . $ticketC . "<br/>" . $groupCWaitingTime . " minutes";

		$groupDCount = $preservationHandler -> getCountByStoreIdAndGroupIdAndStatus($storeId, 3, PRESERVATION_STATUS_WAIT, $timeStart, $timeEnd);
		$groupDCurrent = $preservationHandler -> getLastRecordByStoreIdAndGroupIdAndStatus($storeId, 3, PRESERVATION_STATUS_SEAT, $timeStart, $timeEnd);
		$groupDWaitingTime = getWaittimeOfCountByGroupA($groupDCount);

		$ticketD = "";
		if ($groupDCurrent != null) {
			$ticketD = $groupDCurrent -> getTicketLabel();
		}

		$bufferD = $groupDCount . " group<br/>" . $ticketD . "<br/>" . $groupDWaitingTime . " minutes";

		$response -> rows[$i]['id'] = $storeId;
		$response -> rows[$i]['cell'] = array($storeId, $storeName, $bufferA, $bufferB, $bufferC, $bufferD);
		$i++;
	}
	echo json_encode($response);
}
?>