<?php

session_start();
$position = "store_cmd";
//header ('Content-type:text/html; charset=utf-8');
require_once ("../constant/db.constant.php");
require_once (CONSTANT_PATH . "store.constant.php");
require_once (CONSTANT_PATH . "preservation.constant.php");
require_once (INCLUDE_PATH . "header.php");

global $logHandler;
$variable = $_REQUEST;
$command = $variable["command"];

if ($command == LIST_JQGRID_LOG_CMD) {
	global $logHandler;
	//$storeId = $_REQUEST["store_id"];

	$page = $_REQUEST['page'];
	// get the requested page
	$limit = $_REQUEST['rows'];
	// get how many rows we want to have into the grid
	$sidx = $_REQUEST['sidx'];
	// get index row - i.e. user click to sort
	$sord = $_REQUEST['sord'];
	// get the direction

	//$res = array();
	//$result = array();

	//$criteria  = new CompoCriteria(new Criteria('serial', $serial));
	$count = $logHandler -> getCount();
	$logs = $logHandler -> getLogs($limit, ($page-1) * $limit);

	if ($count > 0) {

		$totalPages = ceil($count / $limit);
	} else {
		$totalPages = 0;
	}

	//if($race==0){
	//	return ;
	//}
	$response = new stdClass;
	$response -> page = (int)$page;
	$response -> total = $totalPages;
	$response -> records = (int)$count;
	$i = 0;
	for ($j = 0; $j < sizeof($logs); $j++) {
		$log = $logs[$i];

		$logId = $log->getVar("log_record_id");
		$logType = $log->getVar("log_record_type");
		$logNode = $log->getVar("log_record_mode");
		$logHost = $log->getVar("log_record_src_host");
		$logRole = $log->getVar("log_record_role");
		$logUserId = $log->getVar("log_record_user_id");
		$logStoreId = $log->getVar("log_record_store_id");
		$logOperation = $log->getVar("log_record_operation");
		$logDescription = $log->getVar("log_record_description");
		$logStatusCode = $log->getVar("log_record_result_status_code");
		$logResultDescription = $log->getVar("log_record_result_description");
		$logTime = $log->getVar("log_record_timestamp");

		$response -> rows[$i]['id'] = $logId;
		$response -> rows[$i]['cell'] = array(
			$logId,
			$logType,
			$logHost, 
			$logStoreId, 
			$logOperation, 
			$logStatusCode, 
			$logResultDescription,
			$logTime);
		$i++;
	}
	echo json_encode($response);
}
?>