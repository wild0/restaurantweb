<?php

session_start();
$position = "ad_cmd";
//header ('Content-type:text/html; charset=utf-8');
require_once ("../constant/db.constant.php");
require_once (CONSTANT_PATH . "card.constant.php");
require_once (INCLUDE_PATH . "header.php");

global $adHandler, $logHandler, $storeHandler;
$variable = $_REQUEST;
$command = $variable["command"];
if($command==REMOVE_CARD_CMD){
	$cardId = $variable["card_id_value"];
	$result = $cardHandler -> remove($cardId);
	
	$response = new stdClass;
	$response -> status_code = COMMON_REMOVE_CARD_CMD_COMPLETE;
	$response -> command = REMOVE_CARD_CMD;
	

	if (!isset($variable["return_url"])) {
		echo json_encode($response);
	} else {
		//header("location:".$_SERVER["HTTP_REFERER"]);
		header("location:../../administrator/card_management.php");
	}
	
}
if ($command == ADD_CARD_CMD) {

	$cardCode = $variable["card_number_value"];
	$cardStoreId = $variableHandler->getVariable(NODE_STORE_ID);
	$cardPermission = $variable["card_number_permission_value"];
	$cardDescription = $variable["card_number_description_value"];

	//echo $cardCode.";".$cardPermission.";".$cardDescription;

	//add log
	//$addr = $_SERVER["REMOTE_ADDR"];
	//$role = getMode();
	//$logHandler->addLog($addr, $role, 101,"add ad to server");

	$card = $cardHandler -> create(true);
	$card -> setVar("card_code", $cardCode);
	$card -> setVar("card_store_id", $cardStoreId);
	$card -> setVar("card_permission", $cardPermission);
	$card -> setVar("card_description", $cardDescription);

	$cardId = $cardHandler -> insert($card);

	$response = new stdClass;
	$response -> status_code = COMMON_ADD_CARD_CMD_COMPLETE;
	$response -> command = ADD_CARD_CMD;
	$response -> insert_id = $cardId;

	if (!isset($variable["return_url"])) {
		echo json_encode($response);
	} else {
		//header("location:".$_SERVER["HTTP_REFERER"]);
		header("location:../../administrator/card_management.php");
	}
} else if ($command == MODIFY_CARD_CMD) {

	$cardId = $variable["card_id_value"];
	//$cardStoreId = $variable["card_store_id_value"];
	$cardStoreId = $variableHandler->getVariable(NODE_STORE_ID);
	$cardCode = $variable["card_code_value"];
	$cardPermission = $variable["card_permission_value"];
	$cardDescription = $variable["card_description_value"];

	//add log
	//$addr = $_SERVER["REMOTE_ADDR"];
	//$role = getMode();
	//$logHandler->addLog($addr, $role, 102,"modify ad to server");

	$card = $cardHandler -> get($cardId);
	$card -> setVar("card_id", $cardCode);
	$card -> setVar("card_store_id", $cardStoreId);
	$card -> setVar("card_code", $cardCode);
	$card -> setVar("card_permission", $cardPermission);
	$card -> setVar("card_description", $cardDescription);

	$cardHandler -> replace($card);
	
	$response = new stdClass;
	$response -> status_code = COMMON_MODIFY_CARD_CMD_COMPLETE;
	$response -> command = MODIFY_CARD_CMD;
	$response -> insert_id = $cardId;

	if (!isset($variable["return_url"])) {
		echo json_encode($response);
	} else {
		//header("location:".$_SERVER["HTTP_REFERER"]);
		header("location:../../administrator/card_management.php");
	}
	
} else if ($command == LIST_CARD_CMD) {

	//add log
	//$addr = $_SERVER["REMOTE_ADDR"];
	//$role = getMode();
	//$logHandler->addLog($addr, $role, 103,"GET_ADVERTISEMENTS_CMD");

	$adsJSON = array();
	$cards = $cardHandler -> getAllCards();
	for ($j = 0; $j < sizeof($cards); $j++) {
		$card = $cards[$j];

		$cardId = $card -> getVar("card_id");
		$cardStoreId = $card -> getVar("card_store_id");
		$cardCode = $card -> getVar("card_code");
		$cardPermission = $card -> getVar("card_persmission");
		$cardDescription = $card -> getVar("card_description");

		$cardJSON = new stdClass;
		$cardJSON -> card_id = $cardId;
		$cardJSON -> card_store_id = $cardStoreId;
		$cardJSON -> card_code = $cardCode;
		$cardJSON -> card_permission = $cardPermission;
		$cardJSON -> card_description = $cardDescription;

		$cardsJSON[] = $cardJSON;
	}
	$response = new stdClass;
	$response -> command = GET_CARDS_CMD;
	$response -> data = $cardsJSON;
	$response -> status_code = 200;

	echo json_encode($response);

} else if ($command == LIST_JQGRID_CARD_CMD) {
	$storeId = $variableHandler->getVariable(NODE_STORE_ID);
	//add log
	//$addr = $_SERVER["REMOTE_ADDR"];
	//$role = getMode();
	//$logHandler->addLog($addr, $role, 105,"GET_ADVERTISEMENTS_CMD");

	//$storeId = $_REQUEST["store_id"];

	$page = $_REQUEST['page'];
	// get the requested page
	$limit = $_REQUEST['rows'];
	// get how many rows we want to have into the grid
	$sidx = $_REQUEST['sidx'];
	// get index row - i.e. user click to sort
	$sord = $_REQUEST['sord'];
	// get the direction

	//$res = array();
	//$result = array();

	//$criteria  = new CompoCriteria(new Criteria('serial', $serial));
	$count = $cardHandler -> getCount();
	$cards = $cardHandler -> getByStoreId($storeId, $limit, ($page-1) * $limit);

	if ($count > 0) {

		$totalPages = ceil($count / $limit);
	} else {
		$totalPages = 0;
	}
	//echo sizeof($cards);
	//if($race==0){
	//	return ;
	//}
	
	$response = new stdClass;
	$response -> page = (int)$page;
	$response -> total = $totalPages;
	$response -> records = (int)$count;
	$i = 0;
	for ($j = 0; $j < sizeof($cards); $j++) {
		//echo sizeof($cards).":".$j;
		$card = $cards[$i];

		$cardId = $card -> getVar("card_id");
		$cardStoreId = $card -> getVar("card_store_id");
		$cardCode = $card -> getVar("card_code");
		$cardPermission = $card -> getVar("card_permission");
		$cardDescription = $card -> getVar("card_description");

		$response -> rows[$i]['id'] = $cardId;
		$response -> rows[$i]['cell'] = array($cardId, $cardCode, $cardPermission, $cardDescription);
		$i++;
	}
	echo json_encode($response);
} else if ($command == CHECK_CARD_CODE_CMD) {
	$cardStoreId = $variable["card_store_id_value"];
	$cardCode = $variable["card_code_value"];

	$card = $cardHandler -> getByCardCode($cardCode, $cardStoreId);
	
	$cardJSON = new stdClass;
	if($card!=null){
		$cardJSON = new stdClass;
		$cardJSON -> card_id = $cardId;
		$cardJSON -> card_store_id = $cardStoreId;
		$cardJSON -> card_code = $cardCode;
		$cardJSON -> card_permission = $cardPermission;
		$cardJSON -> card_description = $cardDescription;
	}
	
	$response = new stdClass;
	$response -> command = CHECK_CARD_CODE_CMD;
	$response -> data = $cardJSON;
	$response -> status_code = 200;

	echo json_encode($response);
}
?>