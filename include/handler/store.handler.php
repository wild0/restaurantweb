<?php
class StoreHandler extends ObjectHandler{
	//basic operation
	function &create($isNew = true){
		$store = new Store();
        if ($isNew) {
            $store->setNew();
        }
        return $store;
	}
	function insert(&$store){
		if (!$store->cleanVars()) {
            return false;
        }
        foreach ($store->cleanVars as $k => $v) {
            ${$k} = $v;
        }
		$template = "INSERT INTO %s (";
		$template .= "store_id, ";
		$template .= "store_name, ";
		$template .= "store_address, ";
		$template .= "store_tel, ";
		$template .= "store_password, ";
		$template .= "store_business_time, ";
		$template .= "store_type, ";
		$template .= "store_capability, ";
		$template .= "store_description, ";
		$template .= "store_lati, ";
		$template .= "store_longi,";
		$template .= "store_host";
		
		$template .= ") VALUES ( %d, %s, %s,%s,%s,%s,%d,%d, %s, %f,%f, %s)";
		
		$sql = sprintf($template, STORE_TABLE,
		$store_id,
		$this->db->quoteString($store_name),
		$this->db->quoteString($store_address),
		$this->db->quoteString($store_tel),
		$this->db->quoteString($store_password),
		$this->db->quoteString($store_business_time),
		$store_type,
		$store_capability,
		$this->db->quoteString($store_description),
		$store_lati,
		$store_longi,
		$this->db->quoteString($store_host)
		);
		//echo $sql ;
		$result = $this->db->queryF($sql);
		$storeId = $this->db->getInsertId();
		
		return $storeId;
	}

	function replace($store){
		if (!$store->cleanVars()) {
            return false;
        }
        foreach ($store->cleanVars as $k => $v) {
            ${$k} = $v;
        }
		$template = "REPLACE INTO %s (";
		$template .= "store_id, ";
		$template .= "store_name, ";
		$template .= "store_address, ";
		$template .= "store_tel, ";
		$template .= "store_password, ";
		$template .= "store_business_time, ";
		$template .= "store_type, ";
		$template .= "store_capability, ";
		$template .= "store_description, ";
		$template .= "store_lati, ";
		$template .= "store_longi,";
		$template .= "store_host";
		
		$template .= ") VALUES ( %d, %s, %s,%s,%s,%s,%d,%d, %s, %f,%f, %s)";
		
		$sql = sprintf($template, STORE_TABLE,
		$store_id,
		$this->db->quoteString($store_name),
		$this->db->quoteString($store_address),
		$this->db->quoteString($store_tel),
		$this->db->quoteString($store_password),
		$this->db->quoteString($store_business_time),
		$store_type,
		$store_capability,
		$this->db->quoteString($store_description),
		$store_lati,
		$store_longi,
		$this->db->quoteString($store_host)
		);
		//echo $sql ;
		$result = $this->db->queryF($sql);
		//$storeId = $this->db->getInsertId();
		
		return $storeId;
	}

	function update($pairValues, $criteria){
		foreach ($pairValues as $k => $v) {
    		${$k} = $v;
    	}
    	$sql = "UPDATE ".STORE_TABLE." SET ";
    
    	for($i=0;$i<sizeof($pairValues);$i++){
    		if($i==0){
    			$sql .= ($k)."=".$this->db->quoteString($v);
    		}
    		else{
    			$sql .= ", ".($k)."=".$this->db->quoteString($v);
    		}
    	}
    
    	if (isset($criteria) && is_subclass_of($criteria, 'criteriaelement')) {
    		$sql .= ' '.$criteria->renderWhere();
    			
    	}
    	$result = $this->db->queryF($sql);
    	return $result;
	}
	
	function delete(&$criteria){
		$ret = array();
        $sql = 'DELETE FROM '.STORE_TABLE;
        if (isset($criteria) && is_subclass_of($criteria, 'criteriaelement')) {
            $sql .= ' '.$criteria->renderWhere();
            
        }
		//echo $sql;
        $result = $this->db->queryF($sql);
        return $result;
	}
	function getObjects($criteria = null, $limit=0, $start=-1)
    {
        $ret = array();
        $sql = 'SELECT * FROM '.STORE_TABLE;
        if (isset($criteria) && is_subclass_of($criteria, 'criteriaelement')) {
            $sql .= ' '.$criteria->renderWhere();
            if ($criteria->getSort() != '') {
                $sql .= ' ORDER BY '.$criteria->getSort().' '.$criteria->getOrder();
            }
        }
		//echo $sql;
        $result = $this->db->queryF($sql,$limit,$start);
        if (!$result){
            return $ret;
        }
        while ($myrow = $this->db->fetchArray($result)) {
            $store = $this->create(true);
            $store->assignVars($myrow);
            $ret[] =& $store;
            unset($store);
        }
        return $ret;
    }
	function getCount($criteria = null)
    {
    	
        $ret = array();
        //$limit = $start = 0;
        $sql = 'SELECT COUNT(*) AS count FROM '.STORE_TABLE;
        //print_r($criteria);
        if (isset($criteria) && is_subclass_of($criteria, 'criteriaelement')) {
            $sql .= ' '.$criteria->renderWhere();
            
        }
        $result = $this->db->queryF($sql);
        if (!$result){
            return 0;
        }
        //echo $sql;
        while ($myrow = $this->db->fetchArray($result)) {
       
            return $myrow["count"];
        }
        return 0;
    }
	////////////////////////////////////////////////////////////////
	
	
	
	
	function get($storeId){
		$criteria = new CriteriaCompo(new Criteria('store_id', $storeId));
		$results = $this->getObjects($criteria, 1, 0);
		if(sizeof($results)>0){
			return $results[0];
		}
		else{
			return false;
		}
	}
	function addStore($name, $address, $tel,$businessTime,  $lati, $longi, $description){
		$store = $this->create(true);
		$store->setVar("store_name", $name);
		$store->setVar("store_address", $address);
		$store->setVar("store_tel", $tel);
		$store->setVar("store_business_time", $businessTime);
		$store->setVar("store_description", $description);
		$store->setVar("store_lati", $lati);
		$store->setVar("store_longi", $longi);
		
		return $this->insert($store);
	}
	function modifyStore($id, $name, $address, $tel, $businessTime, $lati, $longi, $description){
		$store = $this->get($id);
		$store->setVar("store_name", $name);
		$store->setVar("store_address", $address);
		$store->setVar("store_tel", $tel);
		$store->setVar("store_business_time", $businessTime);
		$store->setVar("store_description", $description);
		$store->setVar("store_lati", $lati);
		$store->setVar("store_longi", $longi);
		
		return $this->update($store);
	}
	function removeStore($storeId){
		$criteria = new CriteriaCompo(new Criteria('store_id', $storeId));
		$this->delete($criteria);
		
	}
	
	function login($storeId, $storePassword){
		$criteria = new CriteriaCompo(new Criteria('store_id', $storeId));
		$criteria->add(new Criteria('store_password', $storePassword));
		$stores = $this->getObjects($criteria, 1, 0);
		if(sizeof($stores)>0){
			return $stores[0];
		}
		else{
			return false;
		}
	}
	function getStores($limit = 0, $start = -1){
		$stores = $this->getObjects(null, $limit, $start);
		return $stores;
	}
	function getStoreCount(){
		$count = $this->getCount();
		return $count;
	}
}
?>