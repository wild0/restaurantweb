<?php
class VariableHandler {
	var $db;
	function VariableHandler(&$db)
    {
        $this->db =& $db;
    }
    function getDb(){
    	
    	return $this->db;
    }
	function setVariable($key, $value){
		$template = "SELECT * FROM %s WHERE variable_key=%s";
		
		$sql = sprintf($template, VARIABLE_TABLE, $this->db->quoteString($key));
		
		
		$result = $this->db->queryF($sql);
		if($this->db->getRowsNum($result)==0){
			
			$insertTemplate = "INSERT INTO %s (variable_key, variable_value) VALUES (%s, %s)";
			$insertSql = sprintf($insertTemplate, VARIABLE_TABLE, $this->db->quoteString($key),
			$this->db->quoteString($value));
			$result = $this->db->queryF($insertSql);
			
			//echo $this->db->errno();
			
		}
		else{
			$updateTemplate = "UPDATE %s SET variable_value=%s WHERE variable_key=%s";
			$updateSql = sprintf($updateTemplate, VARIABLE_TABLE, $this->db->quoteString($value), 
			$this->db->quoteString($key));
			$result = $this->db->queryF($updateSql);
			
		}
	}
	function getVariable($key, $default=""){
		$template = "SELECT * FROM %s WHERE variable_key=%s";
		
		$sql = sprintf($template, VARIABLE_TABLE, $this->db->quoteString($key));
		
		$result = $this->db->queryF($sql);
		if($this->db->getRowsNum($result)==0){
			return $default;
		}
		else{
			$myrow = $this->db->fetchArray($result);
			return $myrow["variable_value"];
		}
		
	}
	
}

?>