<?php
class PreservationRecordHandler extends ObjectHandler{
	//basic operation
	function &create($isNew = true){
		$preservationRecord = new PreservationRecord();
        if ($isNew) {
            $preservationRecord->setNew();
        }
        return $preservationRecord;
	}
	function insert(&$preservationRecord){
		if (!$preservationRecord->cleanVars()) {
            return false;
        }
        foreach ($preservationRecord->cleanVars as $k => $v) {
            ${$k} = $v;
        }
		//$playerActivateCode = md5($player_name.$player_uname.$player_email);
        // RMV-NOTIFY
        // Added two fields, notify_method, notify_mode
        //$temp = "REPLACE INTO %s (";
        $template = "INSERT INTO %s (";
		//$template .= "preservation_record_id, ";
		$template .= "preservation_record_store_id, ";
		$template .= "preservation_record_type, ";
		$template .= "preservation_record_timestamp, ";
		$template .= "preservation_record_ticket, ";
		$template .= "preservation_record_group, ";
		$template .= "preservation_record_person_count, ";
		$template .= "preservation_record_tel, ";
		$template .= "preservation_record_result, ";
		$template .= "preservation_record_splitable, ";
		$template .= "preservation_record_comment, ";
		$template .= "preservation_record_status, ";
		$template .= "preservation_record_sync,";
		$template .= "preservation_record_unique_id ";
		$template .= ") VALUES ( %d, %d,%d,%d,%d,%d, %s, %d,%d, %s, %d, %d,%s)";
		
		$sql = sprintf($template, PRESERVATION_RECORD_TABLE,
		//$preservation_record_id,
		$preservation_record_store_id,
		$preservation_record_type,
		$preservation_record_timestamp,
		$preservation_record_ticket,
		$preservation_record_group,
		$preservation_record_person_count,
		$this->db->quoteString($preservation_record_tel),
		$preservation_record_result,
		$preservation_record_splitable,
		$this->db->quoteString($preservation_record_comment),
		$preservation_record_status,
		$preservation_record_sync,
		$this->db->quoteString($preservation_record_unique_id)
		);
		//echo $sql ;
		$result = $this->db->queryF($sql);
		$preservationRecordId = $this->db->getInsertId();
		
		
		
		return $preservationRecordId;
	}
	
	function replace($preservationRecord){
		if (!$preservationRecord->cleanVars()) {
            return false;
        }
        foreach ($preservationRecord->cleanVars as $k => $v) {
            ${$k} = $v;
        }
		//$playerActivateCode = md5($player_name.$player_uname.$player_email);
        // RMV-NOTIFY
        // Added two fields, notify_method, notify_mode
        //$temp = "REPLACE INTO %s (";
        //echo "id:".$preservation_record_id;
        $template = "REPLACE INTO %s (";
		$template .= "preservation_record_id, ";
		$template .= "preservation_record_store_id, ";
		$template .= "preservation_record_type, ";
		$template .= "preservation_record_timestamp, ";
		$template .= "preservation_record_ticket, ";
		$template .= "preservation_record_group, ";
		$template .= "preservation_record_person_count, ";
		$template .= "preservation_record_tel, ";
		$template .= "preservation_record_result, ";
		$template .= "preservation_record_splitable, ";
		$template .= "preservation_record_comment, ";
		$template .= "preservation_record_status, ";
		$template .= "preservation_record_sync,";
		$template .= "preservation_record_unique_id ";
		$template .= ") VALUES ( %s, %d, %d,%d,%d,%d,%d, %s, %d,%d, %s, %d, %d, %s)";
		
		$sql = sprintf($template, PRESERVATION_RECORD_TABLE,
		$preservation_record_id,
		$preservation_record_store_id,
		$preservation_record_type,
		$preservation_record_timestamp,
		$preservation_record_ticket,
		$preservation_record_group,
		$preservation_record_person_count,
		$this->db->quoteString($preservation_record_tel),
		$preservation_record_result,
		$preservation_record_splitable,
		$this->db->quoteString($preservation_record_comment),
		$preservation_record_status,
		$preservation_record_sync,
		$this->db->quoteString($preservation_record_unique_id)
		);
		//echo $sql ;
		$result = $this->db->queryF($sql);
		$preservationRecordId = $this->db->getInsertId();
		
		
		
		return $preservationRecordId;
	}
	function update($preservation){
		
		if (!$preservation->cleanVars()) {
            return false;
        }
        foreach ($preservation->cleanVars as $k => $v) {
            ${$k} = $v;
        }
		
		$template = "UPDATE %s SET ";
		$template .= "preservation_record_store_id =%d,  ";
		$template .= "preservation_record_type =%d,  ";
		$template .= "preservation_record_timestamp =%d,  ";
		$template .= "preservation_record_ticket =%d,  ";
		$template .= "preservation_record_group =%d,  ";
		$template .= "preservation_record_person_count =%d,  ";
		$template .= "preservation_record_tel =%s,  ";
		$template .= "preservation_record_result =%d,  ";
		$template .= "preservation_record_splitable =%d,  ";
		$template .= "preservation_record_comment =%s,  ";
		$template .= "preservation_record_status =%d,   ";
		$template .= "preservation_record_sync =%d   ";
		//$template .= "preservation_record_unique_id =%s   ";
		$template .= " WHERE preservation_record_unique_id=%s ";
		
		
		
		$sql = sprintf($template, PRESERVATION_RECORD_TABLE,
		//$preservation_record_id,
		$preservation_record_store_id,
		$preservation_record_type,
		$preservation_record_timestamp,
		$preservation_record_ticket,
		$preservation_record_group,
		$preservation_record_person_count,
		$this->db->quoteString($preservation_record_tel),
		$preservation_record_result,
		$preservation_record_splitable,
		$this->db->quoteString($preservation_record_comment),
		$preservation_record_status,
		$preservation_record_sync,
		//$this->db->quoteString($preservation_record_unique_id),
		$this->db->quoteString($preservation_record_unique_id)
		);
		$result = $this->db->queryF($sql);
		
		//echo $sql;
	}
	
	
	function getCount($criteria = null)
    {
    	
        $ret = array();
        //$limit = $start = 0;
        $sql = 'SELECT COUNT(*) AS count FROM '.PRESERVATION_RECORD_TABLE;
        //print_r($criteria);
        if (isset($criteria) && is_subclass_of($criteria, 'criteriaelement')) {
            $sql .= ' '.$criteria->renderWhere();
            
        }
		//echo $sql;
        $result = $this->db->queryF($sql);
        if (!$result){
            return 0;
        }
        
        while ($myrow = $this->db->fetchArray($result)) {
       
            return $myrow["count"];
        }
        return 0;
    }
	function getObjects($criteria = null, $limit=0, $start=-1)
    {
        $ret = array();
        $sql = 'SELECT * FROM '.PRESERVATION_RECORD_TABLE;
        if (isset($criteria) && is_subclass_of($criteria, 'criteriaelement')) {
            $sql .= ' '.$criteria->renderWhere();
            if ($criteria->getSort() != '') {
                $sql .= ' ORDER BY '.$criteria->getSort().' '.$criteria->getOrder();
            }
        }
		//echo $sql.":".$limit.":".$start;
        $result = $this->db->queryF($sql,$limit,$start);
        if (!$result){
            return $ret;
        }
        while ($myrow = $this->db->fetchArray($result)) {
            $preservation = $this->create(true);
            $preservation->assignVars($myrow);
            $ret[] =& $preservation;
			//echo $sql.":".$limit.":".$start;
            unset($preservation);
        }
        return $ret;
    }
	
	
	
	function delete(&$preservationRecordId){
		$template = "DELETE FROM %s WHERE preservation_record_id=%s";
		
		$sql = sprintf($template, PRESERVATION_RECORD_TABLE,
		$preservationRecordId);
		
		//echo $sql;
		if (!$result = $this->db->queryF($sql)) {
             return false;
        }
		return true;
	}
	function deleteAll(){
		$template = "DELETE FROM %s";
		
		$sql = sprintf($template, PRESERVATION_RECORD_TABLE);
		//echo $sql;
		if (!$result = $this->db->queryF($sql)) {
             return false;
        }
		return true;	
	}
	
	function get($preservationUniqueId){
		$criteria = new CriteriaCompo(new Criteria('preservation_record_unique_id', $preservationUniqueId));
		
		$records = $this->getObjects($criteria);
		//echo sizeof($records);
		if(sizeof($records)>0){
			return $records[0];
		}
		else{
			return null;
		}
	}
	function getRecordsByStoreId($storeId,  $limit=20, $start=0){
			
		if($storeId!=-1){
			$criteria = new CriteriaCompo(new Criteria('preservation_record_store_id', $storeId));
			//$criteria->add(new Criteria("preservation_record_status",3, "!="));
			//echo $limit.",".$start;
			$records = $this->getObjects($criteria, $limit, $start);
		}
		else{
			$records = $this->getObjects(null, $limit, $start);
		}
		return $records;
	}
	function getRecordsByStoreIdAndTimeRangeAndStatus($storeId, $timeStart, $timeEnd, $status=0){
		$records = array();
		if($storeId!=-1){
			$criteria = new CriteriaCompo(new Criteria('preservation_record_store_id', $storeId));
			$criteria->add(new Criteria("preservation_record_timestamp",$timeStart, ">"));
			$criteria->add(new Criteria("preservation_record_timestamp",$timeEnd, "<"));
			//$criteria->add(new Criteria("preservation_record_status",3, "!="));
			$records = $this->getObjects($criteria);
		}
		else{
			//$records = $this->getObjects(null, $limit, $start);
		}
		return $records;
	}
	function getCountByStoreIdAndTimeRangeAndStatus($storeId, $timeStart, $timeEnd, $status=0){
		if($storeId!=-1){
			$criteria = new CriteriaCompo(new Criteria('preservation_record_store_id', $storeId));
			$criteria->add(new Criteria("preservation_record_timestamp",$timeStart, ">"));
			$criteria->add(new Criteria("preservation_record_timestamp",$timeEnd, "<"));
			//$criteria->add(new Criteria("preservation_record_status",3, "!="));
			$count = $this->getCount($criteria);
			return $count;
		}
		else{
			return 0;
		}
	}
	function getCountByStoreId($storeId){
		$count = 0;
		if($storeId!=-1){
			$criteria = new CriteriaCompo(new Criteria('preservation_record_store_id', $storeId));
			//$criteria->add(new Criteria("preservation_record_status",3, "!="));
			$count = $this->getCount($criteria);
		}
		else{
			$count = $this->getCount();
		}
		
		return $count;
	}
	function getCountByStoreIdAndGroupId($storeId, $groupId,$timeStart=-1, $timeEnd=-1){
		$criteria = new CriteriaCompo();
		if($storeId!=-1){
			$criteria->add(new Criteria('preservation_record_store_id', $storeId));
		}
		$criteria->add(new Criteria('preservation_record_group', $groupId));
		//$criteria->add(new Criteria("preservation_record_status",3, "!="));
		
		
		if($timeStart!=-1){
			$criteria->add(new Criteria("preservation_record_timestamp",$timeStart, ">"));
		}
		if($timeEnd!=-1){
			$criteria->add(new Criteria("preservation_record_timestamp",$timeEnd, "<"));
		}
		
		$count = $this->getCount($criteria);
		
		return $count;
	}
	function getCountByStoreIdAndGroupIdAndStatus($storeId, $groupId, $status,$timeStart=-1, $timeEnd=-1){
		//echo "buffer C status";
		$criteria = new CriteriaCompo(new Criteria('preservation_record_store_id', $storeId));
		$criteria->add(new Criteria('preservation_record_group', $groupId));
		$criteria->add(new Criteria('preservation_record_status', $status));
		//$criteria->add(new Criteria("preservation_record_status",3, "!="));
		
		if($timeStart!=-1){
			$criteria->add(new Criteria("preservation_record_timestamp",$timeStart, ">"));
		}
		if($timeEnd!=-1){
			$criteria->add(new Criteria("preservation_record_timestamp",$timeEnd, "<"));
		}
		
		$count = $this->getCount($criteria);
		
		return $count;
	}
	function getLastRecordByStoreIdAndGroupId($storeId, $groupId){
		$criteria = new CriteriaCompo(new Criteria('preservation_record_store_id', $storeId));
		$criteria->add(new Criteria('preservation_record_group', $groupId));
		//$criteria->add(new Criteria("preservation_record_status",3, "!="));
		$records = $this->getObjects($criteria, 1, 0);
		if(sizeof($records)>0){
			return $records[0];
		}
		else{
			return null;
		}
	}
	function getLastRecordByStoreIdAndGroupIdAndStatus($storeId, $groupId, $status,$timeStart=-1, $timeEnd=-1){
		$criteria = new CriteriaCompo(new Criteria('preservation_record_store_id', $storeId));
		$criteria->add(new Criteria('preservation_record_group', $groupId));
		$criteria->add(new Criteria('preservation_record_status', $status));
		//$criteria->add(new Criteria("preservation_record_status",3, "!="));
		if($timeStart!=-1){
			$criteria->add(new Criteria("preservation_record_timestamp",$timeStart, ">"));
		}
		if($timeEnd!=-1){
			$criteria->add(new Criteria("preservation_record_timestamp",$timeEnd, "<"));
		}
		$criteria->setOrder("DESC");
		$criteria->setOrder("preservation_record_timestamp");
		
		$records = $this->getObjects($criteria, 1, 0);
		if(sizeof($records)>0){
			return $records[0];
		}
		else{
			return null;
		}
	}

	function getLastRecordByStoreId($storeId){
		$criteria = new CriteriaCompo(new Criteria('preservation_record_store_id', $storeId));
		//$criteria->add(new Criteria('preservation_record_group', $groupId));
		//$criteria->add(new Criteria("preservation_record_status",3, "!="));
		$criteria->setOrder("DESC");
		$criteria->setSort("preservation_record_timestamp");
		
		$records = $this->getObjects($criteria, 1, 0);
		if(sizeof($records)>0){
			return $records[0];
		}
		else{
			return null;
		}
	}
	function getRecordsByStatusAndStoreIdAndGroupId($status, $storeId, $groupId, $timeStart=-1, $timeEnd=-1, $order='ASC'){
		$criteria = new CriteriaCompo();
		if($storeId!=-1){
			$criteria->add(new Criteria('preservation_record_store_id', $storeId));
		}
		$criteria->add(new Criteria('preservation_record_group', $groupId));
		//$criteria->add(new Criteria("preservation_record_status",3, "!="));
		if($status!=-1){
			$criteria->add(new Criteria('preservation_record_status', $status));
		}
		
		if($timeStart!=-1){
			$criteria->add(new Criteria("preservation_record_timestamp",$timeStart, ">"));
		}
		if($timeEnd!=-1){
			$criteria->add(new Criteria("preservation_record_timestamp",$timeEnd, "<"));
		}
		
		$criteria->setOrder($order);
		$criteria->setSort("preservation_record_ticket");
		$records = $this->getObjects($criteria);
		//echo "sql where:".' '.$criteria->renderWhere();
		return $records;
		
	}
	function getRecordsByStatusAndStoreIdAndGroupIdAndIndex($status, $storeId, $groupId, $index){
		$criteria = new CriteriaCompo();
		if($storeId!=-1){
			$criteria->add(new Criteria('preservation_record_store_id', $storeId));
		}
		$criteria->add(new Criteria('preservation_record_group', $groupId));
		//$criteria->add(new Criteria("preservation_record_status",3, "!="));
		if($status!=-1){
			$criteria->add(new Criteria('preservation_record_status', $status));
		}
		$criteria->setOrder('ASC');
		$criteria->setSort("preservation_record_ticket");
		$records = $this->getObjects($criteria);
		if(sizeof($records)>$index){
			return $records[$index];
		}
		else {
			return null;
		}
		
		
	}
	function getRecordsByStatusAndStoreId($status, $storeId, $timeStart=-1, $timeEnd = -1){
		$criteria = new CriteriaCompo();
		if($storeId!=-1){
			$criteria->add(new Criteria('preservation_record_store_id', $storeId));
		}
		
		//$criteria->add(new Criteria("preservation_record_status",3, "!="));
		if($status!=-1){
			$criteria->add(new Criteria('preservation_record_status', $status));
		}
		if($timeStart!=-1){
			$criteria->add(new Criteria("preservation_record_timestamp",$timeStart, ">"));
		}
		if($timeEnd!=-1){
			$criteria->add(new Criteria("preservation_record_timestamp",$timeEnd, "<"));
		}
		
		//echo "sql where:".' '.$criteria->renderWhere();
		$records = $this->getObjects($criteria);
		
		return $records;
		
	}
	/*
	function getTicketByGroup($groupIdentity){
		$query = "SELECT * FROM ".GROUP_TABLE." WHERE group_identity=".$groupIdentity;
		$result = $this->db->queryF($sql,$limit,$start);
		$count = $this->db->getRowsNum($result);
		if($count>0){
			$row = $this->db->fetchArray($result);
			$count = $row["group_serial"];
			
			$updateSQL = "UPDATE ".GROUP_TABLE." SET group_serial=group_serial+1 WHERE group_identity=".$groupIdentity; 
			$resultUpdate = $this->db->queryF($updateSQL);
			
			return $count;
		}
		else{
			$insertSQL = "INSERT INTO ".GROUP_TABLE." ('group_serial', 'group_identity') VALUES (1,".$groupIdentity.")";
			$resultInsert = $this->db->queryF($insertSQL);
			return 1;
		}
	}
	 * 
	 */
	function changeStatusByPreservationUniqueId($preservationUniqueId, $status){
		$preservation = $this->get($preservationUniqueId);
		//print_r($preservation);
		if($preservation==null){
			return 500; //preservation id not exist
		}
		else{
			$preservation->setVar("preservation_record_status", $status);
			$this->update($preservation);
			return 200;
		}
	}
	function generatePreservationId(){
		$val = time();
		return $val;
	}
	function generatePreservationUniqueId($storeId){
		$val = (string)(time()).(string)($storeId);
		return $val;
	}
	function syncPreservation($preservationId){
		$preservation = $this->get($preservationId);
		
		$preservation->setVar("preservation_record_sync", 1);
		
		$this->update($preservation);
		
	}
	
	
	
}
?>