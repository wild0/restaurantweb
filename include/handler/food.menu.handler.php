<?php
class FoodMenuHandler extends ObjectHandler{
	//basic operation
	function &create($isNew = true){
		$foodmenu = new FoodMenu();
        if ($isNew) {
            $foodmenu->setNew();
        }
        return $foodmenu;
	}
	function insert(&$foodmenu){
		if (!$foodmenu->cleanVars()) {
            return false;
        }
        foreach ($foodmenu->cleanVars as $k => $v) {
            ${$k} = $v;
        }
		$template = "INSERT INTO %s (";
		$template .= "food_menu_name, ";
		$template .= "food_menu_description, ";
		$template .= "food_menu_price ";

		$template .= ") VALUES ( %s, %s,%d)";
		
		$sql = sprintf($template, FOOD_MENU_TABLE,
		$this->db->quoteString($food_menu_name),
		$this->db->quoteString($food_menu_description),
		($food_menu_price)
		);
		//echo $sql ;
		$result = $this->db->queryF($sql);
		$foodMenuId = $this->db->getInsertId();
		
		return $foodMenuId;
	}
	function replace($foodmenu){
		if (!$foodmenu->cleanVars()) {
            return false;
        }
        foreach ($foodmenu->cleanVars as $k => $v) {
            ${$k} = $v;
        }
		$template = "REPLACE INTO %s (";
		$template .= "food_menu_id, ";
		$template .= "food_menu_name, ";
		$template .= "food_menu_description, ";
		$template .= "food_menu_price ";

		$template .= ") VALUES ( %d, %s, %s,%d)";
		
		$sql = sprintf($template, FOOD_MENU_TABLE,
		$food_menu_id, 
		$this->db->quoteString($food_menu_name),
		$this->db->quoteString($food_menu_description),
		($food_menu_price)
		);
		//echo $sql ;
		$result = $this->db->queryF($sql);
		//$foodMenuId = $this->db->getInsertId();
		
		return $result;
	}
	function update($pairValues, $criteria){
		foreach ($pairValues as $k => $v) {
    		${$k} = $v;
    	}
    	$sql = "UPDATE ".STORE_TABLE." SET ";
    
    	for($i=0;$i<sizeof($pairValues);$i++){
    		if($i==0){
    			$sql .= ($k)."=".$this->db->quoteString($v);
    		}
    		else{
    			$sql .= ", ".($k)."=".$this->db->quoteString($v);
    		}
    	}
    
    	if (isset($criteria) && is_subclass_of($criteria, 'criteriaelement')) {
    		$sql .= ' '.$criteria->renderWhere();
    			
    	}
    	$result = $this->db->queryF($sql);
    	return $result;
	}
	function delete(&$criteria = null){
		$ret = array();
        $sql = 'DELETE FROM '.FOOD_MENU_TABLE;
        if (isset($criteria) && is_subclass_of($criteria, 'criteriaelement')) {
            $sql .= ' '.$criteria->renderWhere();
            
        }
		//echo $sql;
        $result = $this->db->queryF($sql);
        return $result;
	}
	function getCount($criteria = null)
    {
    	
        $ret = array();
        //$limit = $start = 0;
        $sql = 'SELECT COUNT(*) AS count FROM '.FOOD_MENU_TABLE;
        //print_r($criteria);
        if (isset($criteria) && is_subclass_of($criteria, 'criteriaelement')) {
            $sql .= ' '.$criteria->renderWhere();
            
        }
        $result = $this->db->queryF($sql);
        if (!$result){
            return 0;
        }
        //echo $sql;
        while ($myrow = $this->db->fetchArray($result)) {
       
            return $myrow["count"];
        }
        return 0;
    }
	function getObjects($criteria = null, $limit=0, $start=-1)
    {
        $ret = array();
        $sql = 'SELECT * FROM '.FOOD_MENU_TABLE;
        if (isset($criteria) && is_subclass_of($criteria, 'criteriaelement')) {
            $sql .= ' '.$criteria->renderWhere();
            if ($criteria->getSort() != '') {
                $sql .= ' ORDER BY '.$criteria->getSort().' '.$criteria->getOrder();
            }
        }
		//echo $sql;
        $result = $this->db->queryF($sql,$limit,$start);
        if (!$result){
            return $ret;
        }
        while ($myrow = $this->db->fetchArray($result)) {
            $foodmenu = $this->create(true);
            $foodmenu->assignVars($myrow);
            $ret[] =& $foodmenu;
            unset($foodmenu);
        }
        return $ret;
    }
	
	
	
	/////////////////////////////////////////////////////
	
	function getFoodMenus($limit=0, $start=-1){
		return $this->getObjects(null, $limit, $start);
	}
	function get($foodMenuId){
		$criteria = new CriteriaCompo(new Criteria('food_menu_id', $foodMenuId));
		$results = $this->getObjects($criteria, 1, 0);
		if(sizeof($results)>0){
			return $results[0];
		}
		else{
			return false;
		}
	}
	
	function getFoodMenuCount(){
		return $this->getCount();
	}
	function removeFoodMenu($foodMenuId){
		$criteria = new CriteriaCompo(new Criteria('food_menu_id', $foodMenuId));
		return $this->delete($criteria);
	}
	function insertFoodMenu($foodMenu){
		//$criteria = new CriteriaCompo(new Criteria('food_menu_id', $foodMenuId));
		return $this->insert($foodMenu);
	}
	function modifyFoodMenu($foodMenu){
		//$criteria = new CriteriaCompo(new Criteria('food_menu_id', $foodMenuId));
		return $this->replace($foodMenu);
	}
	function updateDataFromRoot(){
		$rootHost = $variableHandler->getVariable(ROOT_IP);
		$rootCmdUrl = $rootHost;
		
		$postData["command"]=GET_ALL_FOOD_MENU_CMD;
		
		$returnData = redirectPostUrl($rootCmdUrl, $postData);
	}
	
	
	
}
?>