<?php
class PreservationFoodRecordHandler extends ObjectHandler{
	//basic operation
	function &create($isNew = true){
		$preservationFoodRecord = new PreservationFoodRecord();
        if ($isNew) {
            $preservationFoodRecord->setNew();
        }
        return $preservationFoodRecord;
	}
	function insert(&$preservationRecord){
		if (!$preservationRecord->cleanVars()) {
            return false;
        }
        foreach ($preservationRecord->cleanVars as $k => $v) {
            ${$k} = $v;
        }
	}
	
}
?>