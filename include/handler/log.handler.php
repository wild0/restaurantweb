<?php
class LogHandler extends ObjectHandler{
	//basic operation
	function &create($isNew = true){
		$logRecord = new LogRecord();
        if ($isNew) {
            $logRecord->setNew();
        }
        return $logRecord;
	}
	function insert(&$logRecord){
		if (!$logRecord->cleanVars()) {
            return false;
        }
        foreach ($logRecord->cleanVars as $k => $v) {
            ${$k} = $v;
        }
		
		$template = "INSERT INTO %s (";
		$template .= "log_record_type, ";
		$template .= "log_record_mode, ";
		$template .= "log_record_src_host, ";
		$template .= "log_record_role, ";
		$template .= "log_record_user_id, ";
		$template .= "log_record_store_id, ";
		$template .= "log_record_operation, ";
		$template .= "log_record_description, ";
		$template .= "log_record_result_status_code, ";
		$template .= "log_record_result_description, ";
		$template .= "log_record_timestamp ";

		$template .= ") VALUES ( %d, %d, %s, %s, %d, %d, %d, %s, %d, %s, %d)";
		
		$sql = sprintf($template, LOG_TABLE,
			$log_record_type,
			$log_record_mode,
			$this->db->quoteString($log_record_src_host),
			$this->db->quoteString($log_record_role),
			$log_record_user_id,
			$log_record_store_id,
			$log_record_operation,
			$this->db->quoteString($log_record_description),	
			$log_record_result_status_code,
			$this->db->quoteString($log_record_result_description),	
			$log_record_timestamp

		);
		//echo $sql ;
		$result = $this->db->queryF($sql);
		$logId = $this->db->getInsertId();
		
		return $logId;
		
	}
	
	function getCount($criteria = null)
    {
    	
        $ret = array();
        //$limit = $start = 0;
        $sql = 'SELECT COUNT(*) AS count FROM '.LOG_TABLE;
        //print_r($criteria);
        if (isset($criteria) && is_subclass_of($criteria, 'criteriaelement')) {
            $sql .= ' '.$criteria->renderWhere();
            
        }
        $result = $this->db->queryF($sql);
        if (!$result){
            return 0;
        }
        //echo $sql;
        while ($myrow = $this->db->fetchArray($result)) {
       
            return $myrow["count"];
        }
        return 0;
    }
	
	function getObjects($criteria = null, $limit=0, $start=-1)
    {
        $ret = array();
        $sql = 'SELECT * FROM '.LOG_TABLE;
        if (isset($criteria) && is_subclass_of($criteria, 'criteriaelement')) {
            $sql .= ' '.$criteria->renderWhere();
            if ($criteria->getSort() != '') {
                $sql .= ' ORDER BY '.$criteria->getSort().' '.$criteria->getOrder();
            }
        }
		//echo $sql;
        $result = $this->db->queryF($sql,$limit,$start);
        if (!$result){
            return $ret;
        }
        while ($myrow = $this->db->fetchArray($result)) {
            $log = $this->create(true);
            $log->assignVars($myrow);
            $ret[] =& $log;
            unset($log);
        }
        return $ret;
    }
	function delete(&$criteria){
		
	}
	
	///////////////////////////////////////////
	
	function getLogs($limit=0, $start=-1){
		//$criteria  = new CompoCriteria();
		return $this->getObjects(null, $limit, $start);
	}
	
	function addLog($srcHost, $role, $operation, $description = "", $resultStatusCode = 0, $resultDescription = ""){
		$logRecord = new LogRecord();
		$logRecord->setVar("log_record_src_host", $srcHost);
		$logRecord->setVar("log_record_role", $role);
		$logRecord->setVar("log_record_operation", $operation);
		$logRecord->setVar("log_record_description", $description);
		$logRecord->setVar("log_record_result_status_code", $resultStatusCode);
		$logRecord->setVar("log_record_result_description", $resultDescription);
		$logRecord->setVar("log_record_timestamp", time());
		$this->insert($logRecord);
		return $logRecord;
	}
	function syncLog($preservationId){
		$preservation = $this->get($preservationId);
		
		$preservation->setVar("preservation_record_sync", 1);
		
		$this->update($preservation);
		
	}
}
?>		