<?php
class UserHandler extends ObjectHandler{
	//basic operation
	function &create($isNew = true){
		$user = new User();
        if ($isNew) {
            $user->setNew();
        }
        return $user;
	}
	function insert(&$user){
		if (!$user->cleanVars()) {
            return false;
        }
        foreach ($user->cleanVars as $k => $v) {
            ${$k} = $v;
        }
		
		$template = "INSERT INTO %s (";
		$template .= "user_name, ";
		$template .= "user_password, ";
		$template .= "user_device_list, ";
		$template .= "user_store_id, ";
		$template .= "user_permission, ";
		$template .= "user_mode ";

		$template .= ") VALUES ( %s, %s, %s,%d, %d, %d)";
		
		$sql = sprintf($template, USER_TABLE,
		$this->db->quoteString($user_name),
		$this->db->quoteString($user_password),
		$this->db->quoteString($user_device_list),
		($user_store_id), 
		$user_permission,
		$user->getMode()
		);
		//echo $sql ;
		$result = $this->db->queryF($sql);
		$userId = $this->db->getInsertId();
		
		return $userId;
	}
	
	function replace($user){
		if (!$user->cleanVars()) {
            return false;
        }
        foreach ($user->cleanVars as $k => $v) {
            ${$k} = $v;
        }
		
		$template = "REPLACE INTO %s (";
		$template .= "user_id,";
		$template .= "user_name, ";
		$template .= "user_password, ";
		$template .= "user_device_list, ";
		$template .= "user_store_id, ";
		$template .= "user_permission, ";
		$template .= "user_mode ";

		$template .= ") VALUES (%d, %s, %s, %s,%d, %d, %d)";
		
		$sql = sprintf($template, USER_TABLE,
		$user_id,
		$this->db->quoteString($user_name),
		$this->db->quoteString($user_password),
		$this->db->quoteString($user_device_list),
		($user_store_id), 
		$user_permission,
		$user->getMode()
		);
		//echo $sql ;
		$result = $this->db->queryF($sql);
		//$userId = $this->db->getInsertId();
		
		return $user_id;
	}
	function update($pairValues, $criteria = null){
    	foreach ($pairValues as $k => $v) {
    		${$k} = $v;
    	}
    	$sql = "UPDATE ".USER_TABLE." SET ";
    
    	for($i=0;$i<sizeof($pairValues);$i++){
    		if($i==0){
    			$sql .= ($k)."=".$this->db->quoteString($v);
    		}
    		else{
    			$sql .= ", ".($k)."=".$this->db->quoteString($v);
    		}
    	}
    
    	if (isset($criteria) && is_subclass_of($criteria, 'criteriaelement')) {
    		$sql .= ' '.$criteria->renderWhere();
    			
    	}
    	$result = $this->db->queryF($sql);
    	return $result;
    }
	function delete(&$criteria){
		$ret = array();
        $sql = 'DELETE FROM '.USER_TABLE;
        if (isset($criteria) && is_subclass_of($criteria, 'criteriaelement')) {
            $sql .= ' '.$criteria->renderWhere();
            
        }
		//echo $sql;
        $result = $this->db->queryF($sql);
        return 1;
	}
	function remove($uid){
		$criteria = new CriteriaCompo(new Criteria('user_id', $uid));
		$this->delete($criteria);
	}
	function getCount($criteria = null)
    {
    	
        $ret = array();
        //$limit = $start = 0;
        $sql = 'SELECT COUNT(*) AS count FROM '.USER_TABLE;
        //print_r($criteria);
        if (isset($criteria) && is_subclass_of($criteria, 'criteriaelement')) {
            $sql .= ' '.$criteria->renderWhere();
            
        }
        $result = $this->db->queryF($sql);
        if (!$result){
            return 0;
        }
        //echo $sql;
        while ($myrow = $this->db->fetchArray($result)) {
       
            return $myrow["count"];
        }
        return 0;
    }
	
	function getObjects($criteria = null, $limit=0, $start=-1)
    {
        $ret = array();
        $sql = 'SELECT * FROM '.USER_TABLE;
        if (isset($criteria) && is_subclass_of($criteria, 'criteriaelement')) {
            $sql .= ' '.$criteria->renderWhere();
            if ($criteria->getSort() != '') {
                $sql .= ' ORDER BY '.$criteria->getSort().' '.$criteria->getOrder();
            }
        }
		//echo $sql;
        $result = $this->db->queryF($sql,$limit,$start);
        if (!$result){
            return $ret;
        }
        while ($myrow = $this->db->fetchArray($result)) {
            $user = $this->create(true);
            $user->assignVars($myrow);
            $ret[] =& $user;
            unset($user);
        }
        return $ret;
    }
	
	function insertUser($user){
		return $this->insert($user);
	}
	
	function login($uname, $password, $device=""){
		
		if($uname==ROOT_ACCOUNT && $password==ROOT_PASSWORD){
			$ticket = md5($uname."_".$device."_".time());
			
			$_SESSION["login"] = 1;
			$_SESSION["user_id"] = 0;
			$_SESSION["user_name"] = ROOT_ACCOUNT;
			$_SESSION["user_device"] = $device;
			$_SESSION["user_permission"] = 0;
			$_SESSION["user_ticket"] = $ticket;
			return 0;
		}
		
		
		$criteria = new CriteriaCompo(new Criteria('user_name', $uname));
		$criteria->add(new Criteria('user_password', $password));
		
		$users = $this->getObjects($criteria);
		if(sizeof($users)>0){
			//$deviceBound = $users[0]->getVar("user_device_list");
			$ticket = md5($uname."_".$device."_".time());
			$deviceList = $users[0]->getVar("user_device_list");
			if($deviceList==""){
				$this->updateDevices(array($device),$users[0]->getVar("user_id"));
			
			
				$_SESSION["login"] = 1;
				$_SESSION["user_id"] = $users[0]->getVar("user_id");
				$_SESSION["user_name"] = $users[0]->getVar("user_name");
				$_SESSION["user_device"] = $device;
				$_SESSION["user_permission"] = $users[0]->getVar("user_permission");
				$_SESSION["user_ticket"] = $ticket;
				return 0;
			}
			else{
				$deviceLists = json_decode($deviceList);
				for($i=0;$i<sizeof($deviceLists);$i++){
					if($deviceLists[$i]==$device){
						$_SESSION["login"] = 1;
						$_SESSION["user_id"] = $users[0]->getVar("user_id");
						$_SESSION["user_name"] = $users[0]->getVar("user_name");
						$_SESSION["user_device"] = $device;
						$_SESSION["user_permission"] = $users[0]->getVar("user_permission");
						$_SESSION["user_ticket"] = $ticket;
						return 0;
					}
				}
				$_SESSION["login"] = 0;
				return 2;
			}
		}
		else{
			$_SESSION["login"] = 0;
			return 1;
		}
	}
	function isLogin(){
		if(isset($_SESSION["login"])){
			return $_SESSION["login"];
		}
		else {
			return 0;
		}
	}
	function logout(){
		$_SESSION["login"] = 0;
		$_SESSION["user_id"] = -1;
		$_SESSION["user_name"] = "";
		$_SESSION["user_device"] = "";
		$_SESSION["user_permission"] = -1;
		$_SESSION["user_ticket"] = "";
	}
	function getUserCount(){
		$count = $this->getCount();
		return $count;
	}
	function getUsers($limit = 0, $start = -1){
		$users = $this->getObjects(null, $limit, $start);
		return $users;
	}
	function get($userId){
		$criteria = new CriteriaCompo(new Criteria('user_id', $userId));
		$results = $this->getObjects($criteria, 1, 0);
		if(sizeof($results)>0){
			return $results[0];
		}
		else{
			return false;
		}
	}
	function modifyUser($user){
		$result = $this->replace($user);
		return $result;
	}
	function getByUserName($userName){
		$criteria = new CriteriaCompo(new Criteria('user_name', $userName));
		$results = $this->getObjects($criteria, 1, 0);
		if(sizeof($results)>0){
			return $results[0];
		}
		else{
			return false;
		}
	}
	function updateDevices($devices, $userId){
		$devicesJson = json_encode($devices);
		$criteria = new CriteriaCompo(new Criteria('user_id', $userId));
		$pairValues = array();
		$pairValues["user_device_list"] = $devicesJson;
		return $this->update($pairValues, $criteria);
	}
}
?>