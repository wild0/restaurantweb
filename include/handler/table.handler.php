<?php
class TableHandler extends ObjectHandler{
	//basic operation
	function &create($isNew = true){
		$table = new Table();
        if ($isNew) {
            $table->setNew();
        }
        return $table;
	}
	function insert($table){
		if (!$table->cleanVars()) {
            return false;
        }
        foreach ($table->cleanVars as $k => $v) {
            ${$k} = $v;
        }
	}
	function update($table){
		
	}
	function delete($id){
		
	}
	function getObjects($criteria = null, $limit=0, $start=-1)
    {
        $ret = array();
        $sql = 'SELECT * FROM '.STORE_TABLE;
        if (isset($criteria) && is_subclass_of($criteria, 'criteriaelement')) {
            $sql .= ' '.$criteria->renderWhere();
            if ($criteria->getSort() != '') {
                $sql .= ' ORDER BY '.$criteria->getSort().' '.$criteria->getOrder();
            }
        }
		//echo $sql;
        $result = $this->db->queryF($sql,$limit,$start);
        if (!$result){
            return $ret;
        }
        while ($myrow = $this->db->fetchArray($result)) {
            $table = $this->create(true);
            $table->assignVars($myrow);
            $ret[] =& $table;
            unset($table);
        }
        return $ret;
    }
}
?>