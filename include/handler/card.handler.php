<?php
class CardHandler extends ObjectHandler{
	//basic operation
	function &create($isNew = true){
		$card = new Card();
        if ($isNew) {
            $card->setNew();
        }
        return $card;
	}
	function insert(&$card){
		if (!$card->cleanVars()) {
            return false;
        }
        foreach ($card->cleanVars as $k => $v) {
            ${$k} = $v;
        }
		
		$template = "INSERT INTO %s (";
		$template .= "card_store_id, ";
		$template .= "card_code, ";
		$template .= "card_permission, ";
		$template .= "card_description";

		$template .= ") VALUES (%d, %s, %d,%s)";
		
		$sql = sprintf($template, CARD_TABLE,
		$card_store_id,
		$this->db->quoteString($card_code),
		($card_permission),
		$this->db->quoteString($card_description)
		
		);
		//echo $sql ;
		$result = $this->db->queryF($sql);
		$cardId = $this->db->getInsertId();
		
		return $cardId;
	}
	
	function replace($card){
		if (!$card->cleanVars()) {
            return false;
        }
        foreach ($card->cleanVars as $k => $v) {
            ${$k} = $v;
        }
		
		$template = "REPLACE INTO %s (";
		$template .= "card_id,";
		$template .= "card_store_id,";
		$template .= "card_code, ";
		$template .= "card_permission, ";
		$template .= "card_description";

		$template .= ") VALUES (%d, %d, %s, %d, %s)";
		
		$sql = sprintf($template, CARD_TABLE,
		$card_id,
		$card_store_id,
		$this->db->quoteString($card_code),
		$card_permission,
		$this->db->quoteString($card_description),
		($user_store_id), 
		$user_permission,
		$user->getMode()
		);
		//echo $sql ;
		$result = $this->db->queryF($sql);
		//$userId = $this->db->getInsertId();
		
		return $user_id;
	}
	function update($pairValues, $criteria){
		
	}
	function delete(&$criteria){
		//$ret = array();
        $sql = 'DELETE FROM '.CARD_TABLE;
        if (isset($criteria) && is_subclass_of($criteria, 'criteriaelement')) {
            $sql .= ' '.$criteria->renderWhere();
            
        }
		//echo $sql;
        $result = $this->db->queryF($sql);
        return 1;
	}
	
	function getCount($criteria = null)
    {
    	
        $ret = array();
        //$limit = $start = 0;
        $sql = 'SELECT COUNT(*) AS count FROM '.CARD_TABLE;
        //print_r($criteria);
        if (isset($criteria) && is_subclass_of($criteria, 'criteriaelement')) {
            $sql .= ' '.$criteria->renderWhere();
            
        }
        $result = $this->db->queryF($sql);
        if (!$result){
            return 0;
        }
        //echo $sql;
        while ($myrow = $this->db->fetchArray($result)) {
       
            return $myrow["count"];
        }
        return 0;
    }
	
	function getObjects($criteria = null, $limit=0, $start=-1)
    {
        $ret = array();
        $sql = 'SELECT * FROM '.CARD_TABLE;
        if (isset($criteria) && is_subclass_of($criteria, 'criteriaelement')) {
            $sql .= ' '.$criteria->renderWhere();
            if ($criteria->getSort() != '') {
                $sql .= ' ORDER BY '.$criteria->getSort().' '.$criteria->getOrder();
            }
        }
		//echo $sql;
        $result = $this->db->queryF($sql,$limit,$start);
        if (!$result){
            return $ret;
        }
        while ($myrow = $this->db->fetchArray($result)) {
            $card = $this->create(true);
            $card->assignVars($myrow);
            $ret[] =& $card;
            unset($card);
        }
        return $ret;
    }
	
	////////////////////////////////////////////////////////////////
	
	function remove($cid){
		$criteria = new CriteriaCompo(new Criteria('card_id', $cid));
		return $this->delete($criteria);
	}
	
	function get($cardId){
		$criteria = new CriteriaCompo(new Criteria('card_id', $userId));
		$results = $this->getObjects($criteria, 1, 0);
		if(sizeof($results)>0){
			return $results[0];
		}
		else{
			return null;
		}
	}
	function getByCardCode($cardCode, $storeId){
		$criteria = new CriteriaCompo(new Criteria('card_code', $cardCode));
		$criteria->addCriteria(new Criteria("card_store_id", $storeId));
		
		$results = $this->getObjects($criteria, 1, 0);
		if(sizeof($results)>0){
			return $results[0];
		}
		else{
			return null;
		}
	}
	function getByStoreId($storeId, $limit, $start){
		$criteria = new CriteriaCompo(new Criteria("card_store_id", $storeId));

		
		$results = $this->getObjects($criteria, $limit, $start);
		return $results;
	}
}
?>