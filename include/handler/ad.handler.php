<?php
Class AdvertisementHandler extends ObjectHandler{
	function &create($isNew = true){
		$advertisement = new Advertisement();
        if ($isNew) {
            $advertisement->setNew();
        }
        return $advertisement;
	}
	function insert(&$advertisement){
		if (!$advertisement->cleanVars()) {
            return false;
        }
        foreach ($advertisement->cleanVars as $k => $v) {
            ${$k} = $v;
        }
		//$playerActivateCode = md5($player_name.$player_uname.$player_email);
        // RMV-NOTIFY
        // Added two fields, notify_method, notify_mode
        //$temp = "REPLACE INTO %s (";
        $template = "INSERT INTO %s (";
		$template .= "advertisement_name, ";
		$template .= "advertisement_content, ";
		$template .= "advertisement_time_start, ";
		$template .= "advertisement_time_end, ";
		$template .= "advertisement_type, ";
		$template .= "advertisement_active";
		
		$template .= ") VALUES ( %s,%s,%d,%d,%d,%d)";
		
		$sql = sprintf($template, ADVERTISEMENT_TABLE,
		$this->db->quoteString($advertisement_name),
		$this->db->quoteString($advertisement_content),
		$advertisement_time_start,
		$advertisement_time_end,
		$advertisement_type,
		$advertisement_active
		);
		//echo $sql ;
		$result = $this->db->queryF($sql);
		$advertisementId = $this->db->getInsertId();
		
		
		
		return $advertisementId;
	}
	function replace($advertisement){
		if (!$advertisement->cleanVars()) {
            return false;
        }
        foreach ($advertisement->cleanVars as $k => $v) {
            ${$k} = $v;
        }
		//$playerActivateCode = md5($player_name.$player_uname.$player_email);
        // RMV-NOTIFY
        // Added two fields, notify_method, notify_mode
        //$temp = "REPLACE INTO %s (";
        $template = "REPLACE INTO %s (";
        $template .= "advertisement_id, ";
		$template .= "advertisement_name, ";
		$template .= "advertisement_content, ";
		$template .= "advertisement_time_start, ";
		$template .= "advertisement_time_end, ";
		$template .= "advertisement_type, ";
		$template .= "advertisement_active";
		
		$template .= ") VALUES ( %d, %s,%s,%d,%d,%d,%d)";
		
		$sql = sprintf($template, ADVERTISEMENT_TABLE,
		$advertisement_id,
		$this->db->quoteString($advertisement_name),
		$this->db->quoteString($advertisement_content),
		$advertisement_time_start,
		$advertisement_time_end,
		$advertisement_type,
		$advertisement_active
		);
		//echo $sql ;
		$result = $this->db->queryF($sql);
		
		
		return $advertisement_id;
	}
	function delete(&$criteria){
		
        $sql = 'DELETE FROM '.ADVERTISEMENT_TABLE;
        if (isset($criteria) && is_subclass_of($criteria, 'criteriaelement')) {
            $sql .= ' '.$criteria->renderWhere();
            
        }
		//echo $sql;
        $result = $this->db->queryF($sql);
        return $result;
	}
	
	function getObjects($criteria = null, $limit=0, $start=-1)
    {
        $ret = array();
        $sql = 'SELECT * FROM '.ADVERTISEMENT_TABLE;
        if (isset($criteria) && is_subclass_of($criteria, 'criteriaelement')) {
            $sql .= ' '.$criteria->renderWhere();
            if ($criteria->getSort() != '') {
                $sql .= ' ORDER BY '.$criteria->getSort().' '.$criteria->getOrder();
            }
        }
		//echo $sql;
        $result = $this->db->queryF($sql,$limit,$start);
        if (!$result){
            return $ret;
        }
        while ($myrow = $this->db->fetchArray($result)) {
            $ad = $this->create(true);
            $ad->assignVars($myrow);
            $ret[] =& $ad;
            unset($ad);
        }
        return $ret;
    }
	function getCount($criteria = null)
    {
    	
        $ret = array();
        //$limit = $start = 0;
        $sql = 'SELECT COUNT(*) AS count FROM '.ADVERTISEMENT_TABLE;
        //print_r($criteria);
        if (isset($criteria) && is_subclass_of($criteria, 'criteriaelement')) {
            $sql .= ' '.$criteria->renderWhere();
            
        }
        $result = $this->db->queryF($sql);
        if (!$result){
            return 0;
        }
        //echo $sql;
        while ($myrow = $this->db->fetchArray($result)) {
       
            return $myrow["count"];
        }
        return 0;
    }
	
	///////////////////////////////////////////////////////////////
	
	
	function get($advertisementId){
		$criteria = new CriteriaCompo(new Criteria('advertisement_id', $advertisementId));
		$ads = $this->getObjects($criteria, 1, 0);
		return $ads[0];
		
	}
	function getLatest(){
		$criteria = new CriteriaCompo();
		$criteria->setOrder("DESC");
		$criteria->setSort("advertisement_time_start");
		
		$ads = $this->getObjects($criteria, 1, 0);
		if(sizeof($ads)>0){
			return $ads[0];
		}
		else{
			return null;
		}
		
	}
	
	function getAds($limit, $start){
		$ads = $this->getObjects(null, $limit, $start);
		return $ads;
	}
	function getAllAds(){
		$ads = $this->getObjects(null);
		return $ads;
	}
	function remove($advertisementId){
		$criteria = new CriteriaCompo(new Criteria('advertisement_id', $advertisementId));
		$result = $this->delete($criteria);
		return $result;
	}
}
?>