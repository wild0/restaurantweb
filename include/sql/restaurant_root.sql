DROP TABLE IF EXISTS log_record_table;
CREATE TABLE IF NOT EXISTS log_record_table (
  log_record_id bigint(20) NOT NULL auto_increment,
  log_record_type int(10) NOT NULL,
  log_record_mode int(10) NOT NULL,
  log_record_src_host text NOT NULL,
  log_record_role int(10) NOT NULL,
  log_record_user_id int(10) NOT NULL,
  log_record_store_id int(10) NOT NULL,
  log_record_operation int(10) NOT NULL,
  log_record_description text NOT NULL,
  log_record_result_status_code int(10) NOT NULL,
  log_record_result_description text NOT NULL,
  log_record_timestamp bigint(20) NOT NULL,
  PRIMARY KEY  (log_record_id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
DROP TABLE IF EXISTS card_table;
CREATE TABLE IF NOT EXISTS card_table (
  card_id bigint(20) NOT NULL auto_increment,
  card_store_id bigint(5) NOT NULL,
  card_code text NOT NULL,
  card_permission bigint(5) NOT NULL,
  card_description text NOT NULL,
  PRIMARY KEY  (card_id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
DROP TABLE IF EXISTS advertisement_table;
CREATE TABLE IF NOT EXISTS advertisement_table (
  advertisement_id bigint(20) NOT NULL auto_increment,
  advertisement_content text NOT NULL,
  advertisement_time_start bigint(20) NOT NULL,
  advertisement_name text NOT NULL,
  advertisement_time_end bigint(20) NOT NULL,
  advertisement_type bigint(5) NOT NULL,
  advertisement_active bigint(5) NOT NULL,
  PRIMARY KEY  (advertisement_id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
DROP TABLE IF EXISTS food_item_of_preservation_table;
CREATE TABLE IF NOT EXISTS food_item_of_preservation_table (
  order_id bigint(20) NOT NULL auto_increment,
  order_preservation_record_id bigint(20) NOT NULL,
  order_food_item_id bigint(20) NOT NULL,
  order_timestamp bigint(20) NOT NULL,
  PRIMARY KEY  (order_id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
DROP TABLE IF EXISTS food_menu_table;
CREATE TABLE IF NOT EXISTS food_menu_table (
  food_menu_id bigint(20) NOT NULL auto_increment,
  food_menu_name text NOT NULL,
  food_menu_price float NOT NULL,
  food_menu_description text NOT NULL,
  PRIMARY KEY  (food_menu_id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;
DROP TABLE IF EXISTS group_table;
CREATE TABLE IF NOT EXISTS group_table (
  group_id bigint(20) NOT NULL auto_increment,
  group_serial int(11) NOT NULL default '0',
  group_identity int(11) NOT NULL,
  PRIMARY KEY  (group_id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
DROP TABLE IF EXISTS `preservation_record`;
CREATE TABLE IF NOT EXISTS preservation_record (
  preservation_record_id bigint(20) NOT NULL auto_increment,
  preservation_record_store_id bigint(20) NOT NULL,
  preservation_record_timestamp bigint(20) NOT NULL,
  preservation_record_type int(5) NOT NULL,
  preservation_record_person_count int(5) NOT NULL,
  preservation_record_tel text NOT NULL,
  preservation_record_result int(5) NOT NULL,
  preservation_record_comment text NOT NULL,
  preservation_record_splitable int(5) NOT NULL,
  preservation_record_status int(11) NOT NULL,
  preservation_record_ticket bigint(20) NOT NULL,
  preservation_record_group int(11) NOT NULL,
  preservation_record_sync int(11) NOT NULL,
  preservation_record_unique_id varchar(255) NOT NULL,
  PRIMARY KEY  (preservation_record_id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;
DROP TABLE IF EXISTS store_table;
CREATE TABLE IF NOT EXISTS store_table (
  store_id int(11) NOT NULL auto_increment,
  store_address text NOT NULL,
  store_tel varchar(255) NOT NULL,
  store_type int(5) NOT NULL,
  store_capability int(5) NOT NULL,
  store_description text NOT NULL,
  store_business_time text NOT NULL,
  store_password text NOT NULL,
  store_lati float NOT NULL,
  store_longi float NOT NULL,
  store_name text NOT NULL,
  store_host varchar(255) NOT NULL,
  PRIMARY KEY  (store_id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;
DROP TABLE IF EXISTS table_table;
CREATE TABLE IF NOT EXISTS table_table (
  table_id bigint(20) NOT NULL auto_increment,
  table_store_id bigint(20) NOT NULL,
  table_status int(5) NOT NULL,
  table_type int(5) NOT NULL,
  table_size int(5) NOT NULL,
  PRIMARY KEY  (table_id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
DROP TABLE IF EXISTS `user_table`;
CREATE TABLE IF NOT EXISTS user_table (
  user_id bigint(20) NOT NULL auto_increment,
  user_name varchar(255) NOT NULL,
  user_password varchar(255) NOT NULL,
  user_store_id int(11) NOT NULL,
  user_device_list text NOT NULL,
  user_mode int(11) NOT NULL,
  user_permission int(11) NOT NULL,
  PRIMARY KEY  (user_id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;
DROP TABLE IF EXISTS `variable_table`;
CREATE TABLE IF NOT EXISTS variable_table (
  variable_key varchar(255) NOT NULL,
  variable_value text NOT NULL,
  PRIMARY KEY  (variable_key)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
INSERT INTO variable_table (variable_key, variable_value) VALUES ('host_role', 'root');