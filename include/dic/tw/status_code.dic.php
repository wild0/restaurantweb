<?php
$operation = array();
$statusCodeDescription = array();
//number 1xx
$operation[101] = "增加廣告內容";
$operation[102] = "修改廣告內容";
$operation[103] = "取得指定廣告內容";
$operation[104] = "移除廣告內容";
$operation[105] = "取得廣告內容列表";
$operation[106] = "取得最新增加廣告內容";

//number 2xx
$operation[201] = "增加菜單內容";
$statusCodeDescription[201200] = "增加菜單成功";
$statusCodeDescription[201500] = "增加菜單失敗";

$operation[202] = "修改菜單內容";
$operation[203] = "列出菜單內容[JQGRID]";
$operation[204] = "移除菜單內容";
$operation[205] = "從Server更新菜單內容";
$operation[206] = "列出菜單內容";




//preservation operation 3xx;


//store 4xx
$operation[401] = "新增分店";
$statusCodeDescription[401200] = "新增分店成功";
$statusCodeDescription[401500] = "新增分店失敗";
$operation[402] = "移除分店";
$statusCodeDescription[402200] = "移除分店成功";
$statusCodeDescription[402500] = "移除分店失敗";
$operation[403] = "修改分店";
$statusCodeDescription[403200] = "修改分店成功";
$statusCodeDescription[403500] = "修改分店失敗";
$operation[406] = "登入分店";
$statusCodeDescription[406200] = "登入分店成功";
$statusCodeDescription[406500] = "登入分店失敗";

//user 5xx
$operation[501] = "使用者{user_name}登入";
$statusCodeDescription[501200] = "使用者{user_name}登入成功";
$statusCodeDescription[501500] = "使用者{user_name}登入失敗";

$operation[502] = "使用者{user_name}登出";
$statusCodeDescription[502200] = "使用者{user_name}登入成功";
$statusCodeDescription[502500] = "使用者{user_name}登入失敗";

$operation[503] = "新增使用者{user_name}";
$statusCodeDescription[503200] = "新增使用者{user_name}成功";
$statusCodeDescription[503500] = "新增使用者{user_name}失敗";

$operation[503] = "移除使用者{user_name}";
$statusCodeDescription[503200] = "移除使用者{user_name}成功";
$statusCodeDescription[503500] = "移除使用者{user_name}失敗";
?>