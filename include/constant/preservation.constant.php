<?php
define("PRESERVATION_RECORD_TABLE","preservation_record");
define("GROUP_TABLE","group_table");

define("PRESERVATION_STATUS_WAIT",1);
define("PRESERVATION_STATUS_CALL",5);
define("PRESERVATION_STATUS_SKIP",4);
define("PRESERVATION_STATUS_SEAT",2);
define("PRESERVATION_STATUS_REMOVE",3);
define("PRESERVATION_STATUS_CANCEL",3);



define("ADD_PRESERVATION_CMD", "add_preservation_cmd");
define("ADD_PRESERVATION_CMD_CODE", 301);
//301
define("LIST_JQGRID_PRESERVATION_CMD", "list_jqgrid_preservation_cmd");
define("LIST_JQGRID_PRESERVATION_CMD_CODE", 302);
//302
define("REMOVE_PRESERVATION_CMD", "remove_preservation_cmd");
define("REMOVE_PRESERVATION_CMD_CODE", 303);
//303
define("LIST_CURRENT_PRESERVATION_OF_GROUP_CMD", "list_current_preservation_of_group_cmd");
define("LIST_CURRENT_PRESERVATION_OF_GROUP_CMD_CODE", 304);
//304
define("LIST_PRESERVATION_BY_TIME_RANGE_CMD", "list_preservation_by_time_range_cmd");
//305
define("LIST_RESERVATION_BY_TODAY_CMD", "list_reservation_by_today_cmd");
//306
define("SEND_SMS_MESSAGE_CMD", "send_sms_message_cmd");
//307
define("WAIT_PRESERVATION_CMD", "wait_preservation_cmd");
//310
define("SEAT_PRESERVATION_CMD", "seat_preservation_cmd");
//311
define("CANCEL_PRESERVATION_CMD", "cancel_preservation_cmd");
//312
define("SKIP_PRESERVATION_CMD", "skip_preservation_cmd");
//314
define("CALL_PRESERVATION_CMD", "call_preservation_cmd");
//315

?>