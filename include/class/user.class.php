<?php
class User extends Object {
	function User(){
		$this->Object();
		$this->initVar('user_id', XOBJ_DTYPE_INT, 0);
		$this->initVar('user_name', XOBJ_DTYPE_TXTBOX, 0);
		$this->initVar('user_realname', XOBJ_DTYPE_TXTBOX, 0);
		$this->initVar('user_password', XOBJ_DTYPE_TXTBOX, 0);
		$this->initVar('user_device_list', XOBJ_DTYPE_TXTBOX, 0);
		$this->initVar('user_store_id', XOBJ_DTYPE_INT, 0);
		$this->initVar('user_mode', XOBJ_DTYPE_INT, 0);
		$this->initVar('user_permission', XOBJ_DTYPE_INT, 0);
		$this->initVar('user_ticket', XOBJ_DTYPE_TXTBOX, 0);
	}
	function getMode(){
		//0:node
		//1:root
		if(getMode()=="node"){
			return 0;
		}
		else if(getMode()=="root"){
			return 1;
		}
	}
}
?>