<?php
class Store extends Object {
	function Store(){
		$this->Object();
		$this->initVar('store_id', XOBJ_DTYPE_INT, 0);
		$this->initVar('store_name', XOBJ_DTYPE_TXTBOX, "");
		$this->initVar('store_address', XOBJ_DTYPE_TXTBOX, "");
		$this->initVar('store_tel', XOBJ_DTYPE_TXTBOX, 0);
		$this->initVar('store_type', XOBJ_DTYPE_INT, 0);
		$this->initVar('store_password', XOBJ_DTYPE_TXTBOX, "");
		
		$this->initVar('store_capability', XOBJ_DTYPE_INT, 0);
		$this->initVar('store_description', XOBJ_DTYPE_TXTBOX, 0);
		$this->initVar('store_lati', XOBJ_DTYPE_INT, 0);
		$this->initVar('store_longi', XOBJ_DTYPE_INT, 0);
		$this->initVar('store_host', XOBJ_DTYPE_TXTBOX, 0);
		
		$this->initVar('store_business_time', XOBJ_DTYPE_TXTBOX, 0);
		
		
	}
}
?>