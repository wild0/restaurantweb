<?php
class PreservationRecord extends Object {
	function PreservationRecord(){
		$this->Object();
		$this->initVar('preservation_record_id', XOBJ_DTYPE_INT, 0);
		$this->initVar('preservation_record_store_id', XOBJ_DTYPE_INT, 0);
		$this->initVar('preservation_record_timestamp', XOBJ_DTYPE_INT, 0);
		$this->initVar('preservation_record_ticket', XOBJ_DTYPE_INT, 0);
		//
		$this->initVar('preservation_record_group', XOBJ_DTYPE_INT, 0);
		$this->initVar('preservation_record_type', XOBJ_DTYPE_INT, 0);
		$this->initVar('preservation_record_status', XOBJ_DTYPE_INT, 0);
		/*
		 * 1:等待
		 * 2:用餐
		 * 3:離開
		 * 4:跳過
		 * 5:呼叫中
		 * 
		 * 
		 */
		
		
		$this->initVar('preservation_record_serial', XOBJ_DTYPE_INT, 0);
		$this->initVar('preservation_record_splitable', XOBJ_DTYPE_INT, 0);
		
		$this->initVar('preservation_record_person_count', XOBJ_DTYPE_INT, 0);
		$this->initVar('preservation_record_tel', XOBJ_DTYPE_TXTBOX, 0);
		$this->initVar('preservation_record_result', XOBJ_DTYPE_INT, 0);
		$this->initVar('preservation_record_comment', XOBJ_DTYPE_TXTBOX, 0);
		
		$this->initVar('preservation_record_sync', XOBJ_DTYPE_TXTBOX, 0);
		$this->initVar('preservation_record_unique_id', XOBJ_DTYPE_TXTBOX, "");	
		/*
		 * 0: 尚未同步
		 * 1: node端同步完成
		 * 2: server端資料建立
		 */
	}
	function toJSON($groupCount = 0, $waitTime = 0){
		$preservation = new stdClass;
		$preservation->id=$this->getVar("preservation_record_id");
		$preservation->store_id=$this->getVar("preservation_record_store_id");
		$preservation->timestamp=$this->getVar("preservation_record_timestamp");
		$preservation->ticket=$this->getVar("preservation_record_ticket");
		$preservation->ticket_label=$this->getTicketLabel();
		
		$preservation->group=$this->getVar("preservation_record_group");
		$preservation->type=$this->getVar("preservation_record_type");
		$preservation->status=$this->getVar("preservation_record_status");
		
		$preservation->serial=$this->getVar("preservation_record_serial");
		$preservation->splitable=$this->getVar("preservation_record_splitable");
		$preservation->person_count=$this->getVar("preservation_record_person_count");
		$preservation->tel=$this->getVar("preservation_record_tel");
		$preservation->result=$this->getVar("preservation_record_result");
		$preservation->comment=$this->getVar("preservation_record_comment");
		$preservation->unique_id=$this->getVar("preservation_record_unique_id");
		
		$preservation->wait_time=$waitTime;
		$preservation->group_count=$groupCount;
		
		return $preservation;
	}
	function getTicketLabel(){
		$ticket = $this->getVar("preservation_record_ticket");
		switch($this->getVar("preservation_record_group")){
			case 0:
				return "A".zerofill($ticket);
				break;
			case 1:
				return "B".zerofill($ticket);
				break;
			case 2:
				return "C".zerofill($ticket);
				break;
			case 3:
				return "D".zerofill($ticket);
				break;
			default:
				return "E".zerofill($ticket);
				break;
		}
	}
}
?>