<?php
define('XOBJ_DTYPE_TXTBOX', 1);
define('XOBJ_DTYPE_TXTAREA', 2);
define('XOBJ_DTYPE_INT', 3);
define('XOBJ_DTYPE_URL', 4);
define('XOBJ_DTYPE_EMAIL', 5);
define('XOBJ_DTYPE_ARRAY', 6);
define('XOBJ_DTYPE_OTHER', 7);
define('XOBJ_DTYPE_SOURCE', 8);
define('XOBJ_DTYPE_STIME', 9);
define('XOBJ_DTYPE_MTIME', 10);
define('XOBJ_DTYPE_LTIME', 11);
class Object {
	var $vars = array();
	/**
	 * variables cleaned for store in DB
	 *
	 * @var array
	 * @access protected
	 */
	var $cleanVars = array();

	/**
	 * is it a newly created object?
	 *
	 * @var bool
	 * @access private
	 */
	var $_isNew = false;

	/**
	 * has any of the values been modified?
	 *
	 * @var bool
	 * @access private
	 */
	var $_isDirty = false;

	/**
	 * errors
	 *
	 * @var array
	 * @access private
	 */
	var $_errors = array();

	/**
	 * additional filters registered dynamically by a child class object
	 *
	 * @access private
	 */
	var $_filters = array();

	function Object() {
	}

	function setNew() {
		$this -> _isNew = true;
	}

	function unsetNew() {
		$this -> _isNew = false;
	}

	function isNew() {
		return $this -> _isNew;
	}

	function setDirty() {
		$this -> _isDirty = true;
	}

	function unsetDirty() {
		$this -> _isDirty = false;
	}

	function isDirty() {
		return $this -> _isDirty;
	}

	/**
	 * initialize variables for the object
	 *
	 * @access public
	 * @param string $key
	 * @param int $data_type  set to one of XOBJ_DTYPE_XXX constants (set to XOBJ_DTYPE_OTHER if no data type ckecking nor text sanitizing is required)
	 * @param mixed
	 * @param bool $required  require html form input?
	 * @param int $maxlength  for XOBJ_DTYPE_TXTBOX type only
	 * @param string $option  does this data have any select options?
	 */
	function initVar($key, $data_type, $value = null, $required = false, $maxlength = null, $options = '') {
		$this -> vars[$key] = array('value' => $value, 'required' => $required, 'data_type' => $data_type, 'maxlength' => $maxlength, 'changed' => false, 'options' => $options);
	}

	/**
	 * assign a value to a variable
	 *
	 * @access public
	 * @param string $key name of the variable to assign
	 * @param mixed $value value to assign
	 */
	function assignVar($key, $value) {
		if (isset($value) && isset($this -> vars[$key])) {
			$this -> vars[$key]['value'] = &$value;
			//echo $key.":".$value;
		}
	}

	/**
	 * assign values to multiple variables in a batch
	 *
	 * @access private
	 * @param array $var_array associative array of values to assign
	 */
	function assignVars($var_arr) {
		//echo "<br>";
		//echo "<br>";
		//echo "<br>";
		//echo "<br>";
		//echo sizeof($var_arr);
		//print_r($var_arr);
		foreach ($var_arr as $key => $value) {
			//echo $key.":".$value."<br>";
			$this -> assignVar($key, $value);

		}
	}

	/**
	 * assign a value to a variable
	 *
	 * @access public
	 * @param string $key name of the variable to assign
	 * @param mixed $value value to assign
	 * @param bool $not_gpc
	 */
	function setVar($key, $value, $not_gpc = false) {
		//echo "1".$key.":".$value."<br>";
		if (!empty($key) && isset($value) && isset($this -> vars[$key])) {
			$this -> vars[$key]['value'] = &$value;
			$this -> vars[$key]['not_gpc'] = $not_gpc;
			$this -> vars[$key]['changed'] = true;
			$this -> setDirty();
		}
		//echo "2".$key.":".$value."<br>";
	}

	/**
	 * assign values to multiple variables in a batch
	 *
	 * @access private
	 * @param array $var_arr associative array of values to assign
	 * @param bool $not_gpc
	 */
	function setVars($var_arr, $not_gpc = false) {
		foreach ($var_arr as $key => $value) {
			$this -> setVar($key, $value, $not_gpc);
		}
	}

	/**
	 * returns all variables for the object
	 *
	 * @access public
	 * @return array associative array of key->value pairs
	 */
	function & getVars() {
		return $this -> vars;
	}

	function getVar($key) {
		if ($this -> vars[$key]['value']) {
			$ret = $this -> vars[$key]['value'];
			return $ret;
		} else {
			if ($this -> vars[$key]['data_type'] == 2) {
				return "";
			} else if ($this -> vars[$key]['data_type'] == 1) {
				return "";
			} else {
				return 0;
			}
		}
	}

	/*
	 function cleanVars(){
	 foreach ($this->vars as $k => $v) {
	 $cleanv = $v['value'];
	 $this->cleanVars[$k] =stripslashes( $cleanv);
	 unset($cleanv);
	 }
	 $this->unsetDirty();
	 return true;
	 }
	 */

	function cleanVars() {

		foreach ($this->vars as $k => $v) {
			$cleanv = $v['value'];
			if (!$v['changed']) {
			} else {
				$cleanv = is_string($cleanv) ? trim($cleanv) : $cleanv;
				switch ($v['data_type']) {
					case XOBJ_DTYPE_TXTBOX :
						if ($v['required'] && $cleanv != '0' && $cleanv == '') {
							$this -> setErrors("$k is required.");
							continue;
						}
						if (isset($v['maxlength']) && strlen($cleanv) > intval($v['maxlength'])) {
							$this -> setErrors("$k must be shorter than " . intval($v['maxlength']) . " characters.");
							continue;
						}
						if (!$v['not_gpc']) {
							$cleanv = $this -> stripSlashesGPC($this -> censorString($cleanv));
						} else {
							$cleanv = $this -> censorString($cleanv);
						}
						break;
					case XOBJ_DTYPE_TXTAREA :
						if ($v['required'] && $cleanv != '0' && $cleanv == '') {
							$this -> setErrors("$k is required.");
							continue;
						}
						if (!$v['not_gpc']) {
							$cleanv = $this -> stripSlashesGPC($ts -> censorString($cleanv));
						} else {
							$cleanv = $ts -> censorString($cleanv);
						}
						break;
					case XOBJ_DTYPE_SOURCE :
						if (!$v['not_gpc']) {
							$cleanv = $ts -> stripSlashesGPC($cleanv);
						} else {
							$cleanv = $cleanv;
						}
						break;
					case XOBJ_DTYPE_INT :
						$cleanv = intval($cleanv);
						break;
					case XOBJ_DTYPE_EMAIL :
						if ($v['required'] && $cleanv == '') {
							$this -> setErrors("$k is required.");
							continue;
						}
						if ($cleanv != '' && !preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+([\.][a-z0-9-]+)+$/i", $cleanv)) {
							$this -> setErrors("Invalid Email");
							continue;
						}
						if (!$v['not_gpc']) {
							$cleanv = $ts -> stripSlashesGPC($cleanv);
						}
						break;
					case XOBJ_DTYPE_URL :
						if ($v['required'] && $cleanv == '') {
							$this -> setErrors("$k is required.");
							continue;
						}
						if ($cleanv != '' && !preg_match("/^http[s]*:\/\//i", $cleanv)) {
							$cleanv = 'http://' . $cleanv;
						}
						if (!$v['not_gpc']) {
							$cleanv = &$ts -> stripSlashesGPC($cleanv);
						}
						break;
					case XOBJ_DTYPE_ARRAY :
						$cleanv = serialize($cleanv);
						break;
					case XOBJ_DTYPE_STIME :
					case XOBJ_DTYPE_MTIME :
					case XOBJ_DTYPE_LTIME :
						$cleanv = !is_string($cleanv) ? intval($cleanv) : strtotime($cleanv);
						break;
					default :
						break;
				}
			}
			$this -> cleanVars[$k] = &$cleanv;
			unset($cleanv);
		}
		if (count($this -> _errors) > 0) {
			return false;
		}
		$this -> unsetDirty();
		return true;
	}

	function stripSlashesGPC($text) {
		if (get_magic_quotes_gpc()) {
			$text = stripslashes($text);
		}
		return $text;
	}

	function & censorString(&$text) {
		/*
		 $ret = $this->executeExtension('censor', $text);
		 if ($ret === false) {
		 return $text;
		 }
		 return $ret;
		 */
		return $text;
	}

}

class ObjectHandler {

	/**
	 * holds referenced to {@link XoopsDatabase} class object
	 *
	 * @var object
	 * @see Database
	 * @access protected
	 */
	var $db;

	//
	/**
	 * called from child classes only
	 *
	 * @param object $db reference to the {@link Database} object
	 * @access protected
	 */
	function ObjectHandler(&$db) {
		$this -> db = &$db;
	}

	function getDb() {

		return $this -> db;
	}

	function & getArrays($objs) {
		for ($i = 0; $i < sizeof($objs); $i++) {
			$ret[$i] = &$objs[$i] -> getVars();
		}
		return $ret;
	}

	/**
	 * creates a new object
	 *
	 * @abstract
	 */
	function & create() {
	}

	

	/**
	 * insert/update object
	 *
	 * @param object $object
	 * @abstract
	 */
	function insert(&$object) {
	}

	/**
	 * delete obejct from database
	 *
	 * @param object $object
	 * @abstract
	 */
	function delete(&$object) {
	}

}
?>