<?php
Class LogRecord extends Object{
	function LogRecord(){
		$this->Object();
		$this->initVar('log_record_id', XOBJ_DTYPE_INT, 0);
		$this->initVar('log_record_type', XOBJ_DTYPE_INT, 0);
		$this->initVar('log_record_mode', XOBJ_DTYPE_INT, 0);
		$this->initVar('log_record_src_host', XOBJ_DTYPE_TXTBOX);
		$this->initVar('log_record_role', XOBJ_DTYPE_INT, 0);
		/*
		 * mobile: 1
		 * pad: 2
		 * application:3
		 * node:4
		 * root:5
		 * 
		 * 
		 */
		$this->initVar('log_record_user_id', XOBJ_DTYPE_INT, 0);
		$this->initVar('log_record_store_id', XOBJ_DTYPE_INT, 0);
		$this->initVar('log_record_operation', XOBJ_DTYPE_TXTBOX);
		/*
		 * ad operation 1xx
		 * food_menu operation 2xx;
		 * preservation operation 3xx;
		 * store operation 4xx;
		 * user operation 5xx;
		 */
		
		$this->initVar('log_record_description', XOBJ_DTYPE_TXTBOX);
		$this->initVar('log_record_result_status_code', XOBJ_DTYPE_INT, 0);
		$this->initVar('log_record_result_description', XOBJ_DTYPE_TXTBOX);
		$this->initVar('log_record_timestamp', XOBJ_DTYPE_INT, 0);
	}
}

?>