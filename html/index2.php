<?php

session_start();

$position="html";



//test
//header ('Content-type:text/html; charset=utf-8');
require_once ("../include/constant/db.constant.php");
require_once (CONSTANT_PATH . "advertisement.constant.php");
require_once (CONSTANT_PATH . "preservation.constant.php");
require_once (INCLUDE_PATH . "header.php");
global $adHandler;
//$adId = $_REQUEST["ad_id"];
$adObject = $adHandler -> getLatest();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>index</title>
		<meta name="author" content="wild0" />
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<!-- Date: 2013-07-16 -->
		<script>
			$(function() {
				//updatePreservations(0);
				var tid = setTimeout(updatePreservations, 2000);

			});

			function showPreservation(tickets) {
				$("#calling_preservation").text(tickets + " Calling now....");
			}

			function hidePreservation() {
				$("#calling_preservation").text("");
			}

			function updatePreservations() {
				var postData = {
					command : "get_current_preservations_cmd"
				};
				var posting = $.post('../include/command/preservation.cmd.php', postData, function(result) {
					//console.log(data);
					var data = result.data;
					if (data.a != undefined) {
						//alert(data.calling);
						preservationsA = data.a;
						$("#group_a_number").text(preservationsA.ticket_label);
					}
					if (data.b != undefined) {
						preservationsB = data.b;
						$("#group_b_number").text(preservationsB.ticket_label);
					}
					if (data.c != undefined) {
						preservationsC = data.c;
						$("#group_c_number").text(preservationsC.ticket_label);
					}
					if (data.d != undefined) {
						preservationsD = data.d;
						$("#group_d_number").text(preservationsD.ticket_label);
					}
					if (data.calling != undefined) {
						var labels = "";
						for(var i=0;i<data.calling.length;i++){
							preservationsCall = data.calling[i];
							labels = labels + preservationsCall.ticket_label;
						}
						
						

						showPreservation(labels)

					} else {
						hidePreservation();
					}

					tid = setTimeout(updatePreservations, 5000);
				}, "json");

			}
		</script>
	</head>
	<body>
		<div>
			<?php
			
			$url = getADURL();
			$postData = array("command" => "get_latest_advertisements_cmd");
			$responseData = redirectPostUrl($url, $postData);
			echo $responseData;
			//if($adObject!=null){
			//	echo $adObject -> getVar("advertisement_content");
			//}
			?>
		</div>
		<div id="calling_preservation">

		</div>
		<table>
			<tr>
				<td>
				<div id="group_a">
					The current number of Group A <span id="group_a_number"></span>
				</div></td>
				<td>
				<div id="group_b">
					The current number of Group B <span id="group_b_number"></span>
				</div></td>
				<td>
				<div id="group_c">
					The current number of Group C <span id="group_c_number"></span>
				</div></td>
				<td>
				<div id="group_d">
					The current number of Group D <span id="group_d_number"></span>
				</div></td>
			</tr>
		</table>
	</body>
</html>
