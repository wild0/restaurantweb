<?php
session_start ();

$position = "html";

require_once ("../include/constant/db.constant.php");
require_once (CONSTANT_PATH . "advertisement.constant.php");
require_once (CONSTANT_PATH . "preservation.constant.php");
require_once (INCLUDE_PATH . "header.php");
global $adHandler;
// $adId = $_REQUEST["ad_id"];
$adObject = $adHandler->getLatest ();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"
	name="viewport" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="content-language" content="en" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="url" content="" />
<meta name="author" content="" />
<meta name="copyright" content="" />
<meta name="distribution" content="iu" />
<title></title>
<link href="../css/xcart.css" rel="stylesheet" type="text/css" />
<link href="../css/global.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]>
		<script src="_includes/js/html5shiv.js"></script>
		<![endif]-->
<script type="text/javascript" src="../js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="../js/main-jquery.js"></script>
<style type="text/css">
html,body,.p-tv-page {
	height: 100%;
}

body {
	background-color: #000104;
}

.p-tv-page {
	width: 100%;
}

.p-tv-page td {
	border: 5px solid #fff;
}

.tv-box {
	background-image: url("_ads/demo.jpg");
}

.p-tv-spotlight {
	vertical-align: top;
}

.p-tv-header {
	background: #000104;
	border-bottom: 5px solid #fff;
	padding: 15px;
}

.p-tv-header h1 {
	font-size: 55px;
	line-height: 55px;
	text-transform: uppercase;
}

.p-tv-header h2 {
	font-size: 50px;
	line-height: 55px;
	text-transform: uppercase;
}

.p-runtopic-box {
	height: 110px;
	overflow: hidden;
}

.p-tv-side {
	text-transform: uppercase;
	text-align: center;
	vertical-align: middle;
}

.p-tv-side h3 {
	font-size: 11vh;
	line-height: 100px;
	padding: 8% 0;
}

.p-tv-side h4 {
	font-size: 25px;
}

.p-tv-side p {
	font-size: 20px;
}
</style>
<script type="text/javascript">
			var ticketArr = new Array();
			Array.prototype.clear = function() {
				while (this.length > 0) {
					this.pop();
				}
			};
			(function($) {

				$.fn.extend({
					RollTitle : function(opt, callback) {
						if (!opt)
							var opt = {};
						var _this = this;
						_this.timer = null;
						//_this.lineH = _this.find("li:first").height();
						//_this.lineH = 55;
						//alert( _this.find("li:first").attr("id"));
						//_this.lineH = _this.find("li:first").attr("id");
						_this.line = opt.line ? parseInt(opt.line, 15) : parseInt(_this.height() / _this.lineH, 10);
						_this.speed = opt.speed ? parseInt(opt.speed, 10) : 3000, //卷动速度，数值越大，速度越慢（毫秒
						_this.timespan = opt.timespan ? parseInt(opt.timespan, 13) : 5000;
						//滚动的时间间隔（毫秒
						if (_this.line == 0)
							this.line = 1;
						//_this.upHeight = 0 - _this.line * _this.lineH;
						_this.scrollUp = function() {
							//alert(_this.find('li').length);
							updatePreservations();
							
							
							if (_this.find('li').length > 1) {
								//元素要大於一才能動
								_this.lineH = _this.find("li:first").height();
								_this.upHeight = 0 - _this.line * _this.lineH;

								var first = _this.find("li:first");

								//alert(_this.upHeight);
								_this.animate({
									marginTop : _this.upHeight
								}, _this.speed, function() {
									for ( i = 1; i <= _this.line; i++) {
										if($.inArray(first.attr("id"), ticketArr)!=-1){
										//if ($.inArray(first.attr("id"), ticketArr)) {
											_this.find("li:first").appendTo(_this);
										}
										else{
											_this.find("li:first").remove();
										}
									}
									_this.css({
										marginTop : 0
									});
								});
							}
						}
						_this.hover(function() {
							clearInterval(_this.timer);
						}, function() {
							_this.timer = setInterval(function() {
								_this.scrollUp();
							}, _this.timespan);
						}).mouseout();
					}
				})
			})(jQuery);
			$(document.body).ready(function() {
				//var tid = setTimeout(updatePreservations, 1000);
				updatePreservations();
				$("#runtopic").RollTitle({
					line : 1,
					speed : 500,
					timespan : 3000
				});

			});
			/*
			 function showPreservation(tickets) {
			 $("#calling_preservation").text(tickets + " Calling now....");
			 }

			 function hidePreservation() {
			 $("#calling_preservation").text("");
			 }
			 */

			function updatePreservations() {
				var postData = {
					command : "list_current_preservation_of_group_cmd"
				};
				var posting = $.post('../include/command/preservation.cmd.php', postData, function(result) {
					//console.log(data);
					var data = result.data;
					if (data.a != undefined) {
						//alert(data.calling);
						preservationsA = data.a;
						$("#group_a_number").text(preservationsA.ticket_label);
					}
					else{
						$("#group_a_number").text("");
					}
					if (data.b != undefined) {
						preservationsB = data.b;
						$("#group_b_number").text(preservationsB.ticket_label);
					}
					else{
						$("#group_b_number").text("");
					}
					if (data.c != undefined) {
						preservationsC = data.c;
						$("#group_c_number").text(preservationsC.ticket_label);
					}
					else{
						$("#group_c_number").text("");
					}
					
					if (data.calling != undefined) {
						//var labels = "";
						ticketArr.clear();
						
						for (var i = 0; i < data.calling.length; i++) {

							preservationsCall = data.calling[i];
							var ticketLabel = preservationsCall.ticket_label;
							ticketArr.push("runtopic_" + ticketLabel);

							if ($("#runtopic_" + preservationsCall.ticket_label).length == 0) {
								var id = "runtopic_" + preservationsCall.ticket_label;
								var li = $("<li>").attr("id", id).appendTo($("#runtopic"));
								var div = $("<div>").addClass("clears").appendTo(li);
								$("<h1>").addClass("f1").html("Now calling number <span class=this-current>" + preservationsCall.ticket_label + "</span>").appendTo(div);
								$("<h1>").html("現在邀請<span class=this-current>"+preservationsCall.ticket_label+"</span>號的客人入座。").appendTo(div);
								//alert(id);
							}

							//labels = labels + preservationsCall.ticket_label;
						}
						$("#runtopic").show();
						//showPreservation(labels)

					} else {
						//hidePreservation();
						$("#runtopic").empty();
						if ($("#runtopic_none").length == 0) {
								var id = "runtopic_none";
								var li = $("<li>").attr("id", id).appendTo($("#runtopic"));
								var div = $("<div>").addClass("clears").appendTo(li);
								$("<h1>").appendTo(div);
								$("<h1>").appendTo(div);
								//alert(id);
							}
						
						//$("#runtopic").hide();
					}

					//tid = setTimeout(updatePreservations, 5000);
				}, "json");

			}
		</script>

</head>
<body>


	<table cellspacing="0" class="p-tv-page">
		<tr>
			<td width="80%" rowspan="3" class="p-tv-spotlight">
				<div class="p-tv-header">
					<div class="p-runtopic-box">
						<ul id="runtopic">
							<!-- 
						<li>
						  <h1>Now calling number <span class="this-current">A20</span></h1>
						  <h2>現在邀請<span class="this-current">A20</span>號的客人入座。</h2>
						</li>
						<li>
						  <h1>Now calling number <span class="this-current">A21</span></h1>
						  <h2>現在邀請<span class="this-current">A21</span>號的客人入座。</h2>
						</li>
						-->
						</ul>
					</div>
				</div>
				<div class="tv-box">
					<div style="height: 505px">
				<?php
				
				$url = getADURL ();
				//echo $url;
				$postData = array (
						"command" => "get_latest_advertisement_cmd" 
				);
				$responseData = redirectPostUrl ( $url, $postData );
				echo $responseData;
				// if($adObject!=null){
				// echo $adObject -> getVar("advertisement_content");
				// }
				?>
				
					</div>
				</div>
			</td>
			<td width="20%" class="p-tv-side" style="height: 33%">
				<h4>GROUP A / A組</h4>
				<h3 id="group_a_number"></h3>
				<p>[ 1-2 people / 1-2人 ]</p>
			</td>
		</tr>
		<tr>
			<td class="p-tv-side" style="height: 34%">
				<h4>GROUP B / B組</h4>
				<h3 id="group_b_number"></h3>
				<p>[ 3-4 people / 3-4人 ]</p>
			</td>
		</tr>
		<tr>
			<td class="p-tv-side" style="height: 33%">
				<h4>GROUP C / C組</h4>
				<h3 id="group_c_number"></h3>
				<p>[ 5+ people / 5+人 ]</p>
			</td>
		</tr>

	</table>


</body>
</html>