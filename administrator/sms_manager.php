<?php
 /**
 * Example Application

 * @package Example-application
 */

session_start();

require_once("../include/constant/db.constant.php");
require_once(INCLUDE_PATH."header.php");
global $variableHandler;

require('../libs/Smarty.class.php');

$smarty = new Smarty;



//$smarty->force_compile = true;
$smarty->debugging = $debug;
$smarty->caching = $cache;
$smarty->cache_lifetime = 120;
$role = $variableHandler->getVariable(HOST_ROLE);
$smsAccountSid = $variableHandler->getVariable(SMS_ACCOUNT_SID);

$smsAuthToken = $variableHandler->getVariable(SMS_AUTH_TOKEN);

//$storeId = $variableHandler->getVariable(NODE_STORE_ID);

$smarty->assign("login", $login);
$smarty->assign("mode", $mode);

$smarty->assign('role',$role);
$smarty->assign('sms_sender_number',$smsSenderNumber);
$smarty->assign('sms_account_sid',$smsAccountSid);
$smarty->assign('sms_auth_token',$smsAuthToken);


$smarty->display('sms_manager.tpl');
?>