<?php
 /**
 * Example Application

 * @package Example-application
 */
 
 
 session_start();
 $position = "log";
require_once("../include/constant/db.constant.php");
require_once(CONSTANT_PATH."advertisement.constant.php");
require_once(INCLUDE_PATH."header.php");
 
require('../libs/Smarty.class.php');

$smarty = new Smarty;



//$smarty->force_compile = true;
$smarty->debugging = $debug;
$smarty->caching = $cache;
$smarty->cache_lifetime = 120;

global $userHandler, $variableHandler;
$login = $userHandler->isLogin();
$mode = getMode();

$smarty->assign("login", $login);
$smarty->assign("mode", $mode);



$groupAMinValue = $variableHandler->getVariable("group_a_min_value", 0);
$groupAMaxValue = $variableHandler->getVariable("group_a_max_value", 0);
$groupBMinValue = $variableHandler->getVariable("group_b_min_value", 0);
$groupBMaxValue = $variableHandler->getVariable("group_b_max_value", 0);
$groupCMinValue = $variableHandler->getVariable("group_c_min_value", 0);
$groupCMaxValue = $variableHandler->getVariable("group_c_max_value", 0);
$groupDMinValue = $variableHandler->getVariable("group_d_min_value", 0);
$groupDMaxValue = $variableHandler->getVariable("group_d_max_value", 0);

$smarty->assign("group_a_min_value", $groupAMinValue);
$smarty->assign("group_a_max_value", $groupAMaxValue);
$smarty->assign("group_b_min_value", $groupBMinValue);
$smarty->assign("group_b_max_value", $groupBMaxValue);
$smarty->assign("group_c_min_value", $groupCMinValue);
$smarty->assign("group_c_max_value", $groupCMaxValue);
$smarty->assign("group_d_min_value", $groupDMinValue);
$smarty->assign("group_d_max_value", $groupDMaxValue);

$smarty->display('group_define.tpl');
?>