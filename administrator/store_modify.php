<?php
 /**
 * Example Application

 * @package Example-application
 */
session_start();
$position = "store";

require_once("../include/constant/db.constant.php");
require_once(CONSTANT_PATH."advertisement.constant.php");
require_once(INCLUDE_PATH."header.php");

require('../libs/Smarty.class.php');

$smarty = new Smarty;



//$smarty->force_compile = true;
$smarty->debugging = $debug;
$smarty->caching = $cache;
$smarty->cache_lifetime = 120;

global $userHandler;
$login = $userHandler->isLogin();
$mode = getMode();

$smarty->assign("login", $login);
$smarty->assign("mode", $mode);

global $storeHandler;
$storeId = $_REQUEST["store_id"];
$store = $storeHandler->get($storeId);
$smarty->assign("store_id", $store->getVar("store_id"));
$smarty->assign("store_name", $store->getVar("store_name"));
$smarty->assign("store_address", $store->getVar("store_address"));
$smarty->assign("store_tel", $store->getVar("store_tel"));
$smarty->assign("store_password", $store->getVar("store_password"));
$smarty->assign("store_business_time", $store->getVar("store_business_time"));
$smarty->assign("store_lati", $store->getVar("store_lati"));
$smarty->assign("store_longi", $store->getVar("store_longi"));
$smarty->assign("store_description", $store->getVar("store_description"));




$smarty->display('store_modify.tpl');
?>