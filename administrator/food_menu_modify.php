<?php
 /**
 * Example Application

 * @package Example-application
 */
session_start();
$position = "food_menu";

require_once("../include/constant/db.constant.php");
require_once(CONSTANT_PATH."advertisement.constant.php");
require_once(INCLUDE_PATH."header.php");

require('../libs/Smarty.class.php');

$smarty = new Smarty;



//$smarty->force_compile = true;
$smarty->debugging = $debug;
$smarty->caching = $cache;
$smarty->cache_lifetime = 120;

global $userHandler;
$login = $userHandler->isLogin();
$mode = getMode();

$smarty->assign("login", $login);
$smarty->assign("mode", $mode);

global $foodMenuHandler;
$foodMenuId = $_REQUEST["food_menu_id"];
$foodMenu = $foodMenuHandler->get($foodMenuId);
$smarty->assign("food_menu_id", $foodMenu->getVar("food_menu_id"));
$smarty->assign("food_menu_name", $foodMenu->getVar("food_menu_name"));
$smarty->assign("food_menu_price", $foodMenu->getVar("food_menu_price"));
$smarty->assign("food_menu_description", $foodMenu->getVar("food_menu_description"));


$smarty->display('food_menu_modify.tpl');
?>