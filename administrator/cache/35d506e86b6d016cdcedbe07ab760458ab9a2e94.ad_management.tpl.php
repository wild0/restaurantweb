<?php /*%%SmartyHeaderCode:14369524d9c8a167e99-03583267%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '35d506e86b6d016cdcedbe07ab760458ab9a2e94' => 
    array (
      0 => '.\\templates\\ad_management.tpl',
      1 => 1380531352,
      2 => 'file',
    ),
    '78dfd564017866b2a07d32220e5f711a454b3ecb' => 
    array (
      0 => '.\\templates\\nav.tpl',
      1 => 1380745495,
      2 => 'file',
    ),
    '1be7ff7fdee636597edd726ee98dfef4bfd55d1f' => 
    array (
      0 => '.\\templates\\footer.tpl',
      1 => 1380528780,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14369524d9c8a167e99-03583267',
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_524f99154eee98_63132122',
  'has_nocache_code' => false,
  'cache_lifetime' => 120,
),true); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_524f99154eee98_63132122')) {function content_524f99154eee98_63132122($_smarty_tpl) {?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
		<title>Boiling Point :: Manage Ads</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<script src="../js/jqgrid/jquery.min.js" type="text/javascript"></script>
		<script src="../js/jqgrid/jquery-ui-1.8.2.custom.min.js" type="text/javascript"></script>
		
		<script type="text/javascript" src="js/main-jquery.js"></script>

		<link rel="stylesheet" type="text/css" media="screen" href="../js/jqgrid/themes/redmond/jquery-ui-1.8.2.custom.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="../js/jqgrid/themes/ui.jqgrid.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="../js/jqgrid/themes/ui.multiselect.css" />

		
		<script src="../js/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
		<script type="text/javascript">
			$.jgrid.no_legacy_api = true;
			$.jgrid.useJSON = true;
		</script>
		<script src="../js/jqgrid/jquery.jqGrid.min.js" type="text/javascript"></script>
		
		<!-- Date: 2013-07-16 -->
		<link rel="shortcut icon" type="image/png" href="http://www.bpgroupusa.com/favicon.ico">

		<link href="css/global.css" rel="stylesheet" type="text/css">
		<link href="css/stylesheet.css" rel="stylesheet" type="text/css">

		
		<script>
			$(function() {
				jQuery("#ad_information_table").jqGrid({
					ajaxGridOptions : {
						type : "POST"
					},
					serializeGridData : function(postdata) {
						postdata.page = 0;
						postdata.command = "get_jqgrid_advertisements_cmd";
						return postdata;
					},
					url : '../include/command/ad.cmd.php',
					datatype : "json",
					colNames : ['ID', 'Title', 'Start Time', 'End Time', 'Active', 'modify', 'delete'],
					colModel : [{
						name : 'id',
						index : 'id',
						width : 55
					}, {
						name : 'title',
						index : 'title',
						width : 90
					}, {
						name : 'start_time',
						index : 'start_time',
						width : 100
					}, {
						name : 'end_time',
						index : 'end_time',
						width : 80,
						align : "right"
					}, {
						name : 'active',
						index : 'active',
						width : 80,
						align : "right"
					}, {
						name : 'ad_modify_action',
						index : 'action1',
						width : 40,
						sortable : false
					}, {
						name : 'ad_delete_action',
						index : 'action2',
						width : 40,
						sortable : false
					}],
					rowNum : 10,
					width : 700,
					rowList : [10, 20, 30],
					pager : '#ad_information_pager',
					sortname : 'invdate',
					viewrecords : true,
					sortorder : "desc",
					caption : "Advertisement Information",
					loadComplete : function() {
						var ids = $("#ad_information_table").jqGrid('getDataIDs');
						for (var i = 0; i < ids.length; i++) {
							var cl = ids[i];
							var data = $("#food_menu_information_table").jqGrid('getRowData', cl);

							var modifyButton = "<span title='編輯' class='img_btn'  onClick='modifyAdOperation(" + cl + ")' >modify</span>";
							var deleteButton = "<span title='刪除' class='img_btn' onClick='deleteAdOperation(" + cl + ")' >delete</span>";

							$("#ad_information_table").jqGrid('setRowData', ids[i], {
								ad_modify_action : modifyButton
							});
							$("#ad_information_table").jqGrid('setRowData', ids[i], {
								ad_delete_action : deleteButton
							});

						}
					}
				});
				jQuery("#ad_information_pager").jqGrid('navGrid', '#ad_information_pager', {
					edit : false,
					add : false,
					del : false
				});
			});
			function deleteAdOperation(adId) {
				//jQuery("#store_information_table").jqGrid().trigger( 'reloadGrid' );

				$.post(cmdUrl + "ad.cmd.php", {
					command : "remove_ad_cmd",
					ad_id : adId
				}, function(data, textStatus) {
					jQuery("#ad_information_table").jqGrid().trigger('reloadGrid');
				}, "json");

			}

			function modifyAdOperation(adId) {
				//jQuery("#store_information_table").jqGrid().trigger( 'reloadGrid' );
				var url = "ad_modify.php?";
				parent.location = url + "ad_id=" + adId;

			}
		</script>

	</head>

	<body class="body-bg-01">

		<!-- Begin Page -->
		<div id="wrap">

			<div id="wp-header">
				<div id="header" class="clears">
					<div id="logo" class="fl">
						<a href="http://www.bpgroupusa.com/"><img src="images/logo.png" alt="logo" height="117" width="117"></a>
					</div>
					<div id="nav">
						<ul>
														<li>
								<a class="menu-head" href="ad_management.php" id="nav-ad" ><span class="english">ads</span><span class="chinese">廣告</span></a>
							</li>
																					<li>
								<a class="menu-head" href="food_menu_management.php" id="nav-food"><span class="english">food</span><span class="chinese">美食</span></a>
							</li>
														
														<li>
								<a class="menu-head" href="store_management.php" id="nav-store"><span class="english">store locations</span><span class="chinese">沸點分店</span></a>
							</li>
														
							<li>
								<a class="menu-head" href="preservation_management.php" id="nav-reservation"><span class="english">reservations</span><span class="chinese">訂位</span></a>
								<div style="display: none;" class="menu-body clears">
									<dl>
										<dd>
											<a href="preservation_status_management.php">Current Reservations</a>
										</dd>
										<dd>
											<a href="#">History Logs</a>
										</dd>
										<dd>
											<a href="#">Set Waiting Time</a>
										</dd>
										<dd>
											<a href="#">SMS Settings</a>
										</dd>
										<dd>
											<a href="#">Define Groups</a>
										</dd>
									</dl>
								</div>
							</li>
							<!--
							<li>
								<a class="menu-head" href="#" id="nav-card-reader"><span class="english">card reader</span><span class="chinese">讀卡機</span></a>
							</li>
							-->
							<li>
								<a class="menu-head" href="user_management.php" id="nav-user"><span class="english">users</span><span class="chinese">使用者</span></a>
							</li>
							
														<li>
								<a class="menu-head" href="../include/command/user.cmd.php?command=logout_cmd&platform=web&return_url=login" id="nav-logout"><span class="english">logout</span><span class="chinese">登出</span></a>
							</li>
													</ul>
					</div><!-- /#nav -->
				</div>
			</div><!-- /#wp-header -->

			<!-- Begin content -->
			<div id="wp-content">
				<div id="content">
					<div id="content-header">
						<div class="b-menu">
							<ul class="clears">
								<li>
									<a href="http://www.bpgroupusa.com/">Home</a>
								</li>
								<li>
									/
								</li>
								<li>
									Manage Ads
								</li>
							</ul>
						</div>
						<div class="content-title">
							<h1 class="clears">Manage Ads</h1>
						</div>
					</div>
					<div class="main">

						<div>
							<table id="ad_information_table"></table>
							<div id="ad_information_pager"></div>
						</div>

						<p class="admin-form__submit">
							<input class="submit-btn" value="new ad" type="submit"  onclick="location.assign('ad_insert.php')">
						</p>

					</div>
				</div>
			</div><!-- /#wp-content -->

			<!-- Begin footer -->
			<div id="wp-footer">
				<div id="footer">
					<div class="foot-main clears">
						<div class="foot-logo fl"><img src="images/foot-logo.png" alt="" height="92" width="101">
						</div>
						<div class="site-map fl clears">
							<ul>
								<li>
									<a href="#">Ads</a>
								</li>
								<li>
									<a href="#">Food</a>
								</li>
								<li>
									<a href="#">Store Locations</a>
								</li>
								<li>
									<a href="#">Card Reader</a>
								</li>
								<li>
									<a href="#">Users</a>
								</li>
							</ul>
							<ul>
								<li>
									<a href="#">Reservations</a>
								</li>
								<li>
									<a href="#">History Logs</a>
								</li>
								<li>
									<a href="#">Set Waiting Time</a>
								</li>
								<li>
									<a href="#">SMS Settings</a>
								</li>
								<li>
									<a href="#">Define Groups</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div><!-- /#wp-footer -->

		</div><!-- /#wrap -->

	</body>
</html><?php }} ?>