<?php /*%%SmartyHeaderCode:290585249481cdc08e9-25736513%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5bf0291c3913cfbb42c93c8885203f3f409db70d' => 
    array (
      0 => '.\\templates\\preservation_management.tpl',
      1 => 1380725677,
      2 => 'file',
    ),
    '78dfd564017866b2a07d32220e5f711a454b3ecb' => 
    array (
      0 => '.\\templates\\nav.tpl',
      1 => 1380745495,
      2 => 'file',
    ),
    '1be7ff7fdee636597edd726ee98dfef4bfd55d1f' => 
    array (
      0 => '.\\templates\\footer.tpl',
      1 => 1380528780,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '290585249481cdc08e9-25736513',
  'cache_lifetime' => 120,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_524d9c877ed143_25894767',
  'has_nocache_code' => false,
),true); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_524d9c877ed143_25894767')) {function content_524d9c877ed143_25894767($_smarty_tpl) {?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<title>Boiling Point :: Manage Reservations</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<link rel="shortcut icon" type="image/png" href="http://www.bpgroupusa.com/favicon.ico">

<link href="css/global.css" rel="stylesheet" type="text/css">
<link href="css/stylesheet.css" rel="stylesheet" type="text/css">

<script src="../js/jqgrid/jquery.min.js" type="text/javascript"></script> 
<script src="../js/jqgrid/jquery-ui-1.8.2.custom.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/main-jquery.js"></script>

<link rel="stylesheet" type="text/css" media="screen" href="../js/jqgrid/themes/redmond/jquery-ui-1.8.2.custom.css" /> 
    	<link rel="stylesheet" type="text/css" media="screen" href="../js/jqgrid/themes/ui.jqgrid.css" />
    	<link rel="stylesheet" type="text/css" media="screen" href="../js/jqgrid/themes/ui.multiselect.css" />
		
		
    	<script src="../js/jqgrid//i18n/grid.locale-en.js" type="text/javascript"></script> 
    	<script type="text/javascript"> 
    	$.jgrid.no_legacy_api = true; 
    	$.jgrid.useJSON = true; 
    	</script> 
    	<script src="../js/jqgrid/jquery.jqGrid.min.js" type="text/javascript"></script> 
    	
		<!-- Date: 2013-07-16 -->

		<script>
		$(function() {
			jQuery("#preservation_information_table").jqGrid({
				ajaxGridOptions : {
					type : "POST"
				},
				serializeGridData : function(postdata) {
					postdata.page = 1;
					postdata.command="get_jqgrid_preservations_cmd";
					postdata.store_id="-1";
					return postdata;
				},
				url : '../include/command/preservation.cmd.php',
				datatype : "json",
				colNames : ['ID', 'Store', 'Time', 'Splite Group', 'Persons','Tel'],
				colModel : [{
					name : 'id',
					index : 'id',
					width : 55
				}, {
					name : 'store_id',
					index : 'store_id',
					width : 90
				}, {
					name : 'preservation_time',
					index : 'preservation_time',
					width : 100
				}, {
					name : 'splite_group',
					index : 'splite_group',
					width : 80,
					align : "right"
				}, {
					name : 'preservation_persons',
					index : 'preservation_persons',
					width : 80,
					align : "right"
				}, {
					name : 'preservation_tel',
					index : 'preservation_tel',
					width : 80,
					align : "right"
				}],
				rowNum : 10,
				width : 700,
				rowList : [10, 20, 30],
				pager : '#preservation_information_pager',
				sortname : 'invdate',
				viewrecords : true,
				sortorder : "desc",
				caption : "Preservation Information"
			});
			jQuery("#preservation_information_pager").jqGrid('navGrid', '#preservation_information_pager', {
				edit : false,
				add : false,
				del : false
			});
		});
		function reloadByStoreId(storeId){
			
			$("#preservation_information_table").jqGrid('setGridParam', { serializeGridData : function(postdata) {
					postdata.page = 1;
					postdata.command="get_jqgrid_preservations_cmd";
					postdata.store_id=storeId;
					return postdata;
				} });
			$("#preservation_information_table").jqGrid().trigger( 'reloadGrid');
		}
		</script>

</head>

<body class="body-bg-01">

<!-- Begin Page -->
<div id="wrap">

	<div id="wp-header">
		<div id="header" class="clears">
			<div id="logo" class="fl"> <a href="http://www.bpgroupusa.com/"><img src="images/logo.png" alt="logo" height="117" width="117"></a> </div>
			<div id="nav">
						<ul>
														<li>
								<a class="menu-head" href="ad_management.php" id="nav-ad" ><span class="english">ads</span><span class="chinese">廣告</span></a>
							</li>
																					<li>
								<a class="menu-head" href="food_menu_management.php" id="nav-food"><span class="english">food</span><span class="chinese">美食</span></a>
							</li>
														
														<li>
								<a class="menu-head" href="store_management.php" id="nav-store"><span class="english">store locations</span><span class="chinese">沸點分店</span></a>
							</li>
														
							<li>
								<a class="menu-head" href="preservation_management.php" id="nav-reservation"><span class="english">reservations</span><span class="chinese">訂位</span></a>
								<div style="display: none;" class="menu-body clears">
									<dl>
										<dd>
											<a href="preservation_status_management.php">Current Reservations</a>
										</dd>
										<dd>
											<a href="#">History Logs</a>
										</dd>
										<dd>
											<a href="#">Set Waiting Time</a>
										</dd>
										<dd>
											<a href="#">SMS Settings</a>
										</dd>
										<dd>
											<a href="#">Define Groups</a>
										</dd>
									</dl>
								</div>
							</li>
							<!--
							<li>
								<a class="menu-head" href="#" id="nav-card-reader"><span class="english">card reader</span><span class="chinese">讀卡機</span></a>
							</li>
							-->
							<li>
								<a class="menu-head" href="user_management.php" id="nav-user"><span class="english">users</span><span class="chinese">使用者</span></a>
							</li>
							
													</ul>
					</div><!-- /#nav -->
		</div>
	</div><!-- /#wp-header -->

	<!-- Begin content -->
	<div id="wp-content">
		<div id="content">
			<div id="content-header">
				<div class="b-menu">
					<ul class="clears">
						<li><a href="http://www.bpgroupusa.com/">Home</a></li>
						<li>/</li>
						<li>Manage Reservations</li>
					</ul>
				</div>
				<div class="content-title">
					<h1 class="clears">Manage Reservations</h1>
				</div>
			</div>
			<div class="main">



				<div>
				
				<table id="preservation_information_table"></table>
				<div id="preservation_information_pager"></div>
				
				</div>



				
			</div>
		</div>
	</div><!-- /#wp-content --> 
  
	<!-- Begin footer -->
			<div id="wp-footer">
				<div id="footer">
					<div class="foot-main clears">
						<div class="foot-logo fl"><img src="images/foot-logo.png" alt="" height="92" width="101">
						</div>
						<div class="site-map fl clears">
							<ul>
								<li>
									<a href="#">Ads</a>
								</li>
								<li>
									<a href="#">Food</a>
								</li>
								<li>
									<a href="#">Store Locations</a>
								</li>
								<li>
									<a href="#">Card Reader</a>
								</li>
								<li>
									<a href="#">Users</a>
								</li>
							</ul>
							<ul>
								<li>
									<a href="#">Reservations</a>
								</li>
								<li>
									<a href="#">History Logs</a>
								</li>
								<li>
									<a href="#">Set Waiting Time</a>
								</li>
								<li>
									<a href="#">SMS Settings</a>
								</li>
								<li>
									<a href="#">Define Groups</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div><!-- /#wp-footer -->

</div><!-- /#wrap --> 
</body>
</html><?php }} ?>