<?php /*%%SmartyHeaderCode:15883524c3a7293f9b3-56823360%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2e09b8dfdd13154a162423db225acbe8b8afcf26' => 
    array (
      0 => '.\\templates\\user_insert.tpl',
      1 => 1380743487,
      2 => 'file',
    ),
    '78dfd564017866b2a07d32220e5f711a454b3ecb' => 
    array (
      0 => '.\\templates\\nav.tpl',
      1 => 1380745495,
      2 => 'file',
    ),
    '1be7ff7fdee636597edd726ee98dfef4bfd55d1f' => 
    array (
      0 => '.\\templates\\footer.tpl',
      1 => 1380528780,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '15883524c3a7293f9b3-56823360',
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_524c8197ceb7b9_08667668',
  'has_nocache_code' => false,
  'cache_lifetime' => 120,
),true); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_524c8197ceb7b9_08667668')) {function content_524c8197ceb7b9_08667668($_smarty_tpl) {?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<title>Boiling Point :: Add Store</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<link rel="shortcut icon" type="image/png" href="http://www.bpgroupusa.com/favicon.ico">

<link href="css/global.css" rel="stylesheet" type="text/css">
<link href="css/stylesheet.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="js/jquery-1.js"></script>
<script type="text/javascript" src="js/main-jquery.js"></script>

</head>

<body class="body-bg-01">

<!-- Begin Page -->
<div id="wrap">

	<div id="wp-header">
		<div id="header" class="clears">
			<div id="logo" class="fl"> <a href="http://www.bpgroupusa.com/"><img src="images/logo.png" alt="logo" height="117" width="117"></a> </div>
			<div id="nav">
						<ul>
														<li>
								<a class="menu-head" href="ad_management.php" id="nav-ad" ><span class="english">ads</span><span class="chinese">廣告</span></a>
							</li>
																					<li>
								<a class="menu-head" href="food_menu_management.php" id="nav-food"><span class="english">food</span><span class="chinese">美食</span></a>
							</li>
														
														<li>
								<a class="menu-head" href="store_management.php" id="nav-store"><span class="english">store locations</span><span class="chinese">沸點分店</span></a>
							</li>
														
							<li>
								<a class="menu-head" href="preservation_management.php" id="nav-reservation"><span class="english">reservations</span><span class="chinese">訂位</span></a>
								<div style="display: none;" class="menu-body clears">
									<dl>
										<dd>
											<a href="preservation_status_management.php">Current Reservations</a>
										</dd>
										<dd>
											<a href="#">History Logs</a>
										</dd>
										<dd>
											<a href="#">Set Waiting Time</a>
										</dd>
										<dd>
											<a href="#">SMS Settings</a>
										</dd>
										<dd>
											<a href="#">Define Groups</a>
										</dd>
									</dl>
								</div>
							</li>
							<!--
							<li>
								<a class="menu-head" href="#" id="nav-card-reader"><span class="english">card reader</span><span class="chinese">讀卡機</span></a>
							</li>
							-->
							<li>
								<a class="menu-head" href="user_management.php" id="nav-user"><span class="english">users</span><span class="chinese">使用者</span></a>
							</li>
							
														<li>
								<a class="menu-head" href="../include/command/user.cmd.php?command=logout_cmd&platform=web&return_url=login" id="nav-logout"><span class="english">logout</span><span class="chinese">登出</span></a>
							</li>
													</ul>
					</div><!-- /#nav -->
		</div>
	</div><!-- /#wp-header -->

	<!-- Begin content -->
	<div id="wp-content">
		<div id="content">
			<div id="content-header">
				<div class="b-menu">
					<ul class="clears">
						<li><a href="http://www.bpgroupusa.com/">Home</a></li>
						<li>/</li>
						<li>Add User</li>
					</ul>
				</div>
				<div class="content-title">
					<h1 class="clears">Add User</h1>
				</div>
			</div>
			<div class="main admin-form">
				
				<form method="post" action="../include/command/user.cmd.php">
					
					<input type="hidden" name="command" value="add_user_cmd" />
					<input type="hidden" name="return_url" value="true">
					
					<h5><label for="user_name_field">User Name:</label></h5>
					<input id="user_name_field" name="uname_value" type="text">
					
					<h5><label for="user_password_field">Password:</label></h5>
					<input id="user_password_field" name="password_value" type="password">
					
					<h5><label for="user_device_field">Device:</label></h5>
					<input id="user_device_field" name="device_value" type="text">
					
					<p class="admin-form__submit"><input class="submit-btn" value="submit" type="submit"> or <a href="#">Cancel</a></p>
				</form>
				
			</div><!-- /.admin-form --> 
		</div>
	</div><!-- /#wp-content --> 
  
	<!-- Begin footer -->
			<div id="wp-footer">
				<div id="footer">
					<div class="foot-main clears">
						<div class="foot-logo fl"><img src="images/foot-logo.png" alt="" height="92" width="101">
						</div>
						<div class="site-map fl clears">
							<ul>
								<li>
									<a href="#">Ads</a>
								</li>
								<li>
									<a href="#">Food</a>
								</li>
								<li>
									<a href="#">Store Locations</a>
								</li>
								<li>
									<a href="#">Card Reader</a>
								</li>
								<li>
									<a href="#">Users</a>
								</li>
							</ul>
							<ul>
								<li>
									<a href="#">Reservations</a>
								</li>
								<li>
									<a href="#">History Logs</a>
								</li>
								<li>
									<a href="#">Set Waiting Time</a>
								</li>
								<li>
									<a href="#">SMS Settings</a>
								</li>
								<li>
									<a href="#">Define Groups</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div><!-- /#wp-footer --> 

</div><!-- /#wrap --> 
</body>
</html><?php }} ?>