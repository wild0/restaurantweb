<?php
 /**
 * Example Application

 * @package Example-application
 */

session_start();

require_once("../include/constant/db.constant.php");
require_once(INCLUDE_PATH."header.php");
global $variableHandler;
if(isset($_POST["database_reset"]) && $_POST["database_reset"]=="yes"){
	if($_POST["host_role"]=="root"){
		importSQL("restaurant_root.sql");
	}
	else if($_POST["host_role"]=="node"){
		importSQL("restaurant_node.sql");
	}
}
if(isset($_POST["host_role"])){
	$variableHandler->setVariable(HOST_ROLE,$_POST["host_role"]);
}
if(isset($_POST["root_ip"])){
	$variableHandler->setVariable(ROOT_IP,$_POST["root_ip"]);
}
if(isset($_POST["node_store_id"])){
	$variableHandler->setVariable(NODE_STORE_ID,$_POST["node_store_id"]);
}

 
require('../libs/Smarty.class.php');

$smarty = new Smarty;



//$smarty->force_compile = true;
$smarty->debugging = $debug;
$smarty->caching = $cache;
$smarty->cache_lifetime = 120;

$role = $variableHandler->getVariable(HOST_ROLE);

$rootIP = $variableHandler->getVariable(ROOT_IP);

$storeId = $variableHandler->getVariable(NODE_STORE_ID);



$smarty->assign('role',$role);
$smarty->assign('rootIP',$rootIP);
$smarty->assign('storeId',$storeId);

$smarty->display('status.tpl');
?>