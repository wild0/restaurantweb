<?php
 /**
 * Example Application

 * @package Example-application
 */
session_start();
$position = "ad";
require_once("../include/constant/db.constant.php");
require_once(CONSTANT_PATH."advertisement.constant.php");
require_once(INCLUDE_PATH."header.php");


require('../libs/Smarty.class.php');

$smarty = new Smarty;



//$smarty->force_compile = true;
$smarty->debugging = $debug;
$smarty->caching = $cache;
$smarty->cache_lifetime = 120;

global $userHandler;
$login = $userHandler->isLogin();
$mode = getMode();

$smarty->assign("login", $login);
$smarty->assign("mode", $mode);


global $adHandler;
$adId = $_REQUEST["ad_id"];
$ad = $adHandler->get($adId);
$smarty->assign("ad_id", $ad->getVar("advertisement_id"));
$smarty->assign("ad_name", $ad->getVar("advertisement_name"));
$smarty->assign("ad_content", $ad->getVar("advertisement_content"));


$smarty->display('ad_modify.tpl');
?>
