<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<title>Boiling Point :: Add Store</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<link rel="shortcut icon" type="image/png" href="http://www.bpgroupusa.com/favicon.ico">

<link href="css/global.css" rel="stylesheet" type="text/css">
<link href="css/stylesheet.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="js/jquery-1.js"></script>
<script type="text/javascript" src="js/main-jquery.js"></script>

</head>

<body class="body-bg-01">

<!-- Begin Page -->
<div id="wrap">

	<div id="wp-header">
		<div id="header" class="clears">
			<div id="logo" class="fl"> <a href="http://www.bpgroupusa.com/"><img src="images/logo.png" alt="logo" height="117" width="117"></a> </div>
			{include file="nav.tpl"}
		</div>
	</div><!-- /#wp-header -->

	<!-- Begin content -->
	<div id="wp-content">
		<div id="content">
			<div id="content-header">
				<div class="b-menu">
					<ul class="clears">
						<li><a href="http://www.bpgroupusa.com/">Home</a></li>
						<li>
									/
								</li>
								<li>
									<a href="user_management.php">User Management</a>
								</li>
						<li>/</li>
						<li>Modify User</li>
					</ul>
				</div>
				<div class="content-title">
					<h1 class="clears">Modify User</h1>
				</div>
			</div>
			<div class="main admin-form">
				
				<form method="post" action="../include/command/user.cmd.php">
					
					<input type="hidden" name="command" value="modify_user_cmd" />
					<input type="hidden" name="return_url" value="true">
					<input type="hidden" name="uid_value" value="{$user_id}">
					
					
					<h5><label for="user_name_field">User Name:</label></h5>
					<input id="user_name_field" name="uname_value" type="text" value="{$user_name}">
					
					<h5><label for="user_password_field">Password:</label></h5>
					<input id="user_password_field" name="password_value" type="password">
					
					<h5><label for="user_device_field">Device:</label></h5>
					<input id="user_device_field" name="device_list_value" type="text" value="{$user_device_list}">
					
					
					<p class="admin-form__submit"><input class="submit-btn" value="Modify" type="submit"> or <a href="user_management.php">Cancel</a></p>
				</form>
				
			</div><!-- /.admin-form --> 
		</div>
	</div><!-- /#wp-content --> 
  
	{include file="footer.tpl"} 

</div><!-- /#wrap --> 
</body>
</html>