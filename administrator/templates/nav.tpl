<div id="nav">
						<ul>
							{if $mode eq 'root'}
							<li>
								<a class="menu-head" href="ad_management.php" id="nav-ad" ><span class="english">ads</span><span class="chinese">廣告</span></a>
							</li>
							{/if}
							{if $mode eq 'root'}
							<li>
								<a class="menu-head" href="food_menu_management.php" id="nav-food"><span class="english">food</span><span class="chinese">美食</span></a>
							</li>
							{/if}
							
							{if $mode eq 'root'}
							<li>
								<a class="menu-head" href="store_management.php" id="nav-store"><span class="english">store locations</span><span class="chinese">沸點分店</span></a>
							</li>
							{/if}
							
							<li>
								<a class="menu-head" href="preservation_management.php" id="nav-reservation"><span class="english">reservations</span><span class="chinese">訂位</span></a>
								<div style="display: none;" class="menu-body clears">
									<dl>
										{if $mode eq 'root'}
										<dd>
											<a href="preservation_status_management.php">Current Reservations</a>
										</dd>
										{/if}
										<dd>
											<a href="log_manager.php">Log Manager</a>
										</dd>
										{if $mode eq 'root'}
										<dd>
											<a href="waiting_time.php">Set Waiting Time</a>
										</dd>
										<dd>
											<a href="sms_manager.php">SMS Settings</a>
										</dd>
										
										<dd>
											<a href="group_define.php">Define Groups</a>
										</dd>
										{/if}
									</dl>
								</div>
							</li>
							
							<li>
								<a class="menu-head" href="card_management.php" id="nav-card-reader"><span class="english">card reader</span><span class="chinese">讀卡機</span></a>
							</li>
							
							<li>
								<a class="menu-head" href="user_management.php" id="nav-user"><span class="english">users</span><span class="chinese">使用者</span></a>
							</li>
							
							{if $login eq 1}
							<li>
								<a class="menu-head" href="../include/command/user.cmd.php?command=logout_cmd&platform=web&return_url=login" id="nav-logout"><span class="english">logout</span><span class="chinese">登出</span></a>
							</li>
							{/if}
						</ul>
					</div><!-- /#nav -->