<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
		<title>Boiling Point :: Manage Ads</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<script src="../js/jqgrid/jquery.min.js" type="text/javascript"></script>
		<script src="../js/jqgrid/jquery-ui-1.8.2.custom.min.js" type="text/javascript"></script>
		
		<script type="text/javascript" src="js/main-jquery.js"></script>

		<link rel="stylesheet" type="text/css" media="screen" href="../js/jqgrid/themes/redmond/jquery-ui-1.8.2.custom.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="../js/jqgrid/themes/ui.jqgrid.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="../js/jqgrid/themes/ui.multiselect.css" />

		
		<script src="../js/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
		<script type="text/javascript">
			$.jgrid.no_legacy_api = true;
			$.jgrid.useJSON = true;
		</script>
		<script src="../js/jqgrid/jquery.jqGrid.min.js" type="text/javascript"></script>
		
		<!-- Date: 2013-07-16 -->
		<link rel="shortcut icon" type="image/png" href="http://www.bpgroupusa.com/favicon.ico">

		<link href="css/global.css" rel="stylesheet" type="text/css">
		<link href="css/stylesheet.css" rel="stylesheet" type="text/css">

		
		<script>
			$(function() {
				jQuery("#ad_information_table").jqGrid({
					autowidth: true,
					height:400,
					ajaxGridOptions : {
						type : "POST"
					},
					serializeGridData : function(postdata) {
						//postdata.page = 1;
						postdata.command = "list_jqgrid_advertisement_cmd";
						return postdata;
					},
					url : '../include/command/ad.cmd.php',
					datatype : "json",
					colNames : ['ID', 'Title', 'Start Time', 'End Time', 'Active', 'modify', 'delete'],
					colModel : [{
						name : 'id',
						index : 'id',
						width : 55
					}, {
						name : 'title',
						index : 'title',
						width : 90
					}, {
						name : 'start_time',
						index : 'start_time',
						width : 100
					}, {
						name : 'end_time',
						index : 'end_time',
						width : 80,
						align : "right"
					}, {
						name : 'active',
						index : 'active',
						width : 80,
						align : "right"
					}, {
						name : 'ad_modify_action',
						index : 'action1',
						width : 40,
						sortable : false
					}, {
						name : 'ad_delete_action',
						index : 'action2',
						width : 40,
						sortable : false
					}],
					rowNum : 20,
					
					rowList : [10, 20, 30],
					pager : '#ad_information_pager',
					sortname : 'invdate',
					viewrecords : true,
					sortorder : "desc",
					caption : "Advertisement Information",
					loadComplete : function() {
						var ids = $("#ad_information_table").jqGrid('getDataIDs');
						for (var i = 0; i < ids.length; i++) {
							var cl = ids[i];
							var data = $("#food_menu_information_table").jqGrid('getRowData', cl);

							var modifyButton = "<span title='編輯' class='img_btn'  onClick='modifyAdOperation(" + cl + ")' >modify</span>";
							var deleteButton = "<span title='刪除' class='img_btn' onClick='deleteAdOperation(" + cl + ")' >delete</span>";

							$("#ad_information_table").jqGrid('setRowData', ids[i], {
								ad_modify_action : modifyButton
							});
							$("#ad_information_table").jqGrid('setRowData', ids[i], {
								ad_delete_action : deleteButton
							});

						}
					}
				});
				jQuery("#ad_information_pager").jqGrid('navGrid', '#ad_information_pager', {
					edit : false,
					add : false,
					del : false
				});
			});
			function deleteAdOperation(adId) {
				//jQuery("#store_information_table").jqGrid().trigger( 'reloadGrid' );
				var cmdUrl = "../include/command/";
				$.post(cmdUrl + "ad.cmd.php", {
					command : "remove_ad_cmd",
					ad_id : adId
				}, function(data, textStatus) {
					jQuery("#ad_information_table").jqGrid().trigger('reloadGrid');
				}, "json");

			}

			function modifyAdOperation(adId) {
				//jQuery("#store_information_table").jqGrid().trigger( 'reloadGrid' );
				var url = "ad_modify.php?";
				parent.location = url + "ad_id=" + adId;

			}
		</script>

	</head>

	<body class="body-bg-01">

		<!-- Begin Page -->
		<div id="wrap">

			<div id="wp-header">
				<div id="header" class="clears">
					<div id="logo" class="fl">
						<a href="http://www.bpgroupusa.com/"><img src="images/logo.png" alt="logo" height="117" width="117"></a>
					</div>
					{include file="nav.tpl"}
				</div>
			</div><!-- /#wp-header -->

			<!-- Begin content -->
			<div id="wp-content">
				<div id="content">
					<div id="content-header">
						<div class="b-menu">
							<ul class="clears">
								<li>
									<a href="http://www.bpgroupusa.com/">Home</a>
								</li>
								<li>
									/
								</li>
								<li>
									Ads Management
								</li>
							</ul>
						</div>
						<div class="content-title">
							<h1 class="clears">Manage Ads</h1>
						</div>
					</div>
					<div class="main">

						<div>
							<table id="ad_information_table"></table>
							<div id="ad_information_pager"></div>
						</div>

						<p class="admin-form__submit">
							<input class="submit-btn" value="new ad" type="submit"  onclick="location.assign('ad_insert.php')">
						</p>

					</div>
				</div>
			</div><!-- /#wp-content -->

			{include file="footer.tpl"}

		</div><!-- /#wrap -->

	</body>
</html>