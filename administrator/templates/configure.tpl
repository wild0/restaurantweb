<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Boiling Point :: Add Store</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />

		<link rel="shortcut icon" type="image/png" href="http://www.bpgroupusa.com/favicon.ico">

		<link href="css/global.css" rel="stylesheet" type="text/css">
		<link href="css/stylesheet.css" rel="stylesheet" type="text/css">

		<script type="text/javascript" src="js/jquery-1.js"></script>
		<script type="text/javascript" src="js/main-jquery.js"></script>

	</head>

	<body class="body-bg-01">

		<!-- Begin Page -->
		<div id="wrap">

			<div id="wp-header">
				<div id="header" class="clears">
					<div id="logo" class="fl">
						<a href="http://www.bpgroupusa.com/"><img src="images/logo.png" alt="logo" height="117" width="117"></a>
					</div>
					{include file="nav.tpl"}
				</div>
			</div><!-- /#wp-header -->

			<!-- Begin content -->
			<div id="wp-content">
				<div id="content">
					<div id="content-header">
						<div class="b-menu">
							<ul class="clears">
								<li>
									<a href="http://www.bpgroupusa.com/">Home</a>
								</li>

							</ul>
						</div>
						<div class="content-title">
							<h1 class="clears">Configure</h1>
						</div>
					</div>
					<div class="main admin-form">
						<form action="../include/command/env.php" method="post">
							<input type="hidden" name="command" value="build_env_cmd">
							<div>Database Status:{$is_database_available}</div>
							<div>
								<select name="host_role">
								<option>Select role</option>
								<option value="root" {$role_root}>root</option>
								<option value="node" {$role_node}>node</option>
								</select>
								<input type="submit" value="Establish">
							
							</div>
						</form>
						{if $role eq 'root'}
						<form method="post">
							<!--<input type="checkbox" name="database_reset" value="yes"> Reset Database-->
							
							<p class="admin-form__submit"><input class="submit-btn" value="submit" type="submit"> or <a href="#">Cancel</a></p>
						</form>
						{elseif $role eq 'node'}
						<form method="post" action="../include/command/env.php">
							<input type="hidden" name="command" value="login_root_cmd">
							<input type="hidden" name="return_url" value="index.php">
							<div>
								<h5><label for="root_ip">Root IP:</label></h5>
							
								<input type="text" id="root_ip" name="root_ip" value="{$rootIP}">
								
							</div>
							<div>
								<h5><label for="store_id">Store Id:</label></h5>

								<input type="text" id="store_id" name="node_store_id" value="{$storeId}">
								
							</div>
							<div>
								<h5><label for="store_password">Store Password:</label></h5>

								<input type="text" id="store_password" name="node_store_password" value="{$storePassword}">
								
							</div>
						
							<p class="admin-form__submit"><input class="submit-btn" value="submit" type="submit"> or <a href="#">Cancel</a></p>
						</form>
						{/if}

					</div><!-- /.admin-form -->
				</div>
			</div><!-- /#wp-content -->

			{include file="footer.tpl"}

		</div><!-- /#wrap -->
	</body>
</html>