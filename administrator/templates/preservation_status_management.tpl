<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<title>Boiling Point :: Add Store</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<link rel="shortcut icon" type="image/png" href="http://www.bpgroupusa.com/favicon.ico">

<link href="css/global.css" rel="stylesheet" type="text/css">
<link href="css/stylesheet.css" rel="stylesheet" type="text/css">


<script src="../js/jqgrid/jquery.min.js" type="text/javascript"></script> 
<script src="../js/jqgrid/jquery-ui-1.8.2.custom.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/main-jquery.js"></script>

<link rel="stylesheet" type="text/css" media="screen" href="../js/jqgrid/themes/redmond/jquery-ui-1.8.2.custom.css" /> 
    	<link rel="stylesheet" type="text/css" media="screen" href="../js/jqgrid/themes/ui.jqgrid.css" />
    	<link rel="stylesheet" type="text/css" media="screen" href="../js/jqgrid/themes/ui.multiselect.css" />
		
		
    	<script src="../js/jqgrid//i18n/grid.locale-en.js" type="text/javascript"></script> 
    	<script type="text/javascript"> 
    	$.jgrid.no_legacy_api = true; 
    	$.jgrid.useJSON = true; 
    	</script> 
    	<script src="../js/jqgrid/jquery.jqGrid.min.js" type="text/javascript"></script> 
    	
    	
		<!-- Date: 2013-07-16 -->

		<script>
		$(function() {
			jQuery("#store_information_table").jqGrid({
				autowidth: true,
				ajaxGridOptions : {
					type : "POST"
				},
				serializeGridData : function(postdata) {
					postdata.page = 1;
					postdata.command="list_jqgrid_store_status_cmd";
					return postdata;
				},
				url : '../include/command/store.cmd.php',
				datatype : "json",
				colNames : ['ID', 'Name', 'Group A', 'Group B', 'Group C','Group D'],
				colModel : [{
					name : 'id',
					index : 'id',
					width : 55
				}, {
					name : 'name',
					index : 'name',
					width : 90
				}, {
					name : 'group_a',
					index : 'group_a',
					width : 100
				}, {
					name : 'group_b',
					index : 'group_b',
					width : 100,
					align : "right"
				}, {
					name : 'group_c',
					index : 'group_c',
					width : 100,
					align : "right"
				}, {
					name : 'group_d',
					index : 'group_d',
					width : 100,
					align : "right"
				}
				],
				rowNum : 10,
				
				height: 400,
				rowList : [10, 20, 30],
				pager : '#store_information_pager',
				sortname : 'invdate',
				viewrecords : true,
				sortorder : "desc",
				caption : "Store Information"
				
			});
			jQuery("#store_information_pager").jqGrid('navGrid', '#store_information_pager', {
				edit : false,
				add : false,
				del : false
			});
		});
		
		</script>


</head>

<body class="body-bg-01">

<!-- Begin Page -->
<div id="wrap">

	<div id="wp-header">
		<div id="header" class="clears">
			<div id="logo" class="fl"> <a href="http://www.bpgroupusa.com/"><img src="images/logo.png" alt="logo" height="117" width="117"></a> </div>
			
			{include file="nav.tpl"}
		</div>
	</div><!-- /#wp-header -->

	<!-- Begin content -->
	<div id="wp-content">
		<div id="content">
			<div id="content-header">
				<div class="b-menu">
					<ul class="clears">
						<li><a href="http://www.bpgroupusa.com/">Home</a></li>
						<li>/</li>
						<li>Manage Reservations</li>
					</ul>
				</div>
				<div class="content-title">
					<h1 class="clears">Manage Reservations</h1>
				</div>
			</div>
			<div class="main">



				<div>
				
				<table id="store_information_table"></table>
				<div id="store_information_pager"></div>
				
				</div>

			</div>
		</div>
	</div><!-- /#wp-content --> 
  
	{include file="footer.tpl"} 

</div><!-- /#wrap --> 
</body>
</html>