<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
		<title>Boiling Point :: Manage Log</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<script src="../js/jqgrid/jquery.min.js" type="text/javascript"></script>
		<script src="../js/jqgrid/jquery-ui-1.8.2.custom.min.js" type="text/javascript"></script>
		
		<script type="text/javascript" src="js/main-jquery.js"></script>

		<link rel="stylesheet" type="text/css" media="screen" href="../js/jqgrid/themes/redmond/jquery-ui-1.8.2.custom.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="../js/jqgrid/themes/ui.jqgrid.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="../js/jqgrid/themes/ui.multiselect.css" />

		
		<script src="../js/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
		<script type="text/javascript">
			$.jgrid.no_legacy_api = true;
			$.jgrid.useJSON = true;
		</script>
		<script src="../js/jqgrid/jquery.jqGrid.min.js" type="text/javascript"></script>
		
		<!-- Date: 2013-07-16 -->
		<link rel="shortcut icon" type="image/png" href="http://www.bpgroupusa.com/favicon.ico">

		<link href="css/global.css" rel="stylesheet" type="text/css">
		<link href="css/stylesheet.css" rel="stylesheet" type="text/css">

		
		

	</head>

	<body class="body-bg-01">

		<!-- Begin Page -->
		<div id="wrap">

			<div id="wp-header">
				<div id="header" class="clears">
					<div id="logo" class="fl">
						<a href="http://www.bpgroupusa.com/"><img src="images/logo.png" alt="logo" height="117" width="117"></a>
					</div>
					{include file="nav.tpl"}
				</div>
			</div><!-- /#wp-header -->

			<!-- Begin content -->
			<div id="wp-content">
				<div id="content">
					<div id="content-header">
						<div class="b-menu">
							<ul class="clears">
								<li><a href="http://www.bpgroupusa.com/">Home</a></li>
								<li>/</li>
								<li><a href="card_management.php">Card Number Management</a></li>
								<li>/</li>
								<li>Add Card Number</li>
							</ul>
						</div>
						<div class="content-title">
							<h1 class="clears">Group Define</h1>
						</div>
					</div>
					<div class="main admin-form">
						<form action="../include/command/env.php" method="post">
						<input type="hidden" name="command" value="change_group_define_cmd"/>
						<div>
							<div><input type="text" name="group_a_min_value" value="{$group_a_min_value}"/><=Group A<=<input type="text"  name="group_a_max_value" value="{$group_a_max_value}"/></div>
						</div> 
						<div>
							<div><input type="text" name="group_b_min_value" value="{$group_b_min_value}"/><=Group B<=<input type="text"  name="group_b_max_value" value="{$group_b_max_value}"/></div>
						</div> 
						<div>
							<div><input type="text" name="group_c_min_value" value="{$group_c_min_value}"/><=Group C<=<input type="text" name="group_c_max_value" value="{$group_c_max_value}"/></div>
						</div> 
						<div>
							<div><input type="text" name="group_d_min_value" value="{$group_d_min_value}"/><=Group D<=<input type="text" name="group_d_max_value" value="{$group_d_max_value}"/></div>
						</div> 
						<input class="submit-btn" value="submit" type="submit">
						</form>
					</div>
					<div id="wp-content">
				</div>
			</div><!-- /#wp-content -->

			{include file="footer.tpl"}

		</div><!-- /#wrap -->

	</body>
</html>