<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Boiling Point :: Manage Stores</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />

		<link rel="shortcut icon" type="image/png" href="http://www.bpgroupusa.com/favicon.ico">

		<link href="css/global.css" rel="stylesheet" type="text/css">
		<link href="css/stylesheet.css" rel="stylesheet" type="text/css">

		<script src="../js/jqgrid/jquery.min.js" type="text/javascript"></script>
		<script src="../js/jqgrid/jquery-ui-1.8.2.custom.min.js" type="text/javascript"></script>

		<script type="text/javascript" src="js/main-jquery.js"></script>

		<link rel="stylesheet" type="text/css" media="screen" href="../js/jqgrid/themes/redmond/jquery-ui-1.8.2.custom.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="../js/jqgrid/themes/ui.jqgrid.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="../js/jqgrid/themes/ui.multiselect.css" />

		<script src="../js/restaurant.js" type="text/javascript"></script>

		<script src="../js/jqgrid//i18n/grid.locale-en.js" type="text/javascript"></script>
		<script type="text/javascript">
			$.jgrid.no_legacy_api = true;
			$.jgrid.useJSON = true;
		</script>
		<script src="../js/jqgrid/jquery.jqGrid.min.js" type="text/javascript"></script>

		<!-- Date: 2013-07-16 -->

		<script>
			$(function() {
				jQuery("#store_information_table").jqGrid({
					autowidth: true,
					height:400,
					ajaxGridOptions : {
						type : "POST"
					},
					serializeGridData : function(postdata) {
						//postdata.page = 1;
						postdata.command = "list_jqgrid_store_cmd";
						return postdata;
					},
					url : '../include/command/store.cmd.php',
					datatype : "json",
					colNames : ['ID', 'Name', 'Addess', 'Tel', 'Description', 'Lati', 'Longi', "Edit", "Delete"],
					colModel : [{
						name : 'id',
						index : 'id',
						width : 55
					}, {
						name : 'name',
						index : 'name',
						width : 90
					}, {
						name : 'address',
						index : 'address',
						width : 100
					}, {
						name : 'tel',
						index : 'tel',
						width : 80,
						align : "right"
					}, {
						name : 'description',
						index : 'description',
						width : 80,
						align : "right"
					}, {
						name : 'lati',
						index : 'lati',
						width : 80,
						align : "right"
					}, {
						name : 'longi',
						index : 'longi',
						width : 150,
						sortable : false
					}, {
						name : 'store_modify_action',
						index : 'action1',
						width : 40,
						sortable : false
					}, {
						name : 'store_delete_action',
						index : 'action2',
						width : 40,
						sortable : false
					}],
					rowNum : 20,
					rowList : [10, 20, 30],
					pager : '#store_information_pager',
					sortname : 'invdate',
					viewrecords : true,
					sortorder : "desc",
					caption : "Store Information",
					loadComplete : function() {
						var ids = $("#store_information_table").jqGrid('getDataIDs');
						for (var i = 0; i < ids.length; i++) {
							var cl = ids[i];
							var data = $("#store_information_table").jqGrid('getRowData', cl);

							var modifyButton = "<span title='Modify' class='img_btn'  onClick='modifyStoreOperation(" + cl + ")' >Modify</span>";
							var deleteButton = "<span title='Delete' class='img_btn' onClick='deleteStoreOperation(" + cl + ")' >Delete</span>";

							$("#store_information_table").jqGrid('setRowData', ids[i], {
								store_modify_action : modifyButton
							});
							$("#store_information_table").jqGrid('setRowData', ids[i], {
								store_delete_action : deleteButton
							});

						}
					}
				});
				jQuery("#store_information_pager").jqGrid('navGrid', '#store_information_pager', {
					edit : false,
					add : false,
					del : false
				});
			});
			function deleteStoreOperation(storeId) {
				//jQuery("#store_information_table").jqGrid().trigger( 'reloadGrid' );
				if (confirm("Do you want to delete?")) {
					var cmdUrl = "../include/command/";
					$.post(cmdUrl + "store.cmd.php", {
						command : "remove_store_cmd",
						store_id : storeId
					}, function(data, textStatus) {
						jQuery("#store_information_table").jqGrid().trigger('reloadGrid');
					}, "json");
				}
			}

			function modifyStoreOperation(storeId) {
				//jQuery("#store_information_table").jqGrid().trigger( 'reloadGrid' );
				/*
				 $.post(cmdUrl+"store.cmd.php", { command:"remove_store_cmd",store_id: storeId},
				 function (data, textStatus){
				 jQuery("#store_information_table").jqGrid().trigger( 'reloadGrid' );
				 }, "json");
				 */
				var url = "store_modify.php?";
				parent.location = url + "store_id=" + storeId;

			}
		</script>
	</head>

	<body class="body-bg-01">

		<!-- Begin Page -->
		<div id="wrap">

			<div id="wp-header">
				<div id="header" class="clears">
					<div id="logo" class="fl">
						<a href="http://www.bpgroupusa.com/"><img src="images/logo.png" alt="logo" height="117" width="117"></a>
					</div>
					{include file="nav.tpl"}
				</div>
			</div><!-- /#wp-header -->

			<!-- Begin content -->
			<div id="wp-content">
				<div id="content">
					<div id="content-header">
						<div class="b-menu">
							<ul class="clears">
								<li>
									<a href="http://www.bpgroupusa.com/">Home</a>
								</li>
								<li>
									/
								</li>
								<li>
									Store Management
								</li>
							</ul>
						</div>
						<div class="content-title">
							<h1 class="clears">Manage Stores</h1>
						</div>
					</div>
					<div class="main">

						<div>

							<table id="store_information_table"></table>
							<div id="store_information_pager"></div>

						</div>

						<p class="admin-form__submit">
							<input class="submit-btn" value="Add store" type="submit"  onclick="location.assign('store_insert.php')">
						</p>

					</div>
				</div>
			</div><!-- /#wp-content -->

			{include file="footer.tpl"}

		</div><!-- /#wrap -->
	</body>
</html>