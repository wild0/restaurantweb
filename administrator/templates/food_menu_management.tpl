<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Boiling Point :: Manage Menu</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />

		<link rel="shortcut icon" type="image/png" href="http://www.bpgroupusa.com/favicon.ico">

		<link href="css/global.css" rel="stylesheet" type="text/css">
		<link href="css/stylesheet.css" rel="stylesheet" type="text/css">

		<script src="../js/jqgrid/jquery.min.js" type="text/javascript"></script>
		<script src="../js/jqgrid/jquery-ui-1.8.2.custom.min.js" type="text/javascript"></script>

		<script type="text/javascript" src="js/main-jquery.js"></script>
		<link rel="stylesheet" type="text/css" media="screen" href="../js/jqgrid/themes/redmond/jquery-ui-1.8.2.custom.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="../js/jqgrid/themes/ui.jqgrid.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="../js/jqgrid/themes/ui.multiselect.css" />

		<script src="../js/restaurant.js" type="text/javascript"></script>

		<script src="../js/jqgrid//i18n/grid.locale-en.js" type="text/javascript"></script>
		<script type="text/javascript">
			$.jgrid.no_legacy_api = true;
			$.jgrid.useJSON = true;
		</script>
		<script src="../js/jqgrid/jquery.jqGrid.min.js" type="text/javascript"></script>

		<!-- Date: 2013-07-16 -->

		<script>
			$(function() {
				jQuery("#food_menu_information_table").jqGrid({
					autowidth: true,
					height:400,
					ajaxGridOptions : {
						type : "POST"
					},
					serializeGridData : function(postdata) {
						//postdata.page = 1;
						postdata.command = "list_jqgrid_food_menu_cmd";
						return postdata;
					},
					url : '../include/command/food.menu.cmd.php',
					datatype : "json",
					colNames : ['ID', 'Name', 'Price', 'Description', '編輯', '刪除'],
					colModel : [{
						name : 'id',
						index : 'id',
						width : 55
					}, {
						name : 'name',
						index : 'name',
						width : 90
					}, {
						name : 'price',
						index : 'price',
						width : 100
					}, {
						name : 'description',
						index : 'description',
						width : 80,
						align : "right"
					}, {
						name : 'food_menu_modify_action',
						index : 'action1',
						width : 40,
						sortable : false
					}, {
						name : 'food_menu_delete_action',
						index : 'action2',
						width : 40,
						sortable : false
					}],
					rowNum : 20,
					
					rowList : [10, 20, 30],
					pager : '#food_menu_information_pager',
					sortname : 'invdate',
					viewrecords : true,
					sortorder : "desc",
					caption : "Food Menu Information",
					loadComplete : function() {
						var ids = $("#food_menu_information_table").jqGrid('getDataIDs');
						for (var i = 0; i < ids.length; i++) {
							var cl = ids[i];
							var data = $("#food_menu_information_table").jqGrid('getRowData', cl);

							var modifyButton = "<span title='編輯' class='img_btn'  onClick='modifyFoodMenuOperation(" + cl + ")' >編輯</span>";
							var deleteButton = "<span title='刪除' class='img_btn' onClick='deleteFoodMenuOperation(" + cl + ")' >刪除</span>";

							$("#food_menu_information_table").jqGrid("setRowData", ids[i], {
								food_menu_modify_action : modifyButton
							});
							$("#food_menu_information_table").jqGrid("setRowData", ids[i], {
								food_menu_delete_action : deleteButton
							});

						}
					}
				});
				jQuery("#food_menu_information_pager").jqGrid('navGrid', '#food_menu_information_pager', {
					edit : false,
					add : false,
					del : false
				});
			});
			function deleteFoodMenuOperation(foodMenuId) {
				//jQuery("#store_information_table").jqGrid().trigger( 'reloadGrid' );
				if (confirm("Do you want to delete?")) {
					var cmdUrl = "../include/command/";
					$.post(cmdUrl + "food.menu.cmd.php", {
						command : "remove_food_menu_cmd",
						food_menu_id : foodMenuId
					}, function(data, textStatus) {
						jQuery("#food_menu_information_table").jqGrid().trigger('reloadGrid');
					}, "json");
				}
			}

			function modifyFoodMenuOperation(foodMenuId) {
				//jQuery("#store_information_table").jqGrid().trigger( 'reloadGrid' );
				var url = "food_menu_modify.php?";
				parent.location = url + "food_menu_id=" + foodMenuId;

			}
		</script>
	</head>

	<body class="body-bg-01">

		<!-- Begin Page -->
		<div id="wrap">

			<div id="wp-header">
				<div id="header" class="clears">
					<div id="logo" class="fl">
						<a href="http://www.bpgroupusa.com/"><img src="images/logo.png" alt="logo" height="117" width="117"></a>
					</div>
					{include file="nav.tpl"}
				</div>
			</div><!-- /#wp-header -->

			<!-- Begin content -->
			<div id="wp-content">
				<div id="content">
					<div id="content-header">
						<div class="b-menu">
							<ul class="clears">
								<li>
									<a href="http://www.bpgroupusa.com/">Home</a>
								</li>
								<li>
									/
								</li>
								<li>
									Food Menu Item Management
								</li>
							</ul>
						</div>
						<div class="content-title">
							<h1 class="clears">Manage Menu</h1>
						</div>
					</div>
					<div class="main">

						<div>

							<table id="food_menu_information_table"></table>
							<div id="food_menu_information_pager"></div>

						</div>

						<p class="admin-form__submit">
							<input class="submit-btn" value="Add menu item" type="submit"  onclick="location.assign('food_menu_insert.php')">
						</p>

					</div>
				</div>
			</div><!-- /#wp-content -->

			{include file="footer.tpl"}

		</div><!-- /#wrap -->
	</body>
</html>