<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Boiling Point :: Add Store</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />

		<link rel="shortcut icon" type="image/png" href="http://www.bpgroupusa.com/favicon.ico">

		<link href="css/global.css" rel="stylesheet" type="text/css">
		<link href="css/stylesheet.css" rel="stylesheet" type="text/css">

		<script type="text/javascript" src="js/jquery-1.js"></script>
		<script type="text/javascript" src="js/main-jquery.js"></script>

	</head>

	<body class="body-bg-01">

		<!-- Begin Page -->
		<div id="wrap">

			<div id="wp-header">
				<div id="header" class="clears">
					<div id="logo" class="fl">
						<a href="http://www.bpgroupusa.com/"><img src="images/logo.png" alt="logo" height="117" width="117"></a>
					</div>
					{include file="nav.tpl"}
				</div>
			</div><!-- /#wp-header -->

			<!-- Begin content -->
			<div id="wp-content">
				<div id="content">
					<div id="content-header">
						<div class="b-menu">
							<ul class="clears">
								<li>
									<a href="http://www.bpgroupusa.com/">Home</a>
								</li>

							</ul>
						</div>
						<div class="content-title">
							<h1 class="clears">SMS Manager</h1>
						</div>
					</div>
					<div class="main admin-form">
						{if $role eq 'root'}
						<form method="post">
							
							<div>
								<h5><label for="sms_sender_number">Sender Number:</label></h5>
								<input type="text" id="sms_sender_number" name="sms_sender_number" value="{$sms_sender_number}">
								
								
							</div>
							
							<div>
								<h5><label for="sms_account_sid">ACCOUND SID:</label></h5>
								<input type="text" id="sms_account_sid" name="sms_account_sid" value="{$sms_account_sid}">
								
							</div>
							
							<div>
								<h5><label for="sms_auth_token">AUTH TOKEN:</label></h5>
								<input type="text" id="sms_auth_token" name="sms_auth_token" value="{$sms_auth_token}">
								
							</div>
						
						</form>
						{elseif $role eq 'node'}
						<form method="post">
							
						
							
						</form>
						{/if}

					</div><!-- /.admin-form -->
				</div>
			</div><!-- /#wp-content -->

			{include file="footer.tpl"}

		</div><!-- /#wrap -->
	</body>
</html>