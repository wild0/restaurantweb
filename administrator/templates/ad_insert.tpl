<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Boiling Point :: New Ad</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />

		<link rel="shortcut icon" type="image/png" href="http://www.bpgroupusa.com/favicon.ico">

		<link href="css/global.css" rel="stylesheet" type="text/css">
		<link href="css/stylesheet.css" rel="stylesheet" type="text/css">

		<script type="text/javascript" src="js/jquery-1.js"></script>
		<script type="text/javascript" src="js/main-jquery.js"></script>

		<!--wild0-->
		<script src="../js/ckeditor/ckeditor.js" type="text/javascript"></script>

		<!--
			<link rel="stylesheet" href="../js/ckeditor/samples/sample.css">
		-->
	</head>

	<body class="body-bg-01">

		<!-- Begin Page -->
		<div id="wrap">

			<div id="wp-header">
				<div id="header" class="clears">
					<div id="logo" class="fl">
						<a href="http://www.bpgroupusa.com/"><img src="images/logo.png" alt="logo" height="117" width="117"></a>
					</div>
					{include file="nav.tpl"}
				</div>
			</div><!-- /#wp-header -->

			<!-- Begin content -->
			<div id="wp-content">
				<div id="content">
					<div id="content-header">
						<div class="b-menu">
							<ul class="clears">
								<li>
									<a href="http://www.bpgroupusa.com/">Home</a>
								</li>
								<li>/</li>
						<li><a href="ad_management.php">Ads Management</a></li>
								<li>
									/
								</li>
								<li>
									New Ad
								</li>
							</ul>
						</div>
						<div class="content-title">
							<h1 class="clears">Create a new ad</h1>
						</div>
					</div>
					<div class="main admin-form">
						

								<form action="../include/command/ad.cmd.php" method="post">
									<input type="hidden" name="command" value="add_advertisement_cmd">
									<input type="hidden" name="return_url" value="true">

									<h5><label for="advertisement_name_value_id">AD Name:</label></h5>
									<input type="text" id="advertisement_name_value_id" name="advertisement_name_value">

									<h5><label for="editor1">AD Content:</label></h5>
									<textarea cols="100" id="editor1" name="advertisement_content_value" rows="10"></textarea>																		



									<script>
										CKEDITOR.on('instanceReady', function(ev) {
											// Show the editor name and description in the browser status bar.
											document.getElementById('eMessage').innerHTML = 'Instance <code>' + ev.editor.name + '<\/code> loaded.';

											// Show this sample buttons.
											document.getElementById('eButtons').style.display = 'block';
										});

										function Focus() {
											CKEDITOR.instances.editor1.focus();
										}

										function onFocus() {
											document.getElementById('eMessage').innerHTML = '<b>' + this.name + ' is focused </b>';
										}

										function onBlur() {
											document.getElementById('eMessage').innerHTML = this.name + ' lost focus';
										}

										//<![CDATA[

										// Replace the <textarea id="editor"> with an CKEditor
										// instance, using the "bbcode" plugin, shaping some of the
										// editor configuration to fit BBCode environment.

										// Replace the <textarea id="editor1"> with an CKEditor instance.
										CKEDITOR.replace('editor1', {
											on : {
												focus : onFocus,
												blur : onBlur,

												// Check for availability of corresponding plugins.
												pluginsLoaded : function(evt) {
													var doc = CKEDITOR.document, ed = evt.editor;
													if (!ed.getCommand('bold'))
														doc.getById('exec-bold').hide();
													if (!ed.getCommand('link'))
														doc.getById('exec-link').hide();
												}
											}
										});

									</script>
									<input type="checkbox" name="advertisement_active_value" value="1">
									Active</input>
									<p class="admin-form__submit">
										<input class="submit-btn" value="submit" type="submit">
										or <a href="ad_management.php">Cancel</a>
									</p>
								</form>

							
					</div><!-- /.admin-form -->
				</div>
			</div><!-- /#wp-content -->

			{include file="footer.tpl"}

		</div><!-- /#wrap -->

	</body>
</html>