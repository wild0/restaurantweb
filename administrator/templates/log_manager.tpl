<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
		<title>Boiling Point :: Manage Log</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<script src="../js/jqgrid/jquery.min.js" type="text/javascript"></script>
		<script src="../js/jqgrid/jquery-ui-1.8.2.custom.min.js" type="text/javascript"></script>
		
		<script type="text/javascript" src="js/main-jquery.js"></script>

		<link rel="stylesheet" type="text/css" media="screen" href="../js/jqgrid/themes/redmond/jquery-ui-1.8.2.custom.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="../js/jqgrid/themes/ui.jqgrid.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="../js/jqgrid/themes/ui.multiselect.css" />

		
		<script src="../js/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
		<script type="text/javascript">
			$.jgrid.no_legacy_api = true;
			$.jgrid.useJSON = true;
		</script>
		<script src="../js/jqgrid/jquery.jqGrid.min.js" type="text/javascript"></script>
		
		<!-- Date: 2013-07-16 -->
		<link rel="shortcut icon" type="image/png" href="http://www.bpgroupusa.com/favicon.ico">

		<link href="css/global.css" rel="stylesheet" type="text/css">
		<link href="css/stylesheet.css" rel="stylesheet" type="text/css">

		
		<script>
			$(function() {
				jQuery("#log_information_table").jqGrid({
					autowidth: true,
					height:400,
					ajaxGridOptions : {
						type : "POST"
					},
					serializeGridData : function(postdata) {
						//postdata.page = 1;
						postdata.command = "list_jqgrid_log_cmd";
						return postdata;
					},
					url : '../include/command/log.cmd.php',
					datatype : "json",
					colNames : ['ID', 'Log type', 'Request host', 'StoreId', 'Operation', 'Status Code', 'Description','Time'],
					colModel : [{
						name : 'id',
						index : 'id',
						width : 55
					}, {
						name : 'type',
						index : 'type',
						width : 90
					}, {
						name : 'host',
						index : 'host',
						width : 100
					}, {
						name : 'store',
						index : 'store',
						width : 80,
						align : "right"
					}, {
						name : 'operation',
						index : 'operation',
						width : 80,
						align : "right"
					}, {
						name : 'status code',
						index : 'status_code',
						width : 40,
						sortable : false
					}, {
						name : 'description',
						index : 'description',
						width : 40,
						sortable : false
					},
					{
						name : 'time',
						index : 'time',
						width : 40,
						sortable : false
					}],
					rowNum : 20,
					width : 700,
					rowList : [10, 20, 30],
					pager : '#log_information_pager',
					sortname : 'invdate',
					viewrecords : true,
					sortorder : "desc",
					caption : "Log"
				});
				jQuery("#log_information_pager").jqGrid('navGrid', '#ad_information_pager', {
					edit : false,
					add : false,
					del : false
				});
			});
			
		</script>

	</head>

	<body class="body-bg-01">

		<!-- Begin Page -->
		<div id="wrap">

			<div id="wp-header">
				<div id="header" class="clears">
					<div id="logo" class="fl">
						<a href="http://www.bpgroupusa.com/"><img src="images/logo.png" alt="logo" height="117" width="117"></a>
					</div>
					{include file="nav.tpl"}
				</div>
			</div><!-- /#wp-header -->

			<!-- Begin content -->
			<div id="wp-content">
				<div id="content">
					<div id="content-header">
						<div class="b-menu">
							<ul class="clears">
								<li>
									<a href="http://www.bpgroupusa.com/">Home</a>
								</li>
								<li>
									/
								</li>
								<li>
									Log Management
								</li>
							</ul>
						</div>
						<div class="content-title">
							<h1 class="clears">Log Viewer</h1>
						</div>
					</div>
					<div class="main">

						<div>
							<table id="log_information_table"></table>
							<div id="log_information_pager"></div>
						</div>

						

					</div>
				</div>
			</div><!-- /#wp-content -->

			{include file="footer.tpl"}

		</div><!-- /#wrap -->

	</body>
</html>