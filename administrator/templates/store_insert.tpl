<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<title>Boiling Point :: Add Store</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<link rel="shortcut icon" type="image/png" href="http://www.bpgroupusa.com/favicon.ico">

<link href="css/global.css" rel="stylesheet" type="text/css">
<link href="css/stylesheet.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="js/jquery-1.js"></script>
<script type="text/javascript" src="js/main-jquery.js"></script>

</head>

<body class="body-bg-01">

<!-- Begin Page -->
<div id="wrap">

	<div id="wp-header">
		<div id="header" class="clears">
			<div id="logo" class="fl"> <a href="http://www.bpgroupusa.com/"><img src="images/logo.png" alt="logo" height="117" width="117"></a> </div>
			{include file="nav.tpl"}
		</div>
	</div><!-- /#wp-header -->

	<!-- Begin content -->
	<div id="wp-content">
		<div id="content">
			<div id="content-header">
				<div class="b-menu">
					<ul class="clears">
						<li><a href="http://www.bpgroupusa.com/">Home</a></li>
						<li>/</li>
						<li><a href="store_management.php">Store Management</a></li>
						<li>/</li>
						<li>Add Store</li>
					</ul>
				</div>
				<div class="content-title">
					<h1 class="clears">Add a new store</h1>
				</div>
			</div>
			<div class="main admin-form">
				
				<form method="post" action="../include/command/store.cmd.php">
					
					<input name="command" value="add_store_cmd" type="hidden">
					<input name="return_url" value="true" type="hidden">
					
					<h5><label for="store_name_field">Name:</label></h5>
					<input id="store_name_field" name="store_name_value" type="text">
					
					<h5><label for="store_address_field">Address:</label></h5>
					<input id="store_address_field" name="store_address_value" type="text">
					
					<h5><label for="store_tel_field">Telephone:</label></h5>
					<input id="store_tel_field" name="store_tel_value" type="text">
					
					<h5><label for="store_password_field">Password:</label></h5>
					<input id="store_password_field" name="store_password_value" type="text">
					
					<h5><label for="store_business_time_field">Business Time:</label></h5>
					<input id="store_business_time_field" name="store_business_time_value" type="text">
					
					<h5><label for="store_lati_field">Latitude:</label></h5>
					<input id="store_lati_field" name="store_lati_value" type="text">
					
					<h5><label for="store_longi_field">Longitude:</label></h5>
					<input id="store_longi_field" name="store_longi_value" type="text">
					
					<h5><label for="store_description_field">Description:</label></h5>
					<input id="store_description_field" name="store_description_value" type="text">
					
					<p class="admin-form__submit"><input class="submit-btn" value="submit" type="submit"> or <a href="store_management.php">Cancel</a></p>

				</form>
				
			</div><!-- /.admin-form --> 
		</div>
	</div><!-- /#wp-content --> 
  
	{include file="footer.tpl"} 

</div><!-- /#wrap --> 
</body>
</html>