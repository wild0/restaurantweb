<?php /* Smarty version Smarty-3.1.13, created on 2014-02-24 23:50:00
         compiled from ".\templates\user_management.tpl" */ ?>
<?php /*%%SmartyHeaderCode:31429530b6a2811a8a6-82182022%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '913931e80377af7dad0dc121289b90c61990195f' => 
    array (
      0 => '.\\templates\\user_management.tpl',
      1 => 1391785082,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '31429530b6a2811a8a6-82182022',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_530b6a2818c863_47670306',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_530b6a2818c863_47670306')) {function content_530b6a2818c863_47670306($_smarty_tpl) {?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Boiling Point :: Manage Stores</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />

		<link rel="shortcut icon" type="image/png" href="http://www.bpgroupusa.com/favicon.ico">

		<link href="css/global.css" rel="stylesheet" type="text/css">
		<link href="css/stylesheet.css" rel="stylesheet" type="text/css">

		<script src="../js/jqgrid/jquery.min.js" type="text/javascript"></script>
		<script src="../js/jqgrid/jquery-ui-1.8.2.custom.min.js" type="text/javascript"></script>

		<script type="text/javascript" src="js/main-jquery.js"></script>

		<link rel="stylesheet" type="text/css" media="screen" href="../js/jqgrid/themes/redmond/jquery-ui-1.8.2.custom.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="../js/jqgrid/themes/ui.jqgrid.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="../js/jqgrid/themes/ui.multiselect.css" />

		<script src="../js/restaurant.js" type="text/javascript"></script>

		<script src="../js/jqgrid//i18n/grid.locale-en.js" type="text/javascript"></script>
		<script type="text/javascript">
			$.jgrid.no_legacy_api = true;
			$.jgrid.useJSON = true;
		</script>
		<script src="../js/jqgrid/jquery.jqGrid.min.js" type="text/javascript"></script>

		<!-- Date: 2013-07-16 -->

		<script>
			$(function() {
				jQuery("#user_information_table").jqGrid({
					autowidth: true,
					height:400,
					ajaxGridOptions : {
						type : "POST"
					},
					serializeGridData : function(postdata) {
						//postdata.page = 1;
						postdata.command = "list_jqgrid_users_cmd";
						return postdata;
					},
					url : '../include/command/user.cmd.php',
					datatype : "json",
					colNames : ['ID', 'Uname', 'Available Devices', 'Modify', 'Delete'],
					colModel : [{
						name : 'id',
						index : 'id',
						width : 55
					}, {
						name : 'uname',
						index : 'uname',
						width : 90
					}, {
						name : 'device_list',
						index : 'device_list',
						width : 80,
						align : "right"
					}, {
						name : 'user_modify_action',
						index : 'action1',
						width : 40,
						sortable : false
					}, {
						name : 'user_delete_action',
						index : 'action2',
						width : 40,
						sortable : false
					}],
					rowNum : 20,
					width : 700,
					rowList : [10, 20, 30],
					pager : '#user_information_pager',
					sortname : 'invdate',
					viewrecords : true,
					sortorder : "desc",
					caption : "User Information",
					loadComplete : function() {
						var ids = $("#user_information_table").jqGrid('getDataIDs');
						for (var i = 0; i < ids.length; i++) {
							var cl = ids[i];
							var data = $("#user_information_table").jqGrid('getRowData', cl);

							var modifyButton = "<span title='編輯' class='img_btn'  onClick='modifyUserOperation(" + cl + ")' >modify</span>";
							var deleteButton = "<span title='Delete' class='img_btn' onClick='deleteUserOperation(" + cl + ")' >Delete</span>";

							$("#user_information_table").jqGrid('setRowData', ids[i], {
								user_modify_action : modifyButton
							});
							$("#user_information_table").jqGrid('setRowData', ids[i], {
								user_delete_action : deleteButton
							});

						}
					}
				});
				jQuery("#user_information_pager").jqGrid('navGrid', '#user_information_pager', {
					edit : false,
					add : false,
					del : false
				});
			});

			function deleteUserOperation(userId) {
				//jQuery("#store_information_table").jqGrid().trigger( 'reloadGrid' );
				if (confirm("Do you want to delete?")) {
					var cmdUrl = "../include/command/";
					$.post(cmdUrl + "user.cmd.php", {
						command : "remove_user_cmd",
						user_id : userId
					}, function(data, textStatus) {
						jQuery("#user_information_table").jqGrid().trigger('reloadGrid');
					}, "json");
				}
			}

			function modifyUserOperation(userId) {

				var url = "user_modify.php?";
				parent.location = url + "user_id=" + userId;

			}
		</script>
	</head>

	<body class="body-bg-01">

		<!-- Begin Page -->
		<div id="wrap">

			<div id="wp-header">
				<div id="header" class="clears">
					<div id="logo" class="fl">
						<a href="http://www.bpgroupusa.com/"><img src="images/logo.png" alt="logo" height="117" width="117"></a>
					</div>
					<?php echo $_smarty_tpl->getSubTemplate ("nav.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

				</div>
			</div><!-- /#wp-header -->

			<!-- Begin content -->
			<div id="wp-content">
				<div id="content">
					<div id="content-header">
						<div class="b-menu">
							<ul class="clears">
								<li>
									<a href="http://www.bpgroupusa.com/">Home</a>
								</li>
								<li>
									/
								</li>
								<li>
									User Management
								</li>
							</ul>
						</div>
						<div class="content-title">
							<h1 class="clears">Manage Users</h1>
						</div>
					</div>
					<div class="main">

						<div>

							<table id="user_information_table"></table>
							<div id="user_information_pager"></div>

						</div>

						<p class="admin-form__submit">
							<input class="submit-btn" value="Add user" type="submit"  onclick="location.assign('user_insert.php')">

						</p>

					</div>
				</div>
			</div><!-- /#wp-content -->

			<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


		</div><!-- /#wrap -->
	</body>
</html><?php }} ?>