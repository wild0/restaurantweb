<?php /* Smarty version Smarty-3.1.13, created on 2014-04-03 01:06:13
         compiled from ".\templates\configure.tpl" */ ?>
<?php /*%%SmartyHeaderCode:16622530368bdcba095-83733063%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '54e9f282703ee5fec5847bddc459088235854469' => 
    array (
      0 => '.\\templates\\configure.tpl',
      1 => 1387716996,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '16622530368bdcba095-83733063',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_530368bdd3d8c2_45165652',
  'variables' => 
  array (
    'is_database_available' => 0,
    'role_root' => 0,
    'role_node' => 0,
    'role' => 0,
    'rootIP' => 0,
    'storeId' => 0,
    'storePassword' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_530368bdd3d8c2_45165652')) {function content_530368bdd3d8c2_45165652($_smarty_tpl) {?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Boiling Point :: Add Store</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />

		<link rel="shortcut icon" type="image/png" href="http://www.bpgroupusa.com/favicon.ico">

		<link href="css/global.css" rel="stylesheet" type="text/css">
		<link href="css/stylesheet.css" rel="stylesheet" type="text/css">

		<script type="text/javascript" src="js/jquery-1.js"></script>
		<script type="text/javascript" src="js/main-jquery.js"></script>

	</head>

	<body class="body-bg-01">

		<!-- Begin Page -->
		<div id="wrap">

			<div id="wp-header">
				<div id="header" class="clears">
					<div id="logo" class="fl">
						<a href="http://www.bpgroupusa.com/"><img src="images/logo.png" alt="logo" height="117" width="117"></a>
					</div>
					<?php echo $_smarty_tpl->getSubTemplate ("nav.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

				</div>
			</div><!-- /#wp-header -->

			<!-- Begin content -->
			<div id="wp-content">
				<div id="content">
					<div id="content-header">
						<div class="b-menu">
							<ul class="clears">
								<li>
									<a href="http://www.bpgroupusa.com/">Home</a>
								</li>

							</ul>
						</div>
						<div class="content-title">
							<h1 class="clears">Configure</h1>
						</div>
					</div>
					<div class="main admin-form">
						<form action="../include/command/env.php" method="post">
							<input type="hidden" name="command" value="build_env_cmd">
							<div>Database Status:<?php echo $_smarty_tpl->tpl_vars['is_database_available']->value;?>
</div>
							<div>
								<select name="host_role">
								<option>Select role</option>
								<option value="root" <?php echo $_smarty_tpl->tpl_vars['role_root']->value;?>
>root</option>
								<option value="node" <?php echo $_smarty_tpl->tpl_vars['role_node']->value;?>
>node</option>
								</select>
								<input type="submit" value="Establish">
							
							</div>
						</form>
						<?php if ($_smarty_tpl->tpl_vars['role']->value=='root'){?>
						<form method="post">
							<!--<input type="checkbox" name="database_reset" value="yes"> Reset Database-->
							
							<p class="admin-form__submit"><input class="submit-btn" value="submit" type="submit"> or <a href="#">Cancel</a></p>
						</form>
						<?php }elseif($_smarty_tpl->tpl_vars['role']->value=='node'){?>
						<form method="post" action="../include/command/env.php">
							<input type="hidden" name="command" value="login_root_cmd">
							<input type="hidden" name="return_url" value="index.php">
							<div>
								<h5><label for="root_ip">Root IP:</label></h5>
							
								<input type="text" id="root_ip" name="root_ip" value="<?php echo $_smarty_tpl->tpl_vars['rootIP']->value;?>
">
								
							</div>
							<div>
								<h5><label for="store_id">Store Id:</label></h5>

								<input type="text" id="store_id" name="node_store_id" value="<?php echo $_smarty_tpl->tpl_vars['storeId']->value;?>
">
								
							</div>
							<div>
								<h5><label for="store_password">Store Password:</label></h5>

								<input type="text" id="store_password" name="node_store_password" value="<?php echo $_smarty_tpl->tpl_vars['storePassword']->value;?>
">
								
							</div>
						
							<p class="admin-form__submit"><input class="submit-btn" value="submit" type="submit"> or <a href="#">Cancel</a></p>
						</form>
						<?php }?>

					</div><!-- /.admin-form -->
				</div>
			</div><!-- /#wp-content -->

			<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


		</div><!-- /#wrap -->
	</body>
</html><?php }} ?>