<?php /* Smarty version Smarty-3.1.13, created on 2014-02-18 22:07:06
         compiled from ".\templates\index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:208335303690a0e2330-53177021%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '749422d4cfc3eb5677cf499730392b6accd4d1c7' => 
    array (
      0 => '.\\templates\\index.tpl',
      1 => 1381045664,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '208335303690a0e2330-53177021',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5303690a1335c2_62108993',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5303690a1335c2_62108993')) {function content_5303690a1335c2_62108993($_smarty_tpl) {?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<title>Boiling Point :: Add Menu Item</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<link rel="shortcut icon" type="image/png" href="http://www.bpgroupusa.com/favicon.ico">

<link href="css/global.css" rel="stylesheet" type="text/css">
<link href="css/stylesheet.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="js/jquery-1.js"></script>
<script type="text/javascript" src="js/main-jquery.js"></script>

</head>

<body class="body-bg-01">

<!-- Begin Page -->
<div id="wrap">

	<div id="wp-header">
		<div id="header" class="clears">
			<div id="logo" class="fl"> <a href="http://www.bpgroupusa.com/"><img src="images/logo.png" alt="logo" height="117" width="117"></a> </div>
			<?php echo $_smarty_tpl->getSubTemplate ("nav.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

		</div>
	</div><!-- /#wp-header -->

	<!-- Begin content -->
	<div id="wp-content">
		<div id="content">
			<div id="content-header">
				<div class="b-menu">
					<ul class="clears">
						<li><a href="http://www.bpgroupusa.com/">Home</a></li>
						<li>/</li>
						<li>Add Menu Item</li>
					</ul>
				</div>
				<div class="content-title">
					<h1 class="clears">LOGIN</h1>
				</div>
			</div>
			<div class="main admin-form">
				
				<form method="post" action="../include/command/user.cmd.php">
					<input type="hidden" name="command" value="login_cmd"/>
					<input type="hidden" name="return_url" value="<<?php ?>?php echo $returnUrl?<?php ?>>"/>
					<input type="hidden" name="platform" value="web"/>
					
					<h5><label for="user_uname_field">User Name</label></h5>
					<input type="text" id="user_uname_field" name="uname" />
					
					<h5><label for="user_password_field">Password</label></h5>
					<input type="password" id="user_password_field" name="password" />
						
					<p class="admin-form__submit"><input class="submit-btn" value="login" type="submit"> or <a href="#">Cancel</a></p>
					
				</form>
				
			</div><!-- /.admin-form --> 
		</div>
	</div><!-- /#wp-content --> 
  
	<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


</div><!-- /#wrap --> 

</body>
</html><?php }} ?>