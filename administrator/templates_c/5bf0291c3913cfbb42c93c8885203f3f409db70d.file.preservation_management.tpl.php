<?php /* Smarty version Smarty-3.1.13, created on 2014-02-18 22:09:53
         compiled from ".\templates\preservation_management.tpl" */ ?>
<?php /*%%SmartyHeaderCode:9968530369b1a60400-93781815%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5bf0291c3913cfbb42c93c8885203f3f409db70d' => 
    array (
      0 => '.\\templates\\preservation_management.tpl',
      1 => 1391738809,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '9968530369b1a60400-93781815',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_530369b1ad2165_99332864',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_530369b1ad2165_99332864')) {function content_530369b1ad2165_99332864($_smarty_tpl) {?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<title>Boiling Point :: Manage Reservations</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<link rel="shortcut icon" type="image/png" href="http://www.bpgroupusa.com/favicon.ico">

<link href="css/global.css" rel="stylesheet" type="text/css">
<link href="css/stylesheet.css" rel="stylesheet" type="text/css">

<script src="../js/jqgrid/jquery.min.js" type="text/javascript"></script> 
<script src="../js/jqgrid/jquery-ui-1.8.2.custom.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/main-jquery.js"></script>

<link rel="stylesheet" type="text/css" media="screen" href="../js/jqgrid/themes/redmond/jquery-ui-1.8.2.custom.css" /> 
    	<link rel="stylesheet" type="text/css" media="screen" href="../js/jqgrid/themes/ui.jqgrid.css" />
    	<link rel="stylesheet" type="text/css" media="screen" href="../js/jqgrid/themes/ui.multiselect.css" />
		
		
    	<script src="../js/jqgrid//i18n/grid.locale-en.js" type="text/javascript"></script> 
    	<script type="text/javascript"> 
    	$.jgrid.no_legacy_api = true; 
    	$.jgrid.useJSON = true; 
    	</script> 
    	<script src="../js/jqgrid/jquery.jqGrid.min.js" type="text/javascript"></script> 
    	
		<!-- Date: 2013-07-16 -->

		<script>
		$(function() {
			jQuery("#preservation_information_table").jqGrid({
				autowidth: true,
				height:400,
				ajaxGridOptions : {
					type : "POST"
				},
				serializeGridData : function(postdata) {
					//postdata.page = 1;
					postdata.command="list_jqgrid_preservation_cmd";
					postdata.store_id="-1";
					return postdata;
				},
				url : '../include/command/preservation.cmd.php',
				datatype : "json",
				colNames : ['ID', 'Store', 'Time', 'Splite Group', 'Persons','Tel'],
				colModel : [{
					name : 'id',
					index : 'id',
					width : 55
				}, {
					name : 'store_id',
					index : 'store_id',
					width : 90
				}, {
					name : 'preservation_time',
					index : 'preservation_time',
					width : 100
				}, {
					name : 'splite_group',
					index : 'splite_group',
					width : 80,
					align : "right"
				}, {
					name : 'preservation_persons',
					index : 'preservation_persons',
					width : 80,
					align : "right"
				}, {
					name : 'preservation_tel',
					index : 'preservation_tel',
					width : 80,
					align : "right"
				}],
				rowNum : 20,
				
				rowList : [10, 20, 30],
				pager : '#preservation_information_pager',
				sortname : 'invdate',
				viewrecords : true,
				sortorder : "desc",
				caption : "Preservation Information"
			});
			jQuery("#preservation_information_pager").jqGrid('navGrid', '#preservation_information_pager', {
				edit : false,
				add : false,
				del : false
			});
		});
		function reloadByStoreId(storeId){
			
			$("#preservation_information_table").jqGrid('setGridParam', { serializeGridData : function(postdata) {
					postdata.page = 1;
					postdata.command="get_jqgrid_preservations_cmd";
					postdata.store_id=storeId;
					return postdata;
				} });
			$("#preservation_information_table").jqGrid().trigger( 'reloadGrid');
		}
		</script>

</head>

<body class="body-bg-01">

<!-- Begin Page -->
<div id="wrap">

	<div id="wp-header">
		<div id="header" class="clears">
			<div id="logo" class="fl"> <a href="http://www.bpgroupusa.com/"><img src="images/logo.png" alt="logo" height="117" width="117"></a> </div>
			<?php echo $_smarty_tpl->getSubTemplate ("nav.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

		</div>
	</div><!-- /#wp-header -->

	<!-- Begin content -->
	<div id="wp-content">
		<div id="content">
			<div id="content-header">
				<div class="b-menu">
					<ul class="clears">
						<li><a href="http://www.bpgroupusa.com/">Home</a></li>
						<li>/</li>
						<li>Manage Reservations</li>
					</ul>
				</div>
				<div class="content-title">
					<h1 class="clears">Manage Reservations</h1>
				</div>
			</div>
			<div class="main">



				<div>
				
				<table id="preservation_information_table"></table>
				<div id="preservation_information_pager"></div>
				
				</div>



				
			</div>
		</div>
	</div><!-- /#wp-content --> 
  
	<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


</div><!-- /#wrap --> 
</body>
</html><?php }} ?>