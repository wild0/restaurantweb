<?php /* Smarty version Smarty-3.1.13, created on 2014-04-03 01:06:13
         compiled from ".\templates\nav.tpl" */ ?>
<?php /*%%SmartyHeaderCode:22927530368bdd65d69-11687832%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '78dfd564017866b2a07d32220e5f711a454b3ecb' => 
    array (
      0 => '.\\templates\\nav.tpl',
      1 => 1391786616,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '22927530368bdd65d69-11687832',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_530368bdda3bc9_50319368',
  'variables' => 
  array (
    'mode' => 0,
    'login' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_530368bdda3bc9_50319368')) {function content_530368bdda3bc9_50319368($_smarty_tpl) {?><div id="nav">
						<ul>
							<?php if ($_smarty_tpl->tpl_vars['mode']->value=='root'){?>
							<li>
								<a class="menu-head" href="ad_management.php" id="nav-ad" ><span class="english">ads</span><span class="chinese">廣告</span></a>
							</li>
							<?php }?>
							<?php if ($_smarty_tpl->tpl_vars['mode']->value=='root'){?>
							<li>
								<a class="menu-head" href="food_menu_management.php" id="nav-food"><span class="english">food</span><span class="chinese">美食</span></a>
							</li>
							<?php }?>
							
							<?php if ($_smarty_tpl->tpl_vars['mode']->value=='root'){?>
							<li>
								<a class="menu-head" href="store_management.php" id="nav-store"><span class="english">store locations</span><span class="chinese">沸點分店</span></a>
							</li>
							<?php }?>
							
							<li>
								<a class="menu-head" href="preservation_management.php" id="nav-reservation"><span class="english">reservations</span><span class="chinese">訂位</span></a>
								<div style="display: none;" class="menu-body clears">
									<dl>
										<?php if ($_smarty_tpl->tpl_vars['mode']->value=='root'){?>
										<dd>
											<a href="preservation_status_management.php">Current Reservations</a>
										</dd>
										<?php }?>
										<dd>
											<a href="log_manager.php">Log Manager</a>
										</dd>
										<?php if ($_smarty_tpl->tpl_vars['mode']->value=='root'){?>
										<dd>
											<a href="waiting_time.php">Set Waiting Time</a>
										</dd>
										<dd>
											<a href="sms_manager.php">SMS Settings</a>
										</dd>
										
										<dd>
											<a href="group_define.php">Define Groups</a>
										</dd>
										<?php }?>
									</dl>
								</div>
							</li>
							
							<li>
								<a class="menu-head" href="card_management.php" id="nav-card-reader"><span class="english">card reader</span><span class="chinese">讀卡機</span></a>
							</li>
							
							<li>
								<a class="menu-head" href="user_management.php" id="nav-user"><span class="english">users</span><span class="chinese">使用者</span></a>
							</li>
							
							<?php if ($_smarty_tpl->tpl_vars['login']->value==1){?>
							<li>
								<a class="menu-head" href="../include/command/user.cmd.php?command=logout_cmd&platform=web&return_url=login" id="nav-logout"><span class="english">logout</span><span class="chinese">登出</span></a>
							</li>
							<?php }?>
						</ul>
					</div><!-- /#nav --><?php }} ?>