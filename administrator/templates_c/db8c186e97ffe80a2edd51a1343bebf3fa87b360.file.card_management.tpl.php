<?php /* Smarty version Smarty-3.1.13, created on 2014-02-18 22:09:52
         compiled from ".\templates\card_management.tpl" */ ?>
<?php /*%%SmartyHeaderCode:21299530369b073c716-21105998%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'db8c186e97ffe80a2edd51a1343bebf3fa87b360' => 
    array (
      0 => '.\\templates\\card_management.tpl',
      1 => 1391783599,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '21299530369b073c716-21105998',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_530369b07c4850_51078483',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_530369b07c4850_51078483')) {function content_530369b07c4850_51078483($_smarty_tpl) {?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
		<title>Boiling Point :: Manage Ads</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<script src="../js/jqgrid/jquery.min.js" type="text/javascript"></script>
		<script src="../js/jqgrid/jquery-ui-1.8.2.custom.min.js" type="text/javascript"></script>
		
		<script type="text/javascript" src="js/main-jquery.js"></script>

		<link rel="stylesheet" type="text/css" media="screen" href="../js/jqgrid/themes/redmond/jquery-ui-1.8.2.custom.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="../js/jqgrid/themes/ui.jqgrid.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="../js/jqgrid/themes/ui.multiselect.css" />

		
		<script src="../js/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
		<script type="text/javascript">
			$.jgrid.no_legacy_api = true;
			$.jgrid.useJSON = true;
		</script>
		<script src="../js/jqgrid/jquery.jqGrid.min.js" type="text/javascript"></script>
		
		<!-- Date: 2013-07-16 -->
		<link rel="shortcut icon" type="image/png" href="http://www.bpgroupusa.com/favicon.ico">

		<link href="css/global.css" rel="stylesheet" type="text/css">
		<link href="css/stylesheet.css" rel="stylesheet" type="text/css">

		
		<script>
			$(function() {
				jQuery("#card_information_table").jqGrid({
					autowidth: true,
					ajaxGridOptions : {
						type : "POST"
					},
					serializeGridData : function(postdata) {
						//postdata.page = 1;
						postdata.command = "list_jqgrid_card_cmd";
						return postdata;
					},
					url : '../include/command/card.cmd.php',
					datatype : "json",
					colNames : ['ID', 'Code', 'Permission', 'Description',  'modify', 'delete'],
					colModel : [{
						name : 'id',
						index : 'id',
						width : 55
					}, {
						name : 'code',
						index : 'code',
						width : 90
					}, {
						name : 'permission',
						index : 'permission',
						width : 100
					}, {
						name : 'description',
						index : 'description',
						width : 80,
						align : "right"
					}, {
						name : 'card_modify_action',
						index : 'action1',
						width : 40,
						sortable : false
					}, {
						name : 'card_delete_action',
						index : 'action2',
						width : 40,
						sortable : false
					}],
					rowNum : 20,
					width : 700,
					rowList : [10, 20, 30],
					pager : '#card_information_pager',
					sortname : 'invdate',
					viewrecords : true,
					sortorder : "desc",
					caption : "Card Information",
					loadComplete : function() {
						var ids = $("#card_information_table").jqGrid('getDataIDs');
						for (var i = 0; i < ids.length; i++) {
							var cl = ids[i];
							var data = $("#card_information_table").jqGrid('getRowData', cl);

							var modifyButton = "<span title='編輯' class='img_btn'  onClick='modifyCardOperation(" + cl + ")' >modify</span>";
							var deleteButton = "<span title='刪除' class='img_btn' onClick='deleteCardOperation(" + cl + ")' >delete</span>";

							$("#card_information_table").jqGrid('setRowData', ids[i], {
								card_modify_action : modifyButton
							});
							$("#card_information_table").jqGrid('setRowData', ids[i], {
								card_delete_action : deleteButton
							});

						}
					}
				});
				jQuery("#card_information_pager").jqGrid('navGrid', '#card_information_pager', {
					edit : false,
					add : false,
					del : false
				});
			});
			function deleteCardOperation(cardId) {
				//jQuery("#card_information_table").jqGrid().trigger( 'reloadGrid' );
				if(confirm("Do you want to delete?")){
					var cmdUrl = "../include/command/";
					$.post(cmdUrl + "card.cmd.php", {
						command : "remove_card_cmd",
						card_id_value : cardId
					}, function(data, textStatus) {
						jQuery("#card_information_table").jqGrid().trigger('reloadGrid');
					}, "json");
				}
				
			}

			function modifyCardOperation(cardId) {
				//jQuery("#store_information_table").jqGrid().trigger( 'reloadGrid' );
				var url = "card_modify.php?";
				parent.location = url + "card_id=" + cardId;

			}
		</script>

	</head>

	<body class="body-bg-01">

		<!-- Begin Page -->
		<div id="wrap">

			<div id="wp-header">
				<div id="header" class="clears">
					<div id="logo" class="fl">
						<a href="http://www.bpgroupusa.com/"><img src="images/logo.png" alt="logo" height="117" width="117"></a>
					</div>
					<?php echo $_smarty_tpl->getSubTemplate ("nav.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

				</div>
			</div><!-- /#wp-header -->

			<!-- Begin content -->
			<div id="wp-content">
				<div id="content">
					<div id="content-header">
						<div class="b-menu">
							<ul class="clears">
								<li>
									<a href="http://www.bpgroupusa.com/">Home</a>
								</li>
								<li>
									/
								</li>
								<li>
									Cards Management
								</li>
							</ul>
						</div>
						<div class="content-title">
							<h1 class="clears">Manage Cards</h1>
						</div>
					</div>
					<div class="main">

						<div>
							<table id="card_information_table"></table>
							<div id="card_information_pager"></div>
						</div>

						<p class="admin-form__submit">
							<input class="submit-btn" value="new card" type="submit"  onclick="location.assign('card_insert.php')">
						</p>

					</div>
				</div>
			</div><!-- /#wp-content -->

			<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


		</div><!-- /#wrap -->

	</body>
</html><?php }} ?>