<?php
 /**
 * Example Application

 * @package Example-application
 */
session_start();
$position = "preservation";

require_once("../include/constant/db.constant.php");
require_once(CONSTANT_PATH."advertisement.constant.php");
require_once(INCLUDE_PATH."header.php");

require('../libs/Smarty.class.php');


$smarty = new Smarty;



//$smarty->force_compile = true;
$smarty->debugging = $debug;
$smarty->caching = $cache;
$smarty->cache_lifetime = 120;

global $userHandler;
$login = $userHandler->isLogin();
$mode = getMode();

$smarty->assign("login", $login);
$smarty->assign("mode", $mode);

$smarty->display('preservation_management.tpl');
?>