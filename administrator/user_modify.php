<?php
 /**
 * Example Application

 * @package Example-application
 */
session_start();
$position = "store";

require_once("../include/constant/db.constant.php");
require_once(CONSTANT_PATH."advertisement.constant.php");
require_once(INCLUDE_PATH."header.php");

require('../libs/Smarty.class.php');

$smarty = new Smarty;



//$smarty->force_compile = true;
$smarty->debugging = $debug;
$smarty->caching = $cache;
$smarty->cache_lifetime = 120;

global $userHandler;
$login = $userHandler->isLogin();
$mode = getMode();

$smarty->assign("login", $login);
$smarty->assign("mode", $mode);

$userId = $_REQUEST["user_id"];
$user = $userHandler->get($userId);
$smarty->assign("user_id", $user->getVar("user_id"));
$smarty->assign("user_name", $user->getVar("user_name"));
$smarty->assign("user_device_list", $user->getVar("user_device_list"));

$smarty->display('user_modify.tpl');
?>