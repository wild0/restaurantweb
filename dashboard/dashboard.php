<?php

session_start();
$position = "dashboard";
//header ('Content-type:text/html; charset=utf-8');
require_once ("../include/constant/db.constant.php");
require_once (CONSTANT_PATH . "preservation.constant.php");
require_once (INCLUDE_PATH . "header.php");

$url = "../include/command/preservation.cmd.php?";

$storeId = $variableHandler -> getVariable(NODE_STORE_ID);
function printSkippedPerservation($preservation){
	global $url;
	$preservationId = $preservation -> getVar("preservation_record_id");
	$status = $preservation -> getVar("preservation_record_status");
	
	echo "<tr>";
	echo "<td>" . $preservation -> getTicketLabel() . "</td>";
	echo "<td>" . date("Y/m/d h:i:s", $preservation -> getVar("preservation_record_timestamp")) . "</td>";
	echo "<td>" . $preservation -> getVar("preservation_record_status") . "</td>";
	echo "<td>" . $preservation -> getVar("preservation_record_person_count") . "</td>";
	echo "<td>" . $preservation -> getVar("preservation_record_splitable") . "</td>";

	if ($status == PRESERVATION_STATUS_SKIP) {
		echo "<td><button onClick=\"parent.location='" . $url . "command=call_preservation_cmd&location=dashboard&preservation_id=" . $preservationId . "'\">Call</button></td>";
		echo "<td><button onClick=\"parent.location='" . $url . "command=seat_preservation_cmd&location=dashboard&preservation_id=" . $preservationId . "'\">Seat</button></td>";
		echo "<td><button onClick=\"parent.location='" . $url . "command=remove_preservation_cmd&location=dashboard&preservation_id=" . $preservationId . "'\">Remove</button></td>";
		echo "<td><button onClick=\"parent.location='" . $url . "command=wait_preservation_cmd&location=dashboard&preservation_id=" . $preservationId . "'\">Waiting</button></td>";
	} 
	echo "</tr>";
}
function printPerservation($preservation) {
	global $url;
	$preservationId = $preservation -> getVar("preservation_record_id");
	$status = $preservation -> getVar("preservation_record_status");
	if($status==PRESERVATION_STATUS_SKIP || $status==PRESERVATION_STATUS_CANCEL){
		return ;
	}

	echo "<tr>";
	echo "<td>" . $preservation -> getTicketLabel() . "</td>";
	echo "<td>" . date("Y/m/d h:i:s", $preservation -> getVar("preservation_record_timestamp")) . "</td>";
	echo "<td>" . $preservation -> getVar("preservation_record_status") . "</td>";
	echo "<td>" . $preservation -> getVar("preservation_record_person_count") . "</td>";
	echo "<td>" . $preservation -> getVar("preservation_record_splitable") . "</td>";
	echo "<td>" . $preservation -> getVar("preservation_record_tel") . "</td>";
	
	if ($status == PRESERVATION_STATUS_WAIT) {
		echo "<td><button onClick=\"parent.location='" . $url . "command=call_preservation_cmd&location=dashboard&preservation_id=" . $preservationId . "'\">Call</button></td>";
		echo "<td><button onClick=\"parent.location='" . $url . "command=remove_preservation_cmd&location=dashboard&preservation_id=" . $preservationId . "'\">Remove</button></td>";
	} else if ($status == PRESERVATION_STATUS_CALL) {
		echo "<td><button onClick=\"parent.location='" . $url . "command=seat_preservation_cmd&location=dashboard&preservation_id=" . $preservationId . "'\">Seat</button></td>";
		echo "<td><button onClick=\"parent.location='" . $url . "command=skip_preservation_cmd&location=dashboard&preservation_id=" . $preservationId . "'\">Skip</button></td>";
		echo "<td><button onClick=\"parent.location='" . $url . "command=wait_preservation_cmd&location=dashboard&preservation_id=" . $preservationId . "'\">Waiting</button></td>";
	} else if ($status == PRESERVATION_STATUS_SEAT) {
		echo "<td><button onClick=\"parent.location='" . $url . "command=wait_preservation_cmd&location=dashboard&preservation_id=" . $preservationId . "'\">Waiting</button></td>";
	}
	echo "</tr>";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>index</title>
		<meta name="author" content="wild0" />

	</head>
	<body>

		<div style='border-color:#0000FF; border-style:solid; padding:5px;'>
			Group A [Dinning list]
			<table>
				<tr>
					<td>Ticket</td>
					<td>Start Time</td>
					<td>Status</td>
					<td>Person</td>
					<td>Splitable</td>
					<td>Operations</td>
				</tr>
				<?php
				global $preservationHandler;
				
				$preservationsASkip = $preservationHandler -> getRecordsByStatusAndStoreIdAndGroupId(PRESERVATION_STATUS_SKIP, $storeId, 0);
				for ($i = 0; $i < sizeof($preservationsASkip); $i++) {
					$preservation = $preservationsASkip[$i];
					printSkippedPerservation($preservation);
				}
				
				$preservationsA = $preservationHandler -> getRecordsByStatusAndStoreIdAndGroupId(-1, $storeId, 0);
				for ($i = 0; $i < sizeof($preservationsA); $i++) {
					
					$preservation = $preservationsA[$i];
					printPerservation($preservation);
					
					/*
					$preservationId = $preservation -> getVar("preservation_record_id");
					$status = $preservation -> getVar("preservation_record_status");

					echo "<td>" . $preservation -> getVar("preservation_record_ticket") . "</td>";
					echo "<td>" . date("Y/m/d h:i:s", $preservation -> getVar("preservation_record_timestamp")) . "</td>";
					echo "<td>" . $preservation -> getVar("preservation_record_status") . "</td>";
					echo "<td>" . $preservation -> getVar("preservation_record_person_count") . "</td>";
					if ($status == PRESERVATION_STATUS_WAIT) {
						echo "<td><button onClick=\"parent.location='" . $url . "command=call_preservation_cmd&location=dashboard&preservation_id=" . $preservationId . "'\">Call</button></td>";
						echo "<td><button onClick=\"parent.location='" . $url . "command=remove_preservation_cmd&location=dashboard&preservation_id=" . $preservationId . "'\">Remove</button></td>";
					} else if ($status == PRESERVATION_STATUS_CALL) {
						echo "<td><button onClick=\"parent.location='" . $url . "command=seat_preservation_cmd&location=dashboard&preservation_id=" . $preservationId . "'\">Seat</button></td>";
						echo "<td><button onClick=\"parent.location='" . $url . "command=skip_preservation_cmd&location=dashboard&preservation_id=" . $preservationId . "'\">Skip</button></td>";
						echo "<td><button onClick=\"parent.location='" . $url . "command=wait_preservation_cmd&location=dashboard&preservation_id=" . $preservationId . "'\">Waiting</button></td>";
					}
					echo "</tr>";
					*/
				}
				?>
			</table>
		</div>
		<br/>
		<div style='border-color:#0000FF; border-style:solid; padding:5px;'>
			Group B [Dinning list]
			<table>

				<tr>
					<td>Ticket</td>
					<td>Start Time</td>
					<td>Status</td>
					<td>Person</td>
					<td>Operations</td>
				</tr>
				<?php

				//SKIP
				$preservationsBSkip = $preservationHandler -> getRecordsByStatusAndStoreIdAndGroupId(PRESERVATION_STATUS_SKIP, $storeId, 1);
				for ($i = 0; $i < sizeof($preservationsBSkip); $i++) {
					$preservation = $preservationsBSkip[$i];
					printSkippedPerservation($preservation);
				}

				//NORMAL
				$preservationsB = $preservationHandler -> getRecordsByStatusAndStoreIdAndGroupId(-1, $storeId, 1);
				for ($i = 0; $i < sizeof($preservationsB); $i++) {
					$preservation = $preservationsB[$i];
					
					printPerservation($preservation);
					
					
				}
				?>
			</table>
		</div>
		<br/>
		<div style='border-color:#0000FF; border-style:solid; padding:5px;'>
			Group C [Dinning list]
			<table>

				<tr>
					<td>Ticket</td>
					<td>Start Time</td>
					<td>Status</td>
					<td>Person</td>
					<td>Operations</td>
				</tr>
				<?php
				
				
				$preservationsCSkip = $preservationHandler -> getRecordsByStatusAndStoreIdAndGroupId(PRESERVATION_STATUS_SKIP, $storeId, 2);
				for ($i = 0; $i < sizeof($preservationsCSkip); $i++) {
					$preservation = $preservationsCSkip[$i];
					printSkippedPerservation($preservation);
				}
				
				
				$preservationsC = $preservationHandler -> getRecordsByStatusAndStoreIdAndGroupId(-1, $storeId, 2);

				for ($i = 0; $i < sizeof($preservationsC); $i++) {
					$preservation = $preservationsC[$i];
					printPerservation($preservation);
					
				}
				?>
			</table>
		</div>

		<br/>
		<div style='border-color:#0000FF; border-style:solid; padding:5px;'>
			Group D [Dinning list]
			<table>

				<tr>
					<td>Ticket</td>
					<td>Start Time</td>
					<td>Status</td>
					<td>Person</td>
					<td>Operations</td>
				</tr>
				<?php
				
				$preservationsDSkip = $preservationHandler -> getRecordsByStatusAndStoreIdAndGroupId(PRESERVATION_STATUS_SKIP, $storeId, 3);
				for ($i = 0; $i < sizeof($preservationsDSkip); $i++) {
					$preservation = $preservationsDSkip[$i];
					printSkippedPerservation($preservation);
				}
				
				
				
				$preservationsD = $preservationHandler -> getRecordsByStatusAndStoreIdAndGroupId(-1, $storeId, 3);

				for ($i = 0; $i < sizeof($preservationsD); $i++) {
					$preservation = $preservationsD[$i];
					printPerservation($preservation);
					
				}
				?>
			</table>
		</div>

	</body>
</html>