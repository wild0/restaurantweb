$(function() {
/*窗口变化时运行*/
	$(window).resize(function() {
		$("#home-list li").width($(window).width());
		$("#home-banner ul li img").height($(window).width()*0.3946);
		$("#home-banner").height($(window).width()*0.3946);
	});
	$("#nav").menu();
	$("#home-list li").width($(window).width());
	
	//$("#home-banner").imgShow({auto:"true",show_nav:"show",show_nav_class:"nav-on-01"});
	
	$(".x-icon").click(function(){
		$(".pop-up-bg").hide();
		return false;
	});
	$("#head-my-account").click(function(){
		$("#home-pop").show();
		return false;
	});
	$("#home-banner ul li img").height($(window).width()*0.3946);
	$("#home-banner").height($(window).width()*0.3946);
	$(".btn-locations-near-me").click(function(){
		//$("body").scrollTop($('#home-map').offset().top);
		$("html,body").animate({scrollTop: $('#home-map').offset().top}, 1000);
		return false;
	});
	if ($(".select-style").length>0){
		$(".select-style").selectbox();
	}
});

$(document).ready(function() {
    var obj = new Image();
    obj.src = "skin/BoilingPoint/_images/home-banner-03.jpg";
    obj.onload = function()
    {
        $("#home-banner").imgShow({auto:"true",show_nav:"show",show_nav_class:"nav-on-01",auto_time:6000});
    }
});


/*menu*/
(function($) {
/*
有下拉的a标签加class=menu-head
下拉则加class=menu-body
*/
  	$.fn.menu=function (optns){
		var menuMain = $(this);
		var settings = {
			 a_mode : "slide",		//动画的样式
			 a_class: "aa1"			//选中menu-head的class样式
		};
		$.extend(settings, optns);
		menuMain.find(".menu-body").hide();
		$(".menu-head",menuMain).hover(function(index) {
			if(settings.a_mode =="slide"){
				$(this).next(".menu-body").stop(true,true).slideDown('fast');
			
				$(this).parent().hover(function() {
					$(this).children(".menu-head").addClass(settings.a_class);
				}, function(){
					$(this).children(".menu-body").slideUp('fast').end().children(".menu-head").removeClass(settings.a_class);
				});
			}else{
				$(this).next(".menu-body").stop(true,true).css("display","block");
			
				$(this).parent().hover(function() {
					$(this).children(".menu-head").addClass(settings.a_class);
				}, function(){
					$(this).children(".menu-body").css("display","none").end().children(".menu-head").removeClass(settings.a_class);
				});
			}
    	});
	};
})(jQuery);
/*end menu*/

/*scrolltotop*/
var scrolltotop={
	setting: {startline:100, scrollto: 0, scrollduration:500, fadeduration:[500, 100]},
	controlHTML: '<img src="/skin/BoilingPoint/_images/btn/btn-top.png" alt="Top" />', 
	controlattrs: {offsetx:0, offsety:25}, 
	anchorkeyword: '#top', 
	state: {isvisible:false, shouldvisible:false},
	scrollup:function(){
		if (!this.cssfixedsupport){this.$control.css({opacity:0}); };
		var dest=isNaN(this.setting.scrollto)? this.setting.scrollto : parseInt(this.setting.scrollto);
		if (typeof dest=="string" && jQuery('#'+dest).length==1){dest=jQuery('#'+dest).offset().top;}
		else{dest=0;};
		this.$body.animate({scrollTop: dest}, this.setting.scrollduration);
	},

	keepfixed:function(){
		var $window=jQuery(window);
		var controlx=$window.scrollLeft() + $window.width() - this.$control.width() - this.controlattrs.offsetx;
		var controly=$window.scrollTop() + $window.height() - this.$control.height() - this.controlattrs.offsety;
		this.$control.css({left:controlx+'px', top:controly+'px'});
	},

	togglecontrol:function(){
		var scrolltop=jQuery(window).scrollTop();
		if (!this.cssfixedsupport){this.keepfixed();};
		this.state.shouldvisible=(scrolltop>=this.setting.startline)? true : false;
		if (this.state.shouldvisible && !this.state.isvisible){
			this.$control.stop().animate({opacity:1}, this.setting.fadeduration[0]);
			this.state.isvisible=true;
		}
		else if (this.state.shouldvisible==false && this.state.isvisible){
			this.$control.stop().animate({opacity:0}, this.setting.fadeduration[1]);
			this.state.isvisible=false;
		}
	},
	
	init:function(){
		jQuery(document).ready(function($){
			var mainobj=scrolltotop;
			var iebrws=document.all;
			mainobj.cssfixedsupport=!iebrws || iebrws && document.compatMode=="CSS1Compat" && window.XMLHttpRequest;
			mainobj.$body=(window.opera)? (document.compatMode=="CSS1Compat"? $('html') : $('body')) : $('html,body');
			mainobj.$control=$('<div id="topcontrol">'+mainobj.controlHTML+'</div>')
				.css({position:mainobj.cssfixedsupport? 'fixed' : 'absolute', bottom:mainobj.controlattrs.offsety, right:mainobj.controlattrs.offsetx, opacity:0, cursor:'pointer',"z-index":'99'})
				.attr({title:'Scroll Back to Top'})
				.click(function(){mainobj.scrollup(); return false})
				.appendTo('body');
			if (document.all && !window.XMLHttpRequest && mainobj.$control.text()!=''){mainobj.$control.css({width:mainobj.$control.width()}); }
			mainobj.togglecontrol();
			$('a[href="' + mainobj.anchorkeyword +'"]').click(function(){
				mainobj.scrollup();
				return false;
			});
			$(window).bind('scroll resize', function(e){
				mainobj.togglecontrol();
			})
		})
	}
};

scrolltotop.init();