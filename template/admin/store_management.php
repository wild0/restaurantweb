<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<title>Boiling Point :: Manage Stores</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<link rel="shortcut icon" type="image/png" href="http://www.bpgroupusa.com/favicon.ico">

<link href="css/global.css" rel="stylesheet" type="text/css">
<link href="css/stylesheet.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="js/jquery-1.js"></script>
<script type="text/javascript" src="js/main-jquery.js"></script>

</head>

<body class="body-bg-01">

<!-- Begin Page -->
<div id="wrap">

	<div id="wp-header">
		<div id="header" class="clears">
			<div id="logo" class="fl"> <a href="http://www.bpgroupusa.com/"><img src="images/logo.png" alt="logo" height="117" width="117"></a> </div>
			<div id="nav">
				<ul>
					<li><a class="menu-head" href="#" id="nav-ad" ><span class="english">ads</span><span class="chinese">廣告</span></a></li>
					<li><a class="menu-head" href="#" id="nav-food"><span class="english">food</span><span class="chinese">美食</span></a></li>
					<li><a class="menu-head" href="#" id="nav-store"><span class="english">store locations</span><span class="chinese">沸點分店</span></a></li>
					<li><a class="menu-head" href="#" id="nav-reservation"><span class="english">reservations</span><span class="chinese">訂位</span></a>
						<div style="display: none;" class="menu-body clears">
						<dl>
							<dd><a href="preservation_management.php">Current Reservations</a></dd>
							<dd><a href="#">History Logs</a></dd>
							<dd><a href="#">Set Waiting Time</a></dd>
							<dd><a href="#">SMS Settings</a></dd>
							<dd><a href="#">Define Groups</a></dd>
						</dl>
						</div>
					</li>
					<li><a class="menu-head" href="#" id="nav-card-reader"><span class="english">card reader</span><span class="chinese">讀卡機</span></a></li>
					<li><a class="menu-head" href="#" id="nav-user"><span class="english">users</span><span class="chinese">使用者</span></a></li>
					<li><a class="menu-head" href="#" id="nav-logout"><span class="english">logout</span><span class="chinese">登出</span></a></li>
				</ul>
			</div><!-- /#nav -->
		</div>
	</div><!-- /#wp-header -->

	<!-- Begin content -->
	<div id="wp-content">
		<div id="content">
			<div id="content-header">
				<div class="b-menu">
					<ul class="clears">
						<li><a href="http://www.bpgroupusa.com/">Home</a></li>
						<li>/</li>
						<li>Manage Stores</li>
					</ul>
				</div>
				<div class="content-title">
					<h1 class="clears">Manage Stores</h1>
				</div>
			</div>
			<div class="main">



				<div>{--- GRID ---}</div>



				<p class="admin-form__submit"><input class="submit-btn" value="Add store" type="submit"></p>
				
			</div>
		</div>
	</div><!-- /#wp-content --> 
  
	<!-- Begin footer -->
	<div id="wp-footer"><div id="footer">
		<div class="foot-main clears">
			<div class="foot-logo fl"><img src="images/foot-logo.png" alt="" height="92" width="101"></div>
			<div class="site-map fl clears">
				<ul>
					<li><a href="#">Ads</a></li>
					<li><a href="#">Food</a></li>
					<li><a href="#">Store Locations</a></li>
					<li><a href="#">Card Reader</a></li>
					<li><a href="#">Users</a></li>
				</ul>
				<ul>
					<li><a href="#">Reservations</a></li>
					<li><a href="#">History Logs</a></li>
					<li><a href="#">Set Waiting Time</a></li>
					<li><a href="#">SMS Settings</a></li>
					<li><a href="#">Define Groups</a></li>
				</ul>
			</div>
		</div>
	</div></div><!-- /#wp-footer --> 

</div><!-- /#wrap --> 
</body>
</html>


